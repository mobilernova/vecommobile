import { Component, OnInit } from '@angular/core';
import { LoadingController, ToastController, NavController, MenuController } from '@ionic/angular';
import { ServiciosService } from '../servicios/servicios.service';


@Component({
  selector: 'app-inicio-sesion',
  templateUrl: './inicio-sesion.page.html',
  styleUrls: ['./inicio-sesion.page.scss'],
})
export class InicioSesionPage implements OnInit {
  email: any;
  password: any;
    success: any;
    tipo_login: string;
    telefono: null;
    nombre: null;
    player_id: any;
  user: any;
    constructor(public nav: NavController, 
    private service: ServiciosService, 
    public loadingController: LoadingController,
    public toastCtrl: ToastController,
    public menuCtrl:MenuController) {
    }

  ionViewWillEnter() {
    this.menuCtrl.enable(false);
  }

  ngOnInit() {
  }

  public ingresar() {
    this.tipo_login = '1';
    this.service.loginVecom(this.tipo_login, this.email, this.nombre, this.telefono, this.password).subscribe(data => {
       this.user = data.data;
       console.log(data);
       localStorage.setItem('access_token', this.user.access_token);
       localStorage.setItem('idUsuario', this.user.id); 
       this.succesSms(this.user.nombre_completo);
      this.nav.navigateRoot('/home');
     }, err => {
       console.log(err);
       if (err.status == 401) {
         this.errorSms(err.error.data.message);
       } else {
         this.errorConexion();
       }
     });
  }

  registrarse() {
    this.nav.navigateBack('/registrarse');
  }

  async succesSms(msm) {
    const toast = await this.toastCtrl.create({
      message: "Bienvenido" +' '+ msm,
      duration: 4000,
      cssClass: 'my-custom-class-success',
    });
    toast.present();
  }

  async errorSms(msm) {
    const toast = await this.toastCtrl.create({
      message: msm,
      duration: 4000,
      cssClass: 'my-custom-class',
    });
    toast.present();
  }

  async errorConexion() {
    const toast = await this.toastCtrl.create({
      message: "error de conexion",
      duration: 3000,
      cssClass: 'my-custom-class',
    });
    toast.present();
  }

}
