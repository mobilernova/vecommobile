import { Component } from '@angular/core';

import { Platform, NavController, MenuController } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';


@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss']
})
export class AppComponent {
  public appPages = [
    {
      title: 'Inicio',
      url: '/home',
      icon: 'home'
    },
    {
      title: 'AutoVenta',
      url: '/autoventa',
      icon: 'logo-model-s'
    },
    {
      title: 'Empresas',
      url: '/empresas',
      icon: 'speedometer'
    },
    {
      title: 'Buscador',
      url: '/busquedas',
      icon: 'search'
    }
  ];

  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    public navCtrl: NavController,
    public menuCtrl: MenuController,
  ) {
    this.initializeApp();
  }

  initializeApp() {
    this.platform.ready().then(() => {
      if (localStorage.getItem('access_token')) {
        this.navCtrl.navigateRoot('/home');
      } else {
        this.navCtrl.navigateRoot('/inicio-sesion');
      }
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });
  }
  // public logout(){
  //   this.navCtrl.navigateRoot('/inicio-sesion');
  // }
  // public perfil(){
  //   this.navCtrl.navigateRoot('/perfil-usuario');
  // }

  closeMenu() {
    this.menuCtrl.close();
  }
}
