import { Component, OnInit } from '@angular/core';
import { ServiciosService } from '../servicios/servicios.service';
import { LoadingController, AlertController, ToastController, NavController } from '@ionic/angular';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-estadisticas',
  templateUrl: './estadisticas.page.html',
  styleUrls: ['./estadisticas.page.scss'],
})
export class EstadisticasPage implements OnInit {
  data: any;
  loadEstadisticas: any;
  arrayPosts: any;
  estado: string;
  estadoAuto:any;
  successEstado: any;
  constructor(private service: ServiciosService,
  public loadingCtrl: LoadingController,
  public route: ActivatedRoute,
  public alertController:AlertController,
  public toastCtrl: ToastController,
  public nav: NavController) { 
    this.data = this.route.snapshot.paramMap.get('detalleAutoVecom');
    this.estado = this.route.snapshot.paramMap.get('estado');

  }

  async ngOnInit() {
    if (this.estado == 'No vendido') {
      this.estadoAuto = false;
    } else {
      this.estadoAuto = true;
    }
    const loading = await this.loadingCtrl.create({
      message: 'Cargando...',
    });
    await loading.present().then(() => {
      this.service.estadisticasVehiculoVecom(this.data).subscribe(async data => {
        this.loadEstadisticas = data.data;
        console.log(this.loadEstadisticas);
        await loading.dismiss();
      }, err => {
        loading.dismiss();
        // this.message();
      })
    });
  }

  checkState(e){
console.log(e);
    if (e.target.checked) {
          this.estadoAuto = true;
    this.presentAlertConfirm();
    } else {
      this.estadoAuto = false;

    this.presentAlertConfirm();
    }
  }

  async presentAlertConfirm() {
    const alert = await this.alertController.create({
      header: 'Confirmar!',
      message: 'Desea cambiar el estado de venta del automovil?',
      buttons: [
        {
          text: 'Cancelar',
          role: 'cancel',
          cssClass: 'secondary',
          handler: (blah) => {
            console.log('Confirm Cancel: blah');
          }
        }, {
          text: 'Si',
          handler: () => {
            this.service.estadoVehiculoVecom(this.data).subscribe(async data => {
              this.successEstado = data.data;
              this.succesSms(this.successEstado);
              this.nav.navigateRoot('/perfil-usuario');
            }, err => {
              console.log(err);
              
              // this.message();
            })
          }
        }
      ]
    });

    await alert.present();
  }

  async succesSms(msm) {
    const toast = await this.toastCtrl.create({
      message: msm,
      duration: 4000,
      cssClass: 'my-custom-class-success',
    });
    toast.present();
  }

  async errorSms(msm) {
    const toast = await this.toastCtrl.create({
      message: msm,
      duration: 4000,
      cssClass: 'my-custom-class',
    });
    toast.present();
  }



}
