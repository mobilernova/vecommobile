import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { AutoventaPage } from './autoventa.page';

describe('AutoventaPage', () => {
  let component: AutoventaPage;
  let fixture: ComponentFixture<AutoventaPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AutoventaPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(AutoventaPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
