import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AutoventaPage } from './autoventa.page';

const routes: Routes = [
  {
    path: '',
    component: AutoventaPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AutoventaPageRoutingModule {}
