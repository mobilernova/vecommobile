import { Component, OnInit} from '@angular/core';
import { NavController, LoadingController, ToastController } from '@ionic/angular';
import { ServiciosService } from '../servicios/servicios.service';
import { Router } from '@angular/router';
import { PublicidadService } from '../servicios/publicidad.service';

@Component({
  selector: 'app-autoventa',
  templateUrl: './autoventa.page.html',
  styleUrls: ['./autoventa.page.scss'],
})
export class AutoventaPage {
  arrayPosts: any;
  page: number;
  imgNotCar = 'assets/images/notfound.jpg'

  constructor(public navCtrl: NavController,
              public loadingController: LoadingController,
              public toastController: ToastController,
              private service:ServiciosService,
              private service2:PublicidadService,
              public router:Router
    ) { }

  async ionViewWillEnter() {
    this.autito();
    this.page = 1;
    const loading = await this.loadingController.create({
      message: 'Cargando lista VECOM',
    });
    await loading.present().then(() => {
    this.service.listaVehiculoVecom(this.page).subscribe(
      async data => {
        this.arrayPosts = data.data;
        // this.categorias = data.data;
        console.log(this.arrayPosts);
        await loading.dismiss();
        this.msjToast();
      }, err => {
        console.log(err);
        loading.dismiss();
      });
    });
  }

  loadData(event){
    setTimeout(()=>{
      this.page++;
      this.service.listaVehiculoVecom(this.page).subscribe(data=>{
          data = data.data;
          console.log(data);
          
          let list:any = data;
          if (list.length > 0){
          for(let item of list){
            this.arrayPosts.push(item);
          }
          event.target.complete();
          }else{
          event.target.disabled = true;
          event.target.complete();
          } 
    },err =>{
      this.message();
    } 
    );
  }, 500)
}

detallePublicacion(c){
  const idVehiculo = c.id;
  this.router.navigate(['detalle-auto', {detalleAutoVecom: idVehiculo}]);
}

async message() {
  const loading = await this.loadingController.create({
    message: 'Error de Conexion',
    // duration: 2000
  });
  await loading.present();
  const { role, data } = await loading.onDidDismiss();

}
    async msjToast() {
      const toast = await this.toastController.create({
        message: 'Datos mostrados.',
        duration: 2000
      });
      toast.present();
    }
    async autito() {
      const loading = await this.loadingController.create({
        message: 'Cargando...',
      });
      await loading.present().then(() => {
      this.service2.listaPublicidadesVecom().subscribe(
        async data => {
          this.arrayPosts = data;
          // this.categorias = data.data;
          console.log(this.arrayPosts);
          await loading.dismiss();
          // this.msjToast();
        }, err => {
          console.log(err);
          loading.dismiss();
        });
      });
      }
}
