import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { AutoventaPageRoutingModule } from './autoventa-routing.module';

import { AutoventaPage } from './autoventa.page';
import { ComponentsModule } from '../components/components.module';



@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ComponentsModule,
    AutoventaPageRoutingModule
  ],
  declarations: [AutoventaPage]
})
export class AutoventaPageModule {}
