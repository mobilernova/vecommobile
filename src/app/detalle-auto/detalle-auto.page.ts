import { Component, OnInit } from '@angular/core';
import { NavController, LoadingController, ToastController, ActionSheetController, AlertController } from '@ionic/angular';
import { ServiciosService } from '../servicios/servicios.service';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-detalle-auto',
  templateUrl: './detalle-auto.page.html',
  styleUrls: ['./detalle-auto.page.scss'],
})
export class DetalleAutoPage implements OnInit {


  arrayPosts={id:"",nombre:"",puertas:"",cilindrada:"",tipo_combustible:"",caracteristicas:"",ciudad:"",precio:"",anho:"",marca:"",modelo:"",tipovehiculo:"",color:"",imagenes_autos:[{}]};


  data:any;

  constructor(public navCtrl: NavController,
              public service: ServiciosService,
              public loadingController: LoadingController,
              public toastController: ToastController,
              public actionSheetCtrl: ActionSheetController,
              public route: ActivatedRoute,
              public alertController: AlertController
    ) { 
      this.data = this.route.snapshot.paramMap.get('detalleAutoVecom');
    }

  async ngOnInit() {
    console.log(this.data);
    
    const loading = await this.loadingController.create({
      message: 'Cargando...',
    });
    await loading.present().then(() => {
    this.service.getDetalleAutomovil(this.data).subscribe(
      async data => {
        this.arrayPosts = data['data'];
        console.log(this.arrayPosts);
        await loading.dismiss();
        this.msjToast();
      }, err => {
        console.log(err);
        loading.dismiss();
      });
    });
    }
    async msjToast() {
      const toast = await this.toastController.create({
        message: 'Datos mostrados.',
        duration: 2000
      });
      toast.present();
    }
    async presentActionSheet() {
      const actionSheet = await this.actionSheetCtrl.create({
        backdropDismiss: false,
        buttons: [{
          text: 'Llamar',
          icon: 'call',
          handler: () => {
            console.log('Call clicked');
          }
        }, {
          text: 'Mandar Mensaje',
          icon: 'logo-whatsapp',
          handler: () => {
            console.log('SendMsj clicked');
          }
        }, {
          text: 'Compartir',
          icon: 'share-alt',
          handler: () => {
            console.log('Share clicked');
          }
        },{
          text: 'Cancelar',
          icon: 'close',
          role: 'cancelar',
          cssClass: 'secondary',
          handler: () => {
            console.log('Cancel clicked');
          }
        }]
      });
  
      await actionSheet.present();
    }

    async addFavorito() {
      const alert = await this.alertController.create({
        header: 'Anadir favorito!',
        message: 'Esta seguro que desea agregar a favoritos?',
        buttons: [
          {
            text: 'No',
            role: 'cancel',
            cssClass: 'secondary',
            handler: (blah) => {
              console.log('Confirm Cancel: blah');
            }
          }, {
            text: 'Si',
            handler: () => {
                this.service.anhadirFavoritoVehiculoVecom(this.data).subscribe(
                  async data => {
                    console.log(data.data);
                    this.succesSms(data.data)
                  }, err => {
                    this.errorSms(err.error.data.message);
                    console.log(err.error.data.message);
                  }
                ); 
            }
          }
        ]
      });
  
      await alert.present();
    }
    async succesSms(msm) {
      const toast = await this.toastController.create({
        message: msm,
        duration: 4000,
        cssClass: 'my-custom-class-success',
      });
      toast.present();
    }
  
    async errorSms(msm) {
      const toast = await this.toastController.create({
        message: msm,
        duration: 4000,
        cssClass: 'my-custom-class',
      });
      toast.present();
    }
  }