import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { EstadisticasPublicacionPage } from './estadisticas-publicacion.page';

const routes: Routes = [
  {
    path: '',
    component: EstadisticasPublicacionPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class EstadisticasPublicacionPageRoutingModule {}
