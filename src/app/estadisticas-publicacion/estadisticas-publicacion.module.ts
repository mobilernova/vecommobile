import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { EstadisticasPublicacionPageRoutingModule } from './estadisticas-publicacion-routing.module';

import { EstadisticasPublicacionPage } from './estadisticas-publicacion.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    EstadisticasPublicacionPageRoutingModule
  ],
  declarations: [EstadisticasPublicacionPage]
})
export class EstadisticasPublicacionPageModule {}
