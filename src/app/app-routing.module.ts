import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'home',
    pathMatch: 'full'
  },
  {
    path: 'home',
    loadChildren: () => import('./home/home.module').then(m => m.HomePageModule)
  },
  {
    path: 'inicio-sesion',
    loadChildren: () => import('./inicio-sesion/inicio-sesion.module').then( m => m.InicioSesionPageModule)
  },
  {
    path: 'registrarse',
    loadChildren: () => import('./registrarse/registrarse.module').then( m => m.RegistrarsePageModule)
  },
  {
    path: 'editar-perfil',
    loadChildren: () => import('./editar-perfil/editar-perfil.module').then( m => m.EditarPerfilPageModule)
  },
  {
    path: 'detalle-auto',
    loadChildren: () => import('./detalle-auto/detalle-auto.module').then( m => m.DetalleAutoPageModule)
  },
  {
    path: 'autoventa',
    loadChildren: () => import('./autoventa/autoventa.module').then( m => m.AutoventaPageModule)
  },
  {
    path: 'perfil-usuario',
    loadChildren: () => import('./perfil-usuario/perfil-usuario.module').then( m => m.PerfilUsuarioPageModule)
  },
  {
    path: 'detalle-empresa',
    loadChildren: () => import('./detalle-empresa/detalle-empresa.module').then( m => m.DetalleEmpresaPageModule)
  },
  {
    path: 'busquedas',
    loadChildren: () => import('./busquedas/busquedas.module').then( m => m.BusquedasPageModule)
  },
  {
    path: 'detalle-publicidad',
    loadChildren: () => import('./detalle-publicidad/detalle-publicidad.module').then( m => m.DetallePublicidadPageModule)
  },
  {
    path: 'empresas',
    loadChildren: () => import('./empresas/empresas.module').then( m => m.EmpresasPageModule)
  },
  {
    path: 'agregar-auto',
    loadChildren: () => import('./agregar-auto/agregar-auto.module').then( m => m.AgregarAutoPageModule)
  },
  {
    path: 'publicaciones-usuario',
    loadChildren: () => import('./publicaciones-usuario/publicaciones-usuario.module').then( m => m.PublicacionesUsuarioPageModule)
  },
  {
    path: 'favoritos-usuario',
    loadChildren: () => import('./favoritos-usuario/favoritos-usuario.module').then( m => m.FavoritosUsuarioPageModule)
  },
  {
    path: 'editar-vehiculo',
    loadChildren: () => import('./editar-vehiculo/editar-vehiculo.module').then( m => m.EditarVehiculoPageModule)
  },
  {
    path: 'galleria-automovil',
    loadChildren: () => import('./galleria-automovil/galleria-automovil.module').then( m => m.GalleriaAutomovilPageModule)
  },
  {
    path: 'estadisticas-publicacion',
    loadChildren: () => import('./estadisticas-publicacion/estadisticas-publicacion.module').then( m => m.EstadisticasPublicacionPageModule)
  },
  {
    path: 'agregar-fotografia',
    loadChildren: () => import('./agregar-fotografia/agregar-fotografia.module').then( m => m.AgregarFotografiaPageModule)
  },
  {
    path: 'estadisticas',
    loadChildren: () => import('./estadisticas/estadisticas.module').then( m => m.EstadisticasPageModule)
  }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {}
