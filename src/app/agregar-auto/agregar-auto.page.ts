import { Component, OnInit } from '@angular/core';
import { NavController, LoadingController, ToastController } from '@ionic/angular';
import { Camera, CameraOptions } from '@ionic-native/camera/ngx';
import { ServiciosService } from '../servicios/servicios.service';
import * as moment from 'moment';

@Component({
  selector: 'app-agregar-auto',
  templateUrl: './agregar-auto.page.html',
  styleUrls: ['./agregar-auto.page.scss'],
})
export class AgregarAutoPage implements OnInit {
  imgOficial: string;
  fotografias: any;
  modelos: any;
  marcas: any;
  marca: any;
  modelo: any;
  nombre: any;
  ciudad: any;
  tipo_vehiculo: any;
  color: any;
  combustible:any;
  caja: any;
  tipoVehiculo: any;
  success: any;
  puertas: any;
  precio: any;
  caracteristicas: any;
  anho: any;
  cilindrada: any;
  userId: string;
  constructor(public navCtrl: NavController,
  private camera: Camera,
  private service: ServiciosService,
  public loadingCtrl: LoadingController,
  public toastCtrl: ToastController
  ) { }

  ngOnInit() {
    this.loadMarcas();
    this.loadTiposVehiculo();
  }

  loadTiposVehiculo(){
    this.service.getTipoVehiculo().subscribe(async data => {
        this.tipoVehiculo = data.data;     
        console.log(this.tipoVehiculo);
      }, err => {
    })
  }

  async verifyMarca(e){
    console.log(e);
    if (!e.detail.value) {
      const toast = await this.toastCtrl.create({
        message: "Seleccionar Marca",
        duration: 2000,
        cssClass: 'my-custom-class',
      });
      toast.present();
    }
  }

  callModelos(e){
    if (e.detail.value) {
      console.log("entre aqui a marcas on select");
      
      this.loadModelos(this.marca);
    } 
  }

  async loadMarcas(){
      const loading = await this.loadingCtrl.create({
        message: 'Cargando...',
      });
      await loading.present().then(()=>{
      this.service.getMarcas().subscribe(async data => {
          this.marcas = data.data;     
          console.log(this.marcas);
          
          await loading.dismiss();
        }, err => {
          console.log(err);
         loading.dismiss();
        })
      });
  }

  async loadModelos(id){
    const loading = await this.loadingCtrl.create({
      message: 'Cargando modelos',
    });
    await loading.present().then(()=>{
    this.service.getModelos(id).subscribe(async data => {
        this.modelos = data.data;     
        console.log(this.modelos);
        
        await loading.dismiss();
      }, err => {
        console.log(err);
       loading.dismiss();
      })
    });
}

  public guardar() {
      this.navCtrl.navigateRoot('/perfil-usuario');
  }

  takephoto() {
    const options: CameraOptions = {
      quality: 100,
      destinationType: this.camera.DestinationType.DATA_URL,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE,
      correctOrientation: true,
      allowEdit: true,
      // targetHeight: 400,
      // targetWidth: 1020 
    }
    
    this.camera.getPicture(options).then((imageData) => {
    this.imgOficial = "data:image/jpeg;base64," + imageData;
     this.service.addPhotography(this.imgOficial);
     this.loadFotografias();
    }, (err) => {
    });
  }
 

  getimage() {
    const options: CameraOptions = {
      quality: 100,
      destinationType: this.camera.DestinationType.DATA_URL,
      sourceType: this.camera.PictureSourceType.PHOTOLIBRARY,
      saveToPhotoAlbum:false,
      correctOrientation: true,
      allowEdit: true,
      // targetHeight: 300,
      // targetWidth: 1020 
    }
    
    this.camera.getPicture(options).then((imageData) => {
    this.imgOficial = "data:image/jpeg;base64," + imageData;
    this.service.addPhotography(this.imgOficial);
    this.loadFotografias();
    }, (err) => {
    });
  }

  loadFotografias(){
    this.service.loadAll().then(result => {
      this.fotografias = result;      
    });
  }

  removeItem(ndx){
    this.service.removeItem(ndx);
  }

  addVehiculoVecom(){
    this.userId = localStorage.getItem('idUsuario');
    this.service.addVehiculoVecom(this.nombre,this.color,this.puertas,this.cilindrada,this.combustible,this.caracteristicas,this.ciudad,this.precio,this.anho,this.marca,this.modelo,this.tipo_vehiculo,this.userId, this.fotografias).subscribe(data => {
      this.success = data.data.data;
      console.log(this.success);
      this.succesSms(this.success);
     this.navCtrl.navigateRoot('/perfil-usuario');
    }, err => {
      console.log(err);
      if (err.status == 422) {
        this.errorSms(err.error.data.data);
      } else {
        this.errorConexion();
      }
    });
  }

  async errorConexion() {
    const toast = await this.toastCtrl.create({
      message: "error de conexion",
      duration: 3000,
      cssClass: 'my-custom-class',
    });
    toast.present();
  }

  async succesSms(msm) {
    const toast = await this.toastCtrl.create({
      message: msm,
      duration: 4000,
      cssClass: 'my-custom-class-success',
    });
    toast.present();
  }

  async errorSms(msm) {
    const toast = await this.toastCtrl.create({
      message: msm,
      duration: 4000,
      cssClass: 'my-custom-class',
    });
    toast.present();
  }
}
