import { PasswordValidator } from './../../../validators/password.validators';
import { UsernameValidator } from './../../../validators/username.validators';
import { Component, OnInit } from '@angular/core';
import { Validators, FormBuilder, FormGroup, FormControl } from '@angular/forms';

import { Router, ActivatedRoute } from '@angular/router';
import { NavController, MenuController, ToastController, AlertController, LoadingController } from '@ionic/angular';
import { ServiciosService } from '../servicios/servicios.service';

@Component({
  selector: 'app-registrarse',
  templateUrl: './registrarse.page.html',
  styleUrls: ['./registrarse.page.scss'],
})
export class RegistrarsePage implements OnInit {
  validations_form: FormGroup;
  matching_passwords_group: FormGroup;
  country_phone_group: FormGroup;
  data: any;
  validation: string;
  routes: string;
  player_id: any;

  constructor(
    public formBuilder: FormBuilder,
    private service: ServiciosService,
    public navCtrl: NavController,
    public menuCtrl: MenuController,
    public toastCtrl: ToastController,
    public alertCtrl: AlertController,
    public loadingCtrl: LoadingController,
    public route: ActivatedRoute 
  ) { }

  ngOnInit() {
    this.matching_passwords_group = new FormGroup({
      password: new FormControl('', Validators.compose([
        Validators.minLength(5),
        Validators.required,
        Validators.pattern('^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])[a-zA-Z0-9]+$')
      ])),
      confirm_password: new FormControl('', Validators.required)
    }, (formGroup: FormGroup) => {
      return PasswordValidator.areEqual(formGroup);
    });
    this.validations_form = this.formBuilder.group({
      username: new FormControl('', Validators.compose([
        UsernameValidator.validUsername,
        Validators.maxLength(25),
        Validators.minLength(5),
        Validators.pattern('^(?=.*[a-zA-Z])(?=.*[0-9])[a-zA-Z0-9]+$'),
        Validators.required
      ])),
      nombre: new FormControl('', Validators.required),
      ciudad: new FormControl('', Validators.required),
      telefono: new FormControl('', Validators.required),

      // lastname: new FormControl('', Validators.required),
      email: new FormControl('', Validators.compose([
        Validators.required,
        Validators.pattern('^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+$')
      ])),
      matching_passwords: this.matching_passwords_group,
    });
  }

  validation_messages = {
    'username': [
      { type: 'required', message: 'Username es requerido.' },
      { type: 'minlength', message: 'El nombre de usuario debe tener al menos 5 caracteres.' },
      { type: 'maxlength', message: 'El nombre de usuario no puede tener más de 25 caracteres.' },
      { type: 'pattern', message: 'Tu nombre de usuario debe contener solo números y letras.' },
      // { type: 'validUsername', message: 'Your username has already been taken.' }
    ],
    'nombre': [
      { type: 'required', message: 'Nombre es requerido.' },
      {type:'int',message:"numero campo"}
    ],

 
    'email': [
      { type: 'required', message: 'Email es requerido.' },
      { type: 'pattern', message: 'Por favor introduzca una dirección de correo electrónico válida.' }
    ],
  
    'password': [
      { type: 'required', message: 'Password es requerido.' },
      { type: 'minlength', message: 'La contraseña debe tener al menos 5 caracteres.' },
      { type: 'pattern', message: 'Su contraseña debe contener al menos una mayúscula, una minúscula y un número.' }
    ],
    'confirm_password': [
      { type: 'required', message: 'Confirmar password es requerido.' }
    ],
    'matching_passwords': [
      { type: 'areEqual', message: 'Contraseña no coincide.' }
    ],
  
  };

  onSubmit(d){
    console.log(d);
    
    // this.player_id = localStorage.getItem("player-id");
    this.service.registroVecom(d.nombre,d.telefono,d.email,d.matching_passwords['password']).subscribe(async data=>{
      this.data = data.data;
      console.log(this.data);
      localStorage.setItem('access_token', this.data.access_token);
      localStorage.setItem('idUsuario', this.data.id); 
      localStorage.setItem('celular', this.data.telefono_celular); 
      this.confirmation(this.data.nombre_completo);  
      this.navCtrl.navigateRoot('/home');
    },err =>{
      console.log(err);
      if (err.status == '0') {
        this.errorConexion();
      } else {
        this.presentToast(err.error.data.message);
      }
    })
  }

  async confirmation(name) {      
    const toast = await this.toastCtrl.create({
      message: 'Felicidades '+ name +' usted es usuario de VECOM.',
      cssClass:"my-custom-class-success",
    });
    // this.welcome(nombre);
        await toast.present();
  }
  async errorConexion() {
    const toast = await this.toastCtrl.create({
      message: "Error de conexión",
      duration: 4000,
      cssClass:"my-custom-class-alert"
    });
    toast.present();
  }

  async presentToast(msm) {
    const toast = await this.toastCtrl.create({
      message: msm,
      duration: 4000,
      cssClass:"my-custom-class"

    });
    toast.present();
  }

}
