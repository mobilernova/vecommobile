import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { PublicacionesUsuarioPageRoutingModule } from './publicaciones-usuario-routing.module';

import { PublicacionesUsuarioPage } from './publicaciones-usuario.page';
import { ComponentsModule } from '../components/components.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ComponentsModule,
    PublicacionesUsuarioPageRoutingModule
  ],
  declarations: [PublicacionesUsuarioPage]
})
export class PublicacionesUsuarioPageModule {}
