import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PublicacionesUsuarioPage } from './publicaciones-usuario.page';

const routes: Routes = [
  {
    path: '',
    component: PublicacionesUsuarioPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PublicacionesUsuarioPageRoutingModule {}
