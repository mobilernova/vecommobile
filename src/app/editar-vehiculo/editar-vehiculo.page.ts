import { Component, OnInit } from '@angular/core';
import { ServiciosService } from '../servicios/servicios.service';
import { ActivatedRoute, Router } from '@angular/router';
import { LoadingController, ToastController } from '@ionic/angular';

@Component({
  selector: 'app-editar-vehiculo',
  templateUrl: './editar-vehiculo.page.html',
  styleUrls: ['./editar-vehiculo.page.scss'],
})
export class EditarVehiculoPage implements OnInit {
  arrayPosts={id:"",nombre:"",puertas:"",cilindrada:"",tipo_combustible:"",caracteristicas:"",ciudad:"",precio:"",anho:"",marca:"",modelo:"",tipovehiculo:"",color:"",caja:"",imagenes_autos:[{}]};
  data: any;
  modelos: any;
  marcas: any;
  marca: any;
  modelo: any;
  nombre: any;
  ciudad: any;
  tipo_vehiculo: any;
  color: any;
  combustible:any;
  caja: any;
  tipoVehiculo: any;
  success: any;
  puertas: any;
  precio: any;
  caracteristicas: any;
  anho: any;
  cilindrada: any;
  userId: string;
  constructor(private service:ServiciosService,
  public route:ActivatedRoute,
  public loadingController:LoadingController,
  public toastCtrl:ToastController) {
    this.data = this.route.snapshot.paramMap.get('detalleAutoVecom');
   }

  async ngOnInit() {
    this.loadMarcas();
    this.loadTiposVehiculo();
    this.loadInformacion();
  }

  loadTiposVehiculo(){
    this.service.getTipoVehiculo().subscribe(async data => {
        this.tipoVehiculo = data.data;     
        console.log(this.tipoVehiculo);
      }, err => {
    })
  }

  async verifyMarca(e){
    console.log(e);
    if (!e.detail.value) {
      const toast = await this.toastCtrl.create({
        message: "Seleccionar Marca",
        duration: 2000,
        cssClass: 'my-custom-class',
      });
      toast.present();
    }
  }
  async loadMarcas(){
    this.service.getMarcas().subscribe(async data => {
        this.marcas = data.data;  
      }, err => {
      })
  }

  callModelos(e){
    if (e.detail.value) {
      console.log("entre aqui a marcas on select");
      
      this.loadModelos(this.marca);
    } 
  }

  async loadInformacion(){
    const loading = await this.loadingController.create({
      message: 'Cargando...',
    });
    await loading.present().then(() => {
    this.service.getEditarAutomovil(this.data).subscribe(
      async data => {
        this.arrayPosts = data['data'];
        this.nombre = this.arrayPosts.nombre;
        this.ciudad = this.arrayPosts.ciudad;
        this.tipo_vehiculo = this.arrayPosts.tipovehiculo;
        this.marca = this.arrayPosts.marca;
        this.modelo = this.arrayPosts.modelo;
        this.color = this.arrayPosts.color;
        this.cilindrada = this.arrayPosts.cilindrada;
        this.combustible = this.arrayPosts.tipo_combustible;
        this.caja = this.arrayPosts.caja;
        this.anho = this.arrayPosts.anho;
        this.caracteristicas = this.arrayPosts.caracteristicas;
        this.precio = this.arrayPosts.precio;
        this.puertas = this.arrayPosts.puertas;

        console.log(this.arrayPosts);
        await loading.dismiss();
      }, err => {
        console.log(err);
        loading.dismiss();
      });
    });
  }

  async loadModelos(id){
    const loading = await this.loadingController.create({
      message: 'Cargando modelos',
    });
    await loading.present().then(()=>{
    this.service.getModelos(id).subscribe(async data => {
        this.modelos = data.data;             
        await loading.dismiss();
      }, err => {
       this.errorSms(err.data.data)
       loading.dismiss();
      })
    });
  }

  editarVehiculoVecom(){
    this.userId = localStorage.getItem('idUsuario');
    this.service.editVehiculoVecom(this.data,this.nombre,this.color,this.puertas,this.cilindrada,this.combustible,this.caracteristicas,this.ciudad,this.precio,this.anho,this.marca,this.modelo,this.tipo_vehiculo,this.caja, this.userId).subscribe(data => {
      this.success = data.data;
      console.log(this.success);
      
      this.succesSms(this.success.data);
    //  this.navCtrl.navigateRoot('/perfil-usuario');
    }, err => {
      console.log(err);
      this.errorSms(err.error.data.data)

      // if (err.status == 401) {
      //   this.errorSms(err.error.data.message);
      // } else {
      //   this.errorConexion();
      // }
    });
  }

  async succesSms(msm) {
    const toast = await this.toastCtrl.create({
      message: msm,
      duration: 4000,
      cssClass: 'my-custom-class-success',
    });
    toast.present();
  }

  async errorSms(msm) {
    const toast = await this.toastCtrl.create({
      message: msm,
      duration: 4000,
      cssClass: 'my-custom-class',
    });
    toast.present();
  }

}
