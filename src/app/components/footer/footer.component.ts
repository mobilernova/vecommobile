import { Component, OnInit } from '@angular/core';
import { NavController, LoadingController } from '@ionic/angular';
import { PublicidadService } from 'src/app/servicios/publicidad.service';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss'],
})
export class FooterComponent implements OnInit {
  arrayPosts: any;
  slideOpts = {
    autoplay: {
      delay: 5000,
    },
    initialSlide: 1,
    speed: 300
  };
  constructor(public navCtrl: NavController,
  public loadingController:LoadingController,
  private service: PublicidadService)
   { }

    ngOnInit() {
    this.loadPublicity();
    }
    async loadPublicity(){
      const loading = await this.loadingController.create({
        message: 'Cargando...',
      });
      await loading.present().then(() => {
      this.service.listaPublicidadesVecom().subscribe(
        async data => {
          // this.arrayPosts = data;
          this.arrayPosts = data.data;
          console.log(this.arrayPosts);
          await loading.dismiss();
          // this.msjToast();
        }, err => {
          console.log(err);
          loading.dismiss();
        });
      });
    }

  // public publicidad(){
  //   this.navCtrl.navigateRoot('/detalle-publicidad');
  // }
  
}
