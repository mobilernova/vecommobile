import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { AgregarFotografiaPageRoutingModule } from './agregar-fotografia-routing.module';

import { AgregarFotografiaPage } from './agregar-fotografia.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    AgregarFotografiaPageRoutingModule
  ],
  declarations: [AgregarFotografiaPage]
})
export class AgregarFotografiaPageModule {}
