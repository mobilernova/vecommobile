import { Component, OnInit, Input } from '@angular/core';
import { Camera, CameraOptions } from '@ionic-native/camera/ngx';
import { LoadingController,NavController,ModalController} from '@ionic/angular';
import { ServiciosService } from 'src/app/servicios/servicios.service';

@Component({
  selector: 'app-agregar-fotografia',
  templateUrl: './agregar-fotografia.page.html',
  styleUrls: ['./agregar-fotografia.page.scss'],
})
export class AgregarFotografiaPage implements OnInit {
  imgOficial = 'assets/images/notfound.jpg';
  buttonOn = true;
  idVehiculo: string;
  constructor(private modalController: ModalController, 
  private navCtrl: NavController,
  private camera: Camera,
  public loadingCtrl: LoadingController, 
  private service: ServiciosService) { }

  ngOnInit() {
    this.idVehiculo = localStorage.getItem("preventIdAutomovil");
  }

  takephoto() {
    const options: CameraOptions = {
      quality: 100,
      destinationType: this.camera.DestinationType.DATA_URL,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE,
      correctOrientation: true,
      allowEdit: true,
      // targetHeight: 400,
      // targetWidth: 1020 
    }
    
    this.camera.getPicture(options).then((imageData) => {
    this.imgOficial = "data:image/jpeg;base64," + imageData;
    this.buttonOn = false;
    }, (err) => {
    });
  }
 

  getimage() {
    const options: CameraOptions = {
      quality: 100,
      destinationType: this.camera.DestinationType.DATA_URL,
      sourceType: this.camera.PictureSourceType.PHOTOLIBRARY,
      saveToPhotoAlbum:false,
      correctOrientation: true,
      allowEdit: true,
      // targetHeight: 400,
      // targetWidth: 1020 

    }
    
    this.camera.getPicture(options).then((imageData) => {
    this.imgOficial = "data:image/jpeg;base64," + imageData;
    this.buttonOn = false;
    }, (err) => {
    });
  }

  async saveFotografia(){
    const loading = await this.loadingCtrl.create({
        message: 'Agregando...',
      });
      await loading.present().then(()=>{
        this.service.anhadirFotografiaVehiculo(this.idVehiculo, this.imgOficial).subscribe(async data=>{
        await loading.dismiss();
        this.modalController.dismiss();
        this.navCtrl.navigateRoot("/galleria-automovil");
      },err =>{
        loading.dismiss();
        this.message();
      })
    });
  }

  async message() {
    const loading = await this.loadingCtrl.create({
      message: 'Error de Conexion',
    });
    await loading.present();
    const { role, data } = await loading.onDidDismiss();
  }
  
  back(){
    this.modalController.dismiss();
  }

}
