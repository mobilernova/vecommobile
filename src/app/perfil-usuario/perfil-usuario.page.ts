import { Component, ViewChild } from '@angular/core';
import { NavController, LoadingController, ToastController } from '@ionic/angular';
import { ServiciosService } from '../servicios/servicios.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-perfil-usuario',
  templateUrl: './perfil-usuario.page.html',
  styleUrls: ['./perfil-usuario.page.scss'],
})
export class PerfilUsuarioPage {
  @ViewChild('lista',{static: false}) lista;
  img = 'assets/images/perfil.jpg'
  imgNotCar = 'assets/images/notfound.jpg'
  arrayPosts: any;
  data ={nombre_completo:"", email:'',celular:"", foto:""};
  categoria: string;
  misPublicaciones: any;
  misFavoritos: any;
  success: any;

  constructor(public navCtrl: NavController,
              public service: ServiciosService,
              public loadingCtrl: LoadingController,
              public toastController: ToastController,
              public router: Router
    ) { }

    ionViewWillEnter(){
      this.loadPerfilUsuario();
      this.categoria = "publicaciones"
      this.loadMisPublicaciones();
    }

    itemSave(e){
      console.log(e);
      if (e.detail.value == "publicaciones") {
        this.loadMisPublicaciones();  
      } else {
        this.loadMisFavoritos();  
      }
      
    }
  
    async loadPerfilUsuario(){
      // const loading = await this.loadingCtrl.create({
      //   message: 'Cargando...',
      // });
      // await loading.present().then(()=>{
      this.service.perfilUsuario().subscribe(async data => {
          this.data = data.data;   
          console.log(this.data);
          
          // await loading.dismiss();
        }, err => {
          console.log(err);
        //  loading.dismiss();
        })
      // });
    }

    async loadMisPublicaciones(){
      const loading = await this.loadingCtrl.create({
        message: 'Cargando mis publicaciones',
      });
      await loading.present().then(()=>{
      this.service.getMisPublicaciones().subscribe(async data => {
          this.misPublicaciones = data.data;  
          console.log(this.misPublicaciones);
             
          await loading.dismiss();
        }, err => {
          console.log(err);
         loading.dismiss();
        })
      });
    }

    async loadMisFavoritos(){
      const loading = await this.loadingCtrl.create({
        message: 'Cargando mis favoritos',
      });
      await loading.present().then(()=>{
      this.service.getMisFavoritos().subscribe(async data => {
          this.misFavoritos = data.data;  
          console.log(this.misFavoritos);
             
          await loading.dismiss();
        }, err => {
          console.log(err);
         loading.dismiss();
        })
      });
    }

    detallePublicacion(c){
      const idVehiculo = c.id;
      this.router.navigate(['detalle-auto', {detalleAutoVecom: idVehiculo}]);
    }

    // public Publicaciones() {
    //   this.navCtrl.navigateRoot('/publicaciones-usuario');
    // }
    // public Favoritos() {
    //   this.navCtrl.navigateRoot('/favoritos-usuario');
    // }
    public EditarPerfil() {
      this.router.navigate(['/editar-perfil']);
    }
    public logout(){
      localStorage.clear();
      this.navCtrl.navigateRoot('/inicio-sesion');
    }
    crearPublicacion(){
      this.router.navigate(['/agregar-auto']);
    }
    editarVehiculo(item){
      const idVehiculo = item;
      this.lista.closeSlidingItems();
      this.router.navigate(['editar-vehiculo', {detalleAutoVecom: idVehiculo}]);
    }

    galleriaAutomovil(item){
      localStorage.setItem("preventIdAutomovil", item);
      this.lista.closeSlidingItems();
      this.navCtrl.navigateRoot(['galleria-automovil']);
    }
    estadisticasVehiculo(item, estado){
      const idVehiculo = item;
      const estadoVehiculo = estado;
      this.lista.closeSlidingItems();      
      this.router.navigate(['estadisticas', {detalleAutoVecom: idVehiculo,estado:estadoVehiculo}]);
    }

    async deleteFavorito(id){
        this.service.eliminarFavoritoVehiculoVecom(id).subscribe(async data => {
          this.success = data.data;  
          this.succesSms(this.success)
          console.log(this.misFavoritos);
        }, err => {
          console.log(err);
          this.errorSms(err.error.data.message)
        })
    }
    async succesSms(msm) {
      const toast = await this.toastController.create({
        message: msm,
        duration: 4000,
        cssClass: 'my-custom-class-success',
      });
      toast.present();
      this.loadMisFavoritos();
    }
  
    async errorSms(msm) {
      const toast = await this.toastController.create({
        message: msm,
        duration: 4000,
        cssClass: 'my-custom-class',
      });
      toast.present();
    }
  }
