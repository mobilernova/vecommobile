import { Component, OnInit } from '@angular/core';
import { ActionSheetController } from '@ionic/angular';

@Component({
  selector: 'app-detalle-publicidad',
  templateUrl: './detalle-publicidad.page.html',
  styleUrls: ['./detalle-publicidad.page.scss'],
})
export class DetallePublicidadPage implements OnInit {

  constructor(public actionSheetCtrl: ActionSheetController) { }

  ngOnInit() {
  }
  async presentActionSheet() {
    const actionSheet = await this.actionSheetCtrl.create({
      backdropDismiss: false,
      buttons: [{
        text: 'Llamar',
        icon: 'call',
        handler: () => {
          console.log('Call clicked');
        }
      }, {
        text: 'Instagram',
        icon: 'logo-instagram',
        handler: () => {
          console.log('Instagram clicked');
        }
      }, {
        text: 'Facebook',
        icon: 'logo-facebook',
        handler: () => {
          console.log('Facebook clicked');
        }
      },{
        text: 'Cancelar',
        icon: 'close',
        role: 'cancelar',
        cssClass: 'secondary',
        handler: () => {
          console.log('Cancel clicked');
        }
      }]
    });

    await actionSheet.present();
  }

}
