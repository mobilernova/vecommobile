import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { FavoritosUsuarioPageRoutingModule } from './favoritos-usuario-routing.module';

import { FavoritosUsuarioPage } from './favoritos-usuario.page';
import { ComponentsModule } from '../components/components.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ComponentsModule,
    FavoritosUsuarioPageRoutingModule
  ],
  declarations: [FavoritosUsuarioPage]
})
export class FavoritosUsuarioPageModule {}
