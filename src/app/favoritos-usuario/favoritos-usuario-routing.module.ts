import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { FavoritosUsuarioPage } from './favoritos-usuario.page';

const routes: Routes = [
  {
    path: '',
    component: FavoritosUsuarioPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class FavoritosUsuarioPageRoutingModule {}
