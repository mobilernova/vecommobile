import { Component, OnInit } from '@angular/core';
import { NavController, LoadingController, ToastController } from '@ionic/angular';
import { ServiciosService } from '../servicios/servicios.service';

@Component({
  selector: 'app-favoritos-usuario',
  templateUrl: './favoritos-usuario.page.html',
  styleUrls: ['./favoritos-usuario.page.scss'],
})
export class FavoritosUsuarioPage implements OnInit {

  arrayPosts: any;

  constructor(public navCtrl: NavController,
              public service: ServiciosService,
              public loadingController: LoadingController,
              public toastController: ToastController,
    ) { }

  async ngOnInit() {
    const loading = await this.loadingController.create({
      message: 'Cargando...',
    });
    await loading.present().then(() => {
    this.service.getDatos().subscribe(
      async data => {
        this.arrayPosts = data;
        // this.categorias = data.data;
        console.log(data);
        await loading.dismiss();
        this.msjToast();
      }, err => {
        console.log(err);
        loading.dismiss();
      });
    });
    }
    async msjToast() {
      const toast = await this.toastController.create({
        message: 'Favoritos Cargados.',
        duration: 2000
      });
      toast.present();
    }
    public DetalleAuto() {
      this.navCtrl.navigateRoot('/detalle-auto');
    }
}
