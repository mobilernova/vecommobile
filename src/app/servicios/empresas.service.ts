import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, throwError, of } from 'rxjs';
import { map, tap, catchError } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class EmpresasService {
  url = 'https://jsonplaceholder.typicode.com/';
  path:string;
  accesToken: string;

  httpOptions: { headers: HttpHeaders; };
  httpOptions2: { headers: HttpHeaders; };
  page_size: number;

  constructor(private http: HttpClient) {
    this.path = 'https://vecom.academiamodernadallas.com/api';
    this.accesToken = localStorage.getItem('access_token');   
    this.page_size = 10;
   }

  /**
  * service empresas con automoviles
  */
 listaEmpresasVecom(page):Observable<any>{
  this.httpOptions = {
    headers: new HttpHeaders({ 
      'Content-Type': 'application/json', 
      'Authorization':'Bearer '+ this.accesToken
    }),
  };

  return this.http.get(this.path +"/empresa/empresas"+"?page="+ page +"&page_size="+this.page_size+"", this.httpOptions)
    .pipe(
      tap( (data:any)=>{
        return of(data);
      } ),
      catchError( (err)=>{
        return throwError(err);
      } )
    );
  }

    /**
  * service detalle de empresa con automoviles
  */
  detalleEmpresasVecom(id):Observable<any>{
  this.httpOptions = {
    headers: new HttpHeaders({ 
      'Content-Type': 'application/json', 
      'Authorization':'Bearer '+ this.accesToken
    }),
  };

  return this.http.get(this.path +"/empresa/detalleempresa"+"?id="+ id +"", this.httpOptions)
    .pipe(
      tap( (data:any)=>{
        return of(data);
      } ),
      catchError( (err)=>{
        return throwError(err);
      } )
    );
  }

 }
