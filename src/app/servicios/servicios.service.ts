import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, throwError, of, BehaviorSubject } from 'rxjs';
import { map, tap, catchError } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ServiciosService {
  path:string;
  url = 'https://jsonplaceholder.typicode.com/';
  accesToken: string;
  httpOptions: { headers: HttpHeaders; };
  httpOptions2: { headers: HttpHeaders; };
  private objectSource = new BehaviorSubject<{}>({});
  $getObjectSource = this.objectSource.asObservable();

  items = [];
  page_size: number;

  constructor(private http: HttpClient) { 
    this.path = 'https://vecom.academiamodernadallas.com/api';
    this.page_size = 10;
  }

  sendObjectSource(data: any) {
    this.objectSource.next(data);
  }
 
  getDatos(): Observable<any> {
    return this.http.get(this.url + 'users')    
    .pipe(
      tap((data: any) => {
      return of(data);
      }),
      catchError((err) => {
        console.log(err);
        return throwError(err);
      })
    );
  }

  /**
  * service login
  */
 loginVecom(value, email, nombre='', celular='', password='',foto=''):Observable<any>{
  this.httpOptions = {
    headers: new HttpHeaders({ 
      'Content-Type': 'application/json', 
    }),
  };
  var datoaEnviar = {
    "tipo_login":value,
    "email":email,
    "nombre_completo":nombre,
    "telefono_celular":celular,
    "password":password,
    "foto":foto
  }
  return this.http.post(this.path +"/user/login", datoaEnviar, this.httpOptions)
    .pipe(
      tap( (data:any)=>{
        return of(data);
      } ),
      catchError( (err)=>{
        return throwError(err);
      } )
    );
}

  /**
  * service Registro usuario
  */
 registroVecom(nombre,celular,email, password):Observable<any>{
  this.httpOptions = {
    headers: new HttpHeaders({ 
      'Content-Type': 'application/json', 
    }),
  };
  var datoaEnviar = {
    "nombre_completo":nombre,
    "email":email,
    "celular":celular,
    "password":password,
    // "player_id":player_id,
    // "foto":foto
  }
  return this.http.post(this.path +"/user/signup", datoaEnviar, this.httpOptions)
    .pipe(
      tap( (data:any)=>{
        return of(data);
      } ),
      catchError( (err)=>{
        return throwError(err);
      } )
    );
  }

  /**
  * service Perfil
  */

 perfilUsuario():Observable<any>{
  this.accesToken = localStorage.getItem('access_token');    
  this.httpOptions = {
    headers: new HttpHeaders({ 
      'Content-Type': 'application/json', 
      'Authorization':'Bearer '+ this.accesToken
    }),
  };
  return this.http.post(this.path +'/user/perfil',null, this.httpOptions)
    .pipe(
      tap( (data:any)=>{
        return of(data);
      } ),
      catchError( (err)=>{
        return throwError(err);
      } )
    );
  }

  /**
  * service buscar automovil
  */
 searchVecom(player_id, value, email, nombre='', celular='', password='',foto=''):Observable<any>{
  this.httpOptions = {
    headers: new HttpHeaders({ 
      'Content-Type': 'application/json', 
    }),
  };
  var datoaEnviar = {
    "tipo_login":value,
    "email":email,
    "nombre_completo":nombre,
    "telefono_celular":celular,
    "password":password,
    "player_id":player_id,
    "foto":foto
  }
  return this.http.post(this.path +"/user/login", datoaEnviar, this.httpOptions)
    .pipe(
      tap( (data:any)=>{
        return of(data);
      } ),
      catchError( (err)=>{
        return throwError(err);
      } )
    );
  }

  
  

  /**
  * service Editar Usuario
  */
 editarUsuarioVecom(nombre, email, celular, fecha_nacimiento,foto):Observable<any>{
  this.accesToken = localStorage.getItem('access_token');   
  this.httpOptions = {
    headers: new HttpHeaders({ 
      'Content-Type': 'application/json', 
      'Authorization':'Bearer '+ this.accesToken
    }),
  };
  var datoaEnviar = {
    "nombre_completo":nombre,
    "email":email,
    "celular":celular,
    "fecha_nacimiento":fecha_nacimiento,
    "foto":foto
  }
  return this.http.post(this.path +"/user/usuarioedit", datoaEnviar, this.httpOptions)
    .pipe(
      tap( (data:any)=>{
        return of(data);
      } ),
      catchError( (err)=>{
        return throwError(err);
      } )
    );
  }

  /*
  / Servicio de modelos
  */
 getModelos(id): Observable<any> {
  this.accesToken = localStorage.getItem('access_token');   
  this.httpOptions = {
    headers: new HttpHeaders({ 
      'Content-Type': 'application/json', 
      'Authorization':'Bearer '+ this.accesToken
    }),
  };
  return this.http.get(this.path + '/vehiculo/modelos/?marca_id='+id +'' , this.httpOptions)    
  .pipe(
    tap((data: any) => {
    return of(data);
    }),
    catchError((err) => {
      console.log(err);
      return throwError(err);
    })
  );
}

/*
  / Servicio de marcas
  */
 getMarcas(): Observable<any> {
  this.accesToken = localStorage.getItem('access_token');   
  this.httpOptions = {
    headers: new HttpHeaders({ 
      'Content-Type': 'application/json', 
      'Authorization':'Bearer '+ this.accesToken
    }),
  };
  return this.http.get(this.path + '/vehiculo/marcas', this.httpOptions)    
  .pipe(
    tap((data: any) => {
    return of(data);
    }),
    catchError((err) => {
      console.log(err);
      return throwError(err);
    })
  );
}

/*
  / Servicio de tipos vehiculos
  */
 getTipoVehiculo(): Observable<any> {
  this.accesToken = localStorage.getItem('access_token');   
  this.httpOptions = {
    headers: new HttpHeaders({ 
      'Content-Type': 'application/json', 
      'Authorization':'Bearer '+ this.accesToken
    }),
  };
  return this.http.get(this.path + '/vehiculo/tipovehiculo', this.httpOptions)    
  .pipe(
    tap((data: any) => {
    return of(data);
    }),
    catchError((err) => {
      console.log(err);
      return throwError(err);
    })
  );
}

addVehiculoVecom(nombre,color,puertas,cilindrada,tipo_combustible,caracteristicas,ciudad,precio,anho,marca_id,modelo_id,tipo_vehiculo_id,user_id,foto){
  this.accesToken = localStorage.getItem('access_token');   
  this.httpOptions = {
    headers: new HttpHeaders({ 
      'Content-Type': 'application/json', 
      'Authorization':'Bearer '+ this.accesToken
    }),
  };
  var datoaEnviar = {
    "nombre":nombre,
    "color": color,
    "puertas":puertas,
    "cilindrada":cilindrada,
    "tipo_combustible":tipo_combustible,
    "caracteristicas":caracteristicas,
    "ciudad": ciudad,
    "precio":precio,
    "año":anho,
    "marcas_id": marca_id,
    "empresa_id":"",
    "modelo_id":modelo_id,
    "tipo_vehiculo_id":tipo_vehiculo_id,
    "usuario_app_id":user_id,
    "datos":foto
  }
  return this.http.post(this.path +"/vehiculo/crearvehiculo", datoaEnviar, this.httpOptions)
    .pipe(
      tap( (data:any)=>{
        return of(data);
      } ),
      catchError( (err)=>{
        return throwError(err);
      } )
    );
}

editVehiculoVecom(id,nombre,color,puertas,cilindrada,tipo_combustible,caracteristicas,ciudad,precio,anho,marca_id,modelo_id,tipo_vehiculo_id,caja,user_id){
  this.accesToken = localStorage.getItem('access_token');   
  this.httpOptions = {
    headers: new HttpHeaders({ 
      'Content-Type': 'application/json', 
      'Authorization':'Bearer '+ this.accesToken
    }),
  };
  var datoaEnviar = {
    "nombre":nombre,
    "color": color,
    "puertas":puertas,
    "cilindrada":cilindrada,
    "tipo_combustible":tipo_combustible,
    "caracteristicas":caracteristicas,
    "ciudad": ciudad,
    "precio":precio,
    "anho":anho,
    "marcas_id": marca_id,
    "modelo_id":modelo_id,
    "tipo_vehiculo_id":tipo_vehiculo_id,
    "caja":caja,
    "usuario_app_id":user_id,

  }
  return this.http.post(this.path +"/vehiculo/editarvehiculo?id="+id+'', datoaEnviar, this.httpOptions)
    .pipe(
      tap( (data:any)=>{
        return of(data);
      } ),
      catchError( (err)=>{
        return throwError(err);
      } )
    );
}

addPhotography(foto){
  this.items.push(
   {"foto":foto});
}

loadAll(){
  return Promise.resolve(this.items);
};

removeItem(ndx) {
  this.items.splice(ndx, 1);
}

deleteItem(){
  this.items = [];
}

  /*
  / Servicio de mis publicaciones
  */
 getMisPublicaciones(): Observable<any> {
  this.accesToken = localStorage.getItem('access_token');   
  this.httpOptions = {
    headers: new HttpHeaders({ 
      'Content-Type': 'application/json', 
      'Authorization':'Bearer '+ this.accesToken
    }),
  };
  return this.http.get(this.path + '/vehiculo/publicaciones', this.httpOptions)    
  .pipe(
    tap((data: any) => {
    return of(data);
    }),
    catchError((err) => {
      console.log(err);
      return throwError(err);
    })
  );
}

  /*
  / Servicio de mis favoritos
  */
 getMisFavoritos(): Observable<any> {
  this.accesToken = localStorage.getItem('access_token');   
  this.httpOptions = {
    headers: new HttpHeaders({ 
      'Content-Type': 'application/json', 
      'Authorization':'Bearer '+ this.accesToken
    }),
  };
  return this.http.get(this.path + '/favorito/favoritos', this.httpOptions)    
  .pipe(
    tap((data: any) => {
    return of(data);
    }),
    catchError((err) => {
      console.log(err);
      return throwError(err);
    })
  );
}

/*
  / Servicio detalle automovil
  */
 getDetalleAutomovil(idAutomovil): Observable<any> {
  this.accesToken = localStorage.getItem('access_token');   
  this.httpOptions = {
    headers: new HttpHeaders({ 
      'Content-Type': 'application/json', 
      'Authorization':'Bearer '+ this.accesToken
    }),
  };

  return this.http.post(this.path + '/vehiculo/detalleauto?id='+idAutomovil+'',null, this.httpOptions)    
  .pipe(
    tap((data: any) => {
    return of(data);
    }),
    catchError((err) => {
      console.log(err);
      return throwError(err);
    })
  );
}

/*
  / Servicio editar automovil
  */
 getEditarAutomovil(idAutomovil): Observable<any> {
  this.accesToken = localStorage.getItem('access_token');   
  this.httpOptions = {
    headers: new HttpHeaders({ 
      'Content-Type': 'application/json', 
      'Authorization':'Bearer '+ this.accesToken
    }),
  };
  return this.http.get(this.path + '/vehiculo/edit?id='+idAutomovil+'', this.httpOptions)    
  .pipe(
    tap((data: any) => {
    return of(data);
    }),
    catchError((err) => {
      console.log(err);
      return throwError(err);
    })
  );
}

/*
  / Servicio de galleria vehiculo
  */
 galleriaVehiculo(id): Observable<any> {
  this.accesToken = localStorage.getItem('access_token');   
  this.httpOptions = {
    headers: new HttpHeaders({ 
      'Content-Type': 'application/json', 
      'Authorization':'Bearer '+ this.accesToken
    }),
  };
  return this.http.get(this.path + '/vehiculo/galeria?id='+id+'',  this.httpOptions)    
  .pipe(
    tap((data: any) => {
    return of(data);
    }),
    catchError((err) => {
      console.log(err);
      return throwError(err);
    })
  );
}

  /*
  / Servicio de eliminar fotografia vehiculo
  */
 eliminarFotografiaVehiculo(id): Observable<any> {
  this.accesToken = localStorage.getItem('access_token');   
  this.httpOptions = {
    headers: new HttpHeaders({ 
      'Content-Type': 'application/json', 
      'Authorization':'Bearer '+ this.accesToken
    }),
  };

  return this.http.post(this.path + '/vehiculo/quitar-imagen?id='+id+'',null, this.httpOptions)    
  .pipe(
    tap((data: any) => {
    return of(data);
    }),
    catchError((err) => {
      console.log(err);
      return throwError(err);
    })
  );
}

 /*
  / Servicio de añadir fotografia vehiculo
  */
 anhadirFotografiaVehiculo(idVehiculo, foto): Observable<any> {
    this.accesToken = localStorage.getItem('access_token');   
    this.httpOptions = {
      headers: new HttpHeaders({ 
        'Content-Type': 'application/json', 
        'Authorization':'Bearer '+ this.accesToken
      }),
    };
    var datoaEnviar = {
      "foto":foto,
    }
    return this.http.post(this.path + '/vehiculo/agregarfoto?id='+idVehiculo+'',datoaEnviar, this.httpOptions)    
    .pipe(
      tap((data: any) => {
      return of(data);
      }),
      catchError((err) => {
        console.log(err);
        return throwError(err);
      })
    );
  }


  /*
  / Servicio de estadisticas vehiculo
  */
 estadisticasVehiculoVecom(idAutomovil): Observable<any> {
  this.accesToken = localStorage.getItem('access_token');   
  this.httpOptions = {
    headers: new HttpHeaders({ 
      'Content-Type': 'application/json', 
      'Authorization':'Bearer '+ this.accesToken
    }),
  };

  return this.http.get(this.path + '/vehiculo/count?id='+idAutomovil+'', this.httpOptions)    
  .pipe(
    tap((data: any) => {
    return of(data);
    }),
    catchError((err) => {
      console.log(err);
      return throwError(err);
    })
  );
}

  /*
    / Servicio de cambiar estado de vehiculo
    */
  estadoVehiculoVecom(idAutomovil): Observable<any> {
    this.accesToken = localStorage.getItem('access_token');   
    this.httpOptions = {
      headers: new HttpHeaders({ 
        'Content-Type': 'application/json', 
        'Authorization':'Bearer '+ this.accesToken
      }),
    };

    return this.http.post(this.path + '/vehiculo/estado?id='+idAutomovil+'', null,  this.httpOptions)    
    .pipe(
      tap((data: any) => {
      return of(data);
      }),
      catchError((err) => {
        console.log(err);
        return throwError(err);
      })
    );
  }

    /*
  / Servicio de lista de  vehiculos
  */
 listaVehiculoVecom(page): Observable<any> {
  this.page_size = 6;

  this.accesToken = localStorage.getItem('access_token');   
  this.httpOptions = {
    headers: new HttpHeaders({ 
      'Content-Type': 'application/json', 
      'Authorization':'Bearer '+ this.accesToken
    }),
  };

  return this.http.get(this.path +"/vehiculo/vehiculos"+"?page="+ page +"&page_size="+this.page_size+"", this.httpOptions)
  .pipe(
    tap((data: any) => {
    return of(data);
    }),
    catchError((err) => {
      console.log(err);
      return throwError(err);
    })
  );
}

    /*
    / Servicio de crear favorito
    */
   anhadirFavoritoVehiculoVecom(idAutomovil): Observable<any> {
    this.accesToken = localStorage.getItem('access_token');   
    this.httpOptions = {
      headers: new HttpHeaders({ 
        'Content-Type': 'application/json', 
        'Authorization':'Bearer '+ this.accesToken
      }),
    };

    return this.http.post(this.path + '/favorito/create-favorito?id='+idAutomovil+'', null,  this.httpOptions)    
    .pipe(
      tap((data: any) => {
      return of(data);
      }),
      catchError((err) => {
        console.log(err);
        return throwError(err);
      })
    );
  }

  /*
    / Servicio de eliminar favorito
    */
   eliminarFavoritoVehiculoVecom(idAutomovil): Observable<any> {
    this.accesToken = localStorage.getItem('access_token');   
    this.httpOptions = {
      headers: new HttpHeaders({ 
        'Content-Type': 'application/json', 
        'Authorization':'Bearer '+ this.accesToken
      }),
    };

    return this.http.post(this.path + '/favorito/delete-favorito?id='+idAutomovil+'', null,  this.httpOptions)    
    .pipe(
      tap((data: any) => {
      return of(data);
      }),
      catchError((err) => {
        console.log(err);
        return throwError(err);
      })
    );
  }
}