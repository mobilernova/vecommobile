import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, throwError, of } from 'rxjs';
import { map, tap, catchError } from 'rxjs/operators';
// http://c1710985.ferozo.com/api/publicidad/publicidades?page=1&page_size=10  method GET

@Injectable({
  providedIn: 'root'
})
export class PublicidadService {
  path:string;
  accesToken: string;

  httpOptions: { headers: HttpHeaders; };
  httpOptions2: { headers: HttpHeaders; };
  page_size: number;

  constructor(private http: HttpClient) {
    this.path = 'https://vecom.academiamodernadallas.com/api';
  }

  /**
  * service empresas con automoviles
  */
 listaPublicidadesVecom():Observable<any>{
  this.accesToken = localStorage.getItem('access_token');   
  this.httpOptions = {
    headers: new HttpHeaders({ 
      'Content-Type': 'application/json', 
      'Authorization':'Bearer '+ this.accesToken
    }),
  };

  return this.http.post(this.path +"/vehiculo/prueba", null, this.httpOptions)
    .pipe(
      tap( (data:any)=>{
        return of(data);
      } ),
      catchError( (err)=>{
        return throwError(err);
      } )
    );
  }

  
  }
