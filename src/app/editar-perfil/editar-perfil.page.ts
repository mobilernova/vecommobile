import { Component, OnInit } from '@angular/core';
import { NavController, LoadingController, ToastController, AlertController,ActionSheetController } from '@ionic/angular';
import { Camera, CameraOptions } from '@ionic-native/camera/ngx';
import { ServiciosService } from '../servicios/servicios.service';
@Component({
  selector: 'app-editar-perfil',
  templateUrl: './editar-perfil.page.html',
  styleUrls: ['./editar-perfil.page.scss'],
})
export class EditarPerfilPage implements OnInit {
  imageOficial = "assets/images/perfil.jpg"
  data ={nombre_completo:"",fecha_nacimiento:"" ,email:'',celular:"", foto:""};

  constructor( private navCtrl: NavController,
    public loadingController: LoadingController,
    public toastCtrl: ToastController,
    public alertController: AlertController,
    public actionSheetController: ActionSheetController,
    private camera: Camera,
    private service: ServiciosService) 
    { }

  ngOnInit() {

    this.service.perfilUsuario().subscribe(async data => {
      this.data = data.data;     
    }, err => {
    })
  }  

  async guardar(){
    // this.navCtrl.navigateRoot('/perfil-usuario');
    const loading = await this.loadingController.create({
      message: 'Editando información....',
      // duration: 3000
    });
    await loading.present().then(() => {
      this.service.editarUsuarioVecom(this.data.nombre_completo,this.data.email,this.data.celular,this.data.fecha_nacimiento, this.data.foto).subscribe(
        async data => {
          this.succesSms(data.data.data);
          this.navCtrl.navigateRoot('/perfil-usuario');
          await loading.dismiss();          
        }, err => {
          this.errorRequest(err.error.data.message);
          loading.dismiss();
        }
      );
    });  
  }

  async succesSms(msm) {
    const toast = await this.toastCtrl.create({
      message:  msm,
      duration: 4000,
      cssClass: 'my-custom-class-success',
    });
    toast.present();
  }

  async errorRequest(sms){
    const toast = await this.toastCtrl.create({
      message: sms,
      duration: 3000,
      cssClass:"my-custom-class-alert"
    });
    toast.present();
}

  async changePhoto(){
    const actionSheet = await this.actionSheetController.create({
      header: 'Fotografias',
      buttons: [{
        text: 'Tomar fotografia',
        icon: 'camera',
        handler: () => {
          const options: CameraOptions = {
            quality: 100,
            destinationType: this.camera.DestinationType.DATA_URL,
            encodingType: this.camera.EncodingType.JPEG,
            mediaType: this.camera.MediaType.PICTURE,
            correctOrientation: true,
            allowEdit: false,
            targetHeight: 300,
            targetWidth: 300 
          }
          this.camera.getPicture(options).then((imageData) => {
          this.data.foto = "data:image/jpeg;base64," + imageData;
          }, (err) => {
          });
        }
      }, {
        text: 'Galleria',
        icon: 'images',
        handler: () => {
          const options: CameraOptions = {
            quality: 100,
            destinationType: this.camera.DestinationType.DATA_URL,
            sourceType: this.camera.PictureSourceType.PHOTOLIBRARY,
            saveToPhotoAlbum:false,
            correctOrientation: true,
            allowEdit: false,
            targetHeight: 300,
            targetWidth: 300 
          }      
          this.camera.getPicture(options).then((imageData) => {
          this.data.foto = "data:image/jpeg;base64," + imageData;
          }, (err) => {            
          });
        }
      },{
        text: 'Cancel',
        icon: 'close',
        role: 'cancel',
        handler: () => {
    
        }
      }]
    });
    await actionSheet.present();
  }
}
