import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { GalleriaAutomovilPage } from './galleria-automovil.page';

describe('GalleriaAutomovilPage', () => {
  let component: GalleriaAutomovilPage;
  let fixture: ComponentFixture<GalleriaAutomovilPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GalleriaAutomovilPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(GalleriaAutomovilPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
