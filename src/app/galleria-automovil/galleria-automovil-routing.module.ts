import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { GalleriaAutomovilPage } from './galleria-automovil.page';

const routes: Routes = [
  {
    path: '',
    component: GalleriaAutomovilPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class GalleriaAutomovilPageRoutingModule {}
