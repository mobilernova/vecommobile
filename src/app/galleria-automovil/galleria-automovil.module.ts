import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { GalleriaAutomovilPageRoutingModule } from './galleria-automovil-routing.module';

import { GalleriaAutomovilPage } from './galleria-automovil.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    GalleriaAutomovilPageRoutingModule
  ],
  declarations: [GalleriaAutomovilPage]
})
export class GalleriaAutomovilPageModule {}
