import { ActivatedRoute,Router } from '@angular/router';
// import { MainService } from './../services/main.service';
import { Component, OnInit } from '@angular/core';
import { NavController, LoadingController, ModalController, AlertController, ToastController, Platform } from '@ionic/angular';
import { ServiciosService } from '../servicios/servicios.service';

@Component({
  selector: 'app-galleria-automovil',
  templateUrl: './galleria-automovil.page.html',
  styleUrls: ['./galleria-automovil.page.scss'],
})
export class GalleriaAutomovilPage {
  userId: string;
  imageAllProducto:any;
  idVehiculo: string;
  smsSuccess: any;
  smsSuccessTrue: any;

  constructor(public modalController: ModalController,
    public alertController: AlertController,
    private service: ServiciosService,
    private route: Router,
    public navCtrl: NavController,
    public loadingCtrl: LoadingController,
    public toastCtrl: ToastController,
    public platform: Platform
    ) {
    this.platform.backButton.subscribe(() => {
      this.navCtrl.navigateRoot('/perfil-usuario')
      localStorage.removeItem('preventIdAutomovil');
    });
  }

  ionViewWillEnter(){
     this.galeria();
  }

  async galeria() {
    this.idVehiculo = localStorage.getItem('preventIdAutomovil');
    const loading = await this.loadingCtrl.create({
      message: 'Cargando...',
    });
    await loading.present().then(() => {
      this.service.galleriaVehiculo(this.idVehiculo).subscribe(async data => {
        this.imageAllProducto = data.data;
        console.log(this.imageAllProducto);
        
        await loading.dismiss();
      }, err => {
        loading.dismiss();
        this.message();
      })
    });
  }

  async deleteFotography(id) {    
    const alert = await this.alertController.create({
      header: 'Eliminar',
      message: '<strong>Esta seguro de eliminar esta fotografia?</strong>!!!',
      buttons: [
        {
          text: 'Si',
          // role: 'cancel',
          // cssClass: 'secondary',
          handler: (blah) => {
            this.service.eliminarFotografiaVehiculo(id).subscribe(async data => {
              this.smsSuccessTrue = data.data;
              console.log(this.smsSuccessTrue);
              
              this.successRequest(this.smsSuccessTrue.data);
              this.galeria();
            }, err => {
              this.message();
            })
          }
        }, {
          text: 'No',
          role: 'cancel',
          handler: () => {
          }
        }
      ]
    });

    await alert.present();
  }

async message() {
  const loading = await this.loadingCtrl.create({
    message: 'Error de Conexion'
  });
  await loading.present();
  const { role, data } = await loading.onDidDismiss();
}

async successRequest(sms){
  const toast = await this.toastCtrl.create({
    message: sms ,
    duration: 1000,
    cssClass:"my-custom-class-success"
  });
  toast.present();
}


// async addImage() {
//   const modal = await this.modalController.create({
//     component: AnhadirFotografiaComponent,
//     componentProps:{
//       idVehiculo:this.idVehiculo
//     },
//     keyboardClose: true,
//     showBackdrop: true
//   });
//   return await modal.present();
// }

addImage(){
    // localStorage.setItem("preventIdAutomovil", item);
    this.route.navigate(['/agregar-fotografia']);
}
back(){
  this.navCtrl.navigateRoot('/perfil');
  localStorage.removeItem('preventIdAutomovil');
}

}
