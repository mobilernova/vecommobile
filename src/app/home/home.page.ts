import { Component } from '@angular/core';
import { NavController, MenuController } from '@ionic/angular';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {

  constructor(public navCtrl: NavController,
    public menuCtrl:MenuController
    ) {
      this.menuCtrl.enable(true)
    }

  public AutoventaPage(){
    this.navCtrl.navigateRoot('/autoventa');
  }
  public DetalleEmpresaPage(){
    this.navCtrl.navigateRoot('/empresas');
  }
  public BusquedasPage(){
    this.navCtrl.navigateRoot('/busquedas');
  }
  public EditarPerfilPage(){
    this.navCtrl.navigateRoot('/perfil-usuario');
  }
}
