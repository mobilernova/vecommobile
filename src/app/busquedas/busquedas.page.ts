import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';

@Component({
  selector: 'app-busquedas',
  templateUrl: './busquedas.page.html',
  styleUrls: ['./busquedas.page.scss'],
})
export class BusquedasPage implements OnInit {
  public minmaxprice = {
    upper: 100000,
    lower: 5000
  };

  constructor(private modalCtrl: ModalController) { }

  ngOnInit() {
  }

  buscar() {
    this.modalCtrl.dismiss();
  }

}
