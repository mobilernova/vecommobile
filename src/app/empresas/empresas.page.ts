import { Component, OnInit } from '@angular/core';
import { NavController, LoadingController, ToastController } from '@ionic/angular';
import { EmpresasService } from '../servicios/empresas.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-empresas',
  templateUrl: './empresas.page.html',
  styleUrls: ['./empresas.page.scss'],
})
export class EmpresasPage implements OnInit {

  arrayPosts: any;
  page: number;

  constructor(public navCtrl: NavController,
              public service: EmpresasService,
              public loadingController: LoadingController,
              public toastController: ToastController,
              public router:Router
    ) { }

  async ngOnInit() {
    this.page = 1;
    const loading = await this.loadingController.create({
      message: 'Cargando...',
    });
    await loading.present().then(() => {
    this.service.listaEmpresasVecom(this.page).subscribe(
      async data => {
        this.arrayPosts = data.data;
        // this.categorias = data.data;
        console.log(this.arrayPosts);
        await loading.dismiss();
        this.msjToast();
      }, err => {
        console.log(err);
        loading.dismiss();
      });
    });
    }
    async msjToast() {
      const toast = await this.toastController.create({
        message: 'Empresas Cargadas',
        duration: 2000
      });
      toast.present();
    }
    public DetalleEmpresa(item){
      const idEmpresa = item.id;
      this.router.navigate(['/detalle-empresa',{item:idEmpresa}]);
    }
  }