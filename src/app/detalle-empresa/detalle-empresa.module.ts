import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { DetalleEmpresaPageRoutingModule } from './detalle-empresa-routing.module';

import { DetalleEmpresaPage } from './detalle-empresa.page';
import { ComponentsModule } from '../components/components.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ComponentsModule,
    DetalleEmpresaPageRoutingModule
  ],
  declarations: [DetalleEmpresaPage]
})
export class DetalleEmpresaPageModule {}
