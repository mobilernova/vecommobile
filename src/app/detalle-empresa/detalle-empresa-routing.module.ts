import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DetalleEmpresaPage } from './detalle-empresa.page';

const routes: Routes = [
  {
    path: '',
    component: DetalleEmpresaPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class DetalleEmpresaPageRoutingModule {}
