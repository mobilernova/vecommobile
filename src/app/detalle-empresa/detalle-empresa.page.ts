import { Component, OnInit } from '@angular/core';
import { NavController, LoadingController, ToastController } from '@ionic/angular';
import { ActivatedRoute, Router } from '@angular/router';
import { EmpresasService } from '../servicios/empresas.service';
import { Empresa } from './../model/empresa';

@Component({
  selector: 'app-detalle-empresa',
  templateUrl: './detalle-empresa.page.html',
  styleUrls: ['./detalle-empresa.page.scss'],
})
export class DetalleEmpresaPage implements OnInit {

  // arrayPosts = [{nombre:"",foto:"",ciudad:"",direccion:"",descripcion:"",pagina_web:"",facebook:"",instagram:"",telefono:[],celular:[],watsapp:[],publicaciones:[]}];
  idEmpresa: any;
  arrayPosts = new Empresa();
  imgNotCar = 'assets/images/notfound.jpg'

  constructor(public navCtrl: NavController,
              public service: EmpresasService,
              public loadingController: LoadingController,
              public toastController: ToastController,
              public route: ActivatedRoute,
              public router:Router
    ) { 
      this.idEmpresa = this.route.snapshot.paramMap.get('item');

    }

  async ngOnInit() {
    const loading = await this.loadingController.create({
      message: 'Cargando...',
    });
    await loading.present().then(() => {
      this.service.detalleEmpresasVecom(this.idEmpresa).subscribe(
        async data => {
          this.arrayPosts = data.data;
          // this.categorias = data.data;
          console.log(this.arrayPosts);
          await loading.dismiss();
          this.msjToast();
        }, err => {
          console.log(err);
          loading.dismiss();
        });
    });
    }
    async msjToast() {
      const toast = await this.toastController.create({
        message: 'Datos mostrados.',
        duration: 2000
      });
      toast.present();
    }
    detallePublicacion(c){
      const idVehiculo = c.id;
      this.router.navigate(['detalle-auto', {detalleAutoVecom: idVehiculo}]);
    }
}
