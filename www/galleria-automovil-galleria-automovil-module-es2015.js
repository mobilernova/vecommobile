(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["galleria-automovil-galleria-automovil-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/galleria-automovil/galleria-automovil.page.html":
/*!*******************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/galleria-automovil/galleria-automovil.page.html ***!
  \*******************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-header>\n  <ion-toolbar class=\"head\">\n    <ion-buttons slot=\"start\" (click)=\"back()\">\n      <!-- <ion-back-button color=\"light\"></ion-back-button> -->\n      <ion-icon slot=\"start\" name=\"arrow-back\" color=\"light\"></ion-icon>\n    </ion-buttons>\n  <ion-title align=\"center\" color=\"light\">Galeria de Fotos</ion-title>\n  <ion-buttons slot=\"end\">\n    <ion-menu-button  style=\"color: #ffffff;\"></ion-menu-button>\n  </ion-buttons>\n</ion-toolbar>\n</ion-header>\n\n<ion-content>\n  <ion-grid>\n      <ion-row>\n        <ion-col size=\"6\" *ngFor=\"let item of imageAllProducto\" >\n          <ion-card >\n            <img [src]=\"item.foto\" />\n          </ion-card>\n          <div align=\"center\">\n              <ion-button icon-left color=\"danger\" (click)=\"deleteFotography(item.id)\">\n                  <ion-icon name=\"close\"></ion-icon> \n              </ion-button>\n          </div>\n        </ion-col>\n      </ion-row>\n    </ion-grid>\n\n\n    <ion-fab class=\"head\" horizontal=\"end\" vertical=\"bottom\" slot=\"fixed\">\n      <ion-fab-button  (click)=\"addImage()\">\n        <ion-icon name=\"add-circle-outline\"></ion-icon>\n    </ion-fab-button>\n    </ion-fab>\n\n    \n</ion-content>\n");

/***/ }),

/***/ "./src/app/galleria-automovil/galleria-automovil-routing.module.ts":
/*!*************************************************************************!*\
  !*** ./src/app/galleria-automovil/galleria-automovil-routing.module.ts ***!
  \*************************************************************************/
/*! exports provided: GalleriaAutomovilPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GalleriaAutomovilPageRoutingModule", function() { return GalleriaAutomovilPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _galleria_automovil_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./galleria-automovil.page */ "./src/app/galleria-automovil/galleria-automovil.page.ts");




const routes = [
    {
        path: '',
        component: _galleria_automovil_page__WEBPACK_IMPORTED_MODULE_3__["GalleriaAutomovilPage"]
    }
];
let GalleriaAutomovilPageRoutingModule = class GalleriaAutomovilPageRoutingModule {
};
GalleriaAutomovilPageRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], GalleriaAutomovilPageRoutingModule);



/***/ }),

/***/ "./src/app/galleria-automovil/galleria-automovil.module.ts":
/*!*****************************************************************!*\
  !*** ./src/app/galleria-automovil/galleria-automovil.module.ts ***!
  \*****************************************************************/
/*! exports provided: GalleriaAutomovilPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GalleriaAutomovilPageModule", function() { return GalleriaAutomovilPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _galleria_automovil_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./galleria-automovil-routing.module */ "./src/app/galleria-automovil/galleria-automovil-routing.module.ts");
/* harmony import */ var _galleria_automovil_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./galleria-automovil.page */ "./src/app/galleria-automovil/galleria-automovil.page.ts");







let GalleriaAutomovilPageModule = class GalleriaAutomovilPageModule {
};
GalleriaAutomovilPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _galleria_automovil_routing_module__WEBPACK_IMPORTED_MODULE_5__["GalleriaAutomovilPageRoutingModule"]
        ],
        declarations: [_galleria_automovil_page__WEBPACK_IMPORTED_MODULE_6__["GalleriaAutomovilPage"]]
    })
], GalleriaAutomovilPageModule);



/***/ }),

/***/ "./src/app/galleria-automovil/galleria-automovil.page.scss":
/*!*****************************************************************!*\
  !*** ./src/app/galleria-automovil/galleria-automovil.page.scss ***!
  \*****************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("ion-fab-button {\n  --background: #0ECFA8;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9pYmVyL0RvY3VtZW50cy9pYmVyL3Byb2plY3RzL3ZlY29tbW9iaWxlL3NyYy9hcHAvZ2FsbGVyaWEtYXV0b21vdmlsL2dhbGxlcmlhLWF1dG9tb3ZpbC5wYWdlLnNjc3MiLCJzcmMvYXBwL2dhbGxlcmlhLWF1dG9tb3ZpbC9nYWxsZXJpYS1hdXRvbW92aWwucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0kscUJBQUE7QUNDSiIsImZpbGUiOiJzcmMvYXBwL2dhbGxlcmlhLWF1dG9tb3ZpbC9nYWxsZXJpYS1hdXRvbW92aWwucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiaW9uLWZhYi1idXR0b257XG4gICAgLS1iYWNrZ3JvdW5kOiAjMEVDRkE4O1xufSIsImlvbi1mYWItYnV0dG9uIHtcbiAgLS1iYWNrZ3JvdW5kOiAjMEVDRkE4O1xufSJdfQ== */");

/***/ }),

/***/ "./src/app/galleria-automovil/galleria-automovil.page.ts":
/*!***************************************************************!*\
  !*** ./src/app/galleria-automovil/galleria-automovil.page.ts ***!
  \***************************************************************/
/*! exports provided: GalleriaAutomovilPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GalleriaAutomovilPage", function() { return GalleriaAutomovilPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _servicios_servicios_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../servicios/servicios.service */ "./src/app/servicios/servicios.service.ts");


// import { MainService } from './../services/main.service';



let GalleriaAutomovilPage = class GalleriaAutomovilPage {
    constructor(modalController, alertController, service, route, navCtrl, loadingCtrl, toastCtrl, platform) {
        this.modalController = modalController;
        this.alertController = alertController;
        this.service = service;
        this.route = route;
        this.navCtrl = navCtrl;
        this.loadingCtrl = loadingCtrl;
        this.toastCtrl = toastCtrl;
        this.platform = platform;
        this.platform.backButton.subscribe(() => {
            this.navCtrl.navigateRoot('/perfil-usuario');
            localStorage.removeItem('preventIdAutomovil');
        });
    }
    ionViewWillEnter() {
        this.galeria();
    }
    galeria() {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            this.idVehiculo = localStorage.getItem('preventIdAutomovil');
            const loading = yield this.loadingCtrl.create({
                message: 'Cargando...',
            });
            yield loading.present().then(() => {
                this.service.galleriaVehiculo(this.idVehiculo).subscribe((data) => tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
                    this.imageAllProducto = data.data;
                    console.log(this.imageAllProducto);
                    yield loading.dismiss();
                }), err => {
                    loading.dismiss();
                    this.message();
                });
            });
        });
    }
    deleteFotography(id) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            const alert = yield this.alertController.create({
                header: 'Eliminar',
                message: '<strong>Esta seguro de eliminar esta fotografia?</strong>!!!',
                buttons: [
                    {
                        text: 'Si',
                        // role: 'cancel',
                        // cssClass: 'secondary',
                        handler: (blah) => {
                            this.service.eliminarFotografiaVehiculo(id).subscribe((data) => tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
                                this.smsSuccessTrue = data.data;
                                console.log(this.smsSuccessTrue);
                                this.successRequest(this.smsSuccessTrue.data);
                                this.galeria();
                            }), err => {
                                this.message();
                            });
                        }
                    }, {
                        text: 'No',
                        role: 'cancel',
                        handler: () => {
                        }
                    }
                ]
            });
            yield alert.present();
        });
    }
    message() {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            const loading = yield this.loadingCtrl.create({
                message: 'Error de Conexion'
            });
            yield loading.present();
            const { role, data } = yield loading.onDidDismiss();
        });
    }
    successRequest(sms) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            const toast = yield this.toastCtrl.create({
                message: sms,
                duration: 1000,
                cssClass: "my-custom-class-success"
            });
            toast.present();
        });
    }
    // async addImage() {
    //   const modal = await this.modalController.create({
    //     component: AnhadirFotografiaComponent,
    //     componentProps:{
    //       idVehiculo:this.idVehiculo
    //     },
    //     keyboardClose: true,
    //     showBackdrop: true
    //   });
    //   return await modal.present();
    // }
    addImage() {
        // localStorage.setItem("preventIdAutomovil", item);
        this.route.navigate(['/agregar-fotografia']);
    }
    back() {
        this.navCtrl.navigateRoot('/perfil');
        localStorage.removeItem('preventIdAutomovil');
    }
};
GalleriaAutomovilPage.ctorParameters = () => [
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["ModalController"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["AlertController"] },
    { type: _servicios_servicios_service__WEBPACK_IMPORTED_MODULE_4__["ServiciosService"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["NavController"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["LoadingController"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["ToastController"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["Platform"] }
];
GalleriaAutomovilPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["Component"])({
        selector: 'app-galleria-automovil',
        template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./galleria-automovil.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/galleria-automovil/galleria-automovil.page.html")).default,
        styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./galleria-automovil.page.scss */ "./src/app/galleria-automovil/galleria-automovil.page.scss")).default]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_angular__WEBPACK_IMPORTED_MODULE_3__["ModalController"],
        _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["AlertController"],
        _servicios_servicios_service__WEBPACK_IMPORTED_MODULE_4__["ServiciosService"],
        _angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"],
        _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["NavController"],
        _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["LoadingController"],
        _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["ToastController"],
        _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["Platform"]])
], GalleriaAutomovilPage);



/***/ })

}]);
//# sourceMappingURL=galleria-automovil-galleria-automovil-module-es2015.js.map