function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["publicaciones-usuario-publicaciones-usuario-module"], {
  /***/
  "./node_modules/raw-loader/dist/cjs.js!./src/app/publicaciones-usuario/publicaciones-usuario.page.html":
  /*!*************************************************************************************************************!*\
    !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/publicaciones-usuario/publicaciones-usuario.page.html ***!
    \*************************************************************************************************************/

  /*! exports provided: default */

  /***/
  function node_modulesRawLoaderDistCjsJsSrcAppPublicacionesUsuarioPublicacionesUsuarioPageHtml(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "<app-header titulo=\"Publicaciones\"></app-header>\n\n<ion-content class=\"ion-padding\">\n  <ion-list #lista>\n    <ion-item-sliding *ngFor=\"let post of arrayPosts\">\n      <ion-item>\n        <ion-img\n          src=\"https://cdn.aarp.net/content/dam/aarp/auto/2017/07/1140-hyundai-ioniq-great-cars-road-trips-esp.imgcache.revfd63981c76a67e8a4ed28622bb76883e.web.900.513.jpg\"\n          (click)=\"DetalleAuto()\" style=\"width: 110px; height: 90px;\"></ion-img>\n        <ion-label class=\"datos\">\n          <h3>{{ post.address.city }}</h3>\n          <p>{{ post.username }}</p>\n          <ion-button class=\"precio\">{{ post.address.zipcode }} $</ion-button>\n         \n        </ion-label>\n      </ion-item>\n\n      <ion-item-options side=\"start\">\n        <ion-item-option (click)=\"favorite()\">\n          <ion-icon slot=\"icon-only\" name=\"heart\"></ion-icon>\n        </ion-item-option>\n        <ion-item-option color=\"secondary\" (click)=\"share()\">\n          <ion-icon slot=\"icon-only\" name=\"share\"></ion-icon>\n        </ion-item-option>\n\n      </ion-item-options>\n\n      <ion-item-options side=\"end\">\n\n        <ion-item-option color=\"danger\" (click)=\"borrar()\">\n          <ion-icon slot=\"icon-only\" name=\"trash\"></ion-icon>\n        </ion-item-option>\n      </ion-item-options>\n    </ion-item-sliding>\n  </ion-list>\n</ion-content>\n\n<app-footer></app-footer>";
    /***/
  },

  /***/
  "./src/app/publicaciones-usuario/publicaciones-usuario-routing.module.ts":
  /*!*******************************************************************************!*\
    !*** ./src/app/publicaciones-usuario/publicaciones-usuario-routing.module.ts ***!
    \*******************************************************************************/

  /*! exports provided: PublicacionesUsuarioPageRoutingModule */

  /***/
  function srcAppPublicacionesUsuarioPublicacionesUsuarioRoutingModuleTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "PublicacionesUsuarioPageRoutingModule", function () {
      return PublicacionesUsuarioPageRoutingModule;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/fesm2015/router.js");
    /* harmony import */


    var _publicaciones_usuario_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! ./publicaciones-usuario.page */
    "./src/app/publicaciones-usuario/publicaciones-usuario.page.ts");

    var routes = [{
      path: '',
      component: _publicaciones_usuario_page__WEBPACK_IMPORTED_MODULE_3__["PublicacionesUsuarioPage"]
    }];

    var PublicacionesUsuarioPageRoutingModule = function PublicacionesUsuarioPageRoutingModule() {
      _classCallCheck(this, PublicacionesUsuarioPageRoutingModule);
    };

    PublicacionesUsuarioPageRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
      imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
      exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
    })], PublicacionesUsuarioPageRoutingModule);
    /***/
  },

  /***/
  "./src/app/publicaciones-usuario/publicaciones-usuario.module.ts":
  /*!***********************************************************************!*\
    !*** ./src/app/publicaciones-usuario/publicaciones-usuario.module.ts ***!
    \***********************************************************************/

  /*! exports provided: PublicacionesUsuarioPageModule */

  /***/
  function srcAppPublicacionesUsuarioPublicacionesUsuarioModuleTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "PublicacionesUsuarioPageModule", function () {
      return PublicacionesUsuarioPageModule;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/common */
    "./node_modules/@angular/common/fesm2015/common.js");
    /* harmony import */


    var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! @angular/forms */
    "./node_modules/@angular/forms/fesm2015/forms.js");
    /* harmony import */


    var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! @ionic/angular */
    "./node_modules/@ionic/angular/dist/fesm5.js");
    /* harmony import */


    var _publicaciones_usuario_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! ./publicaciones-usuario-routing.module */
    "./src/app/publicaciones-usuario/publicaciones-usuario-routing.module.ts");
    /* harmony import */


    var _publicaciones_usuario_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! ./publicaciones-usuario.page */
    "./src/app/publicaciones-usuario/publicaciones-usuario.page.ts");
    /* harmony import */


    var _components_components_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
    /*! ../components/components.module */
    "./src/app/components/components.module.ts");

    var PublicacionesUsuarioPageModule = function PublicacionesUsuarioPageModule() {
      _classCallCheck(this, PublicacionesUsuarioPageModule);
    };

    PublicacionesUsuarioPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
      imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"], _components_components_module__WEBPACK_IMPORTED_MODULE_7__["ComponentsModule"], _publicaciones_usuario_routing_module__WEBPACK_IMPORTED_MODULE_5__["PublicacionesUsuarioPageRoutingModule"]],
      declarations: [_publicaciones_usuario_page__WEBPACK_IMPORTED_MODULE_6__["PublicacionesUsuarioPage"]]
    })], PublicacionesUsuarioPageModule);
    /***/
  },

  /***/
  "./src/app/publicaciones-usuario/publicaciones-usuario.page.scss":
  /*!***********************************************************************!*\
    !*** ./src/app/publicaciones-usuario/publicaciones-usuario.page.scss ***!
    \***********************************************************************/

  /*! exports provided: default */

  /***/
  function srcAppPublicacionesUsuarioPublicacionesUsuarioPageScss(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = ".datos {\n  margin-left: 15px;\n  margin-right: 15px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9pYmVyL0RvY3VtZW50cy9pYmVyL3Byb2plY3RzL3ZlY29tbW9iaWxlL3NyYy9hcHAvcHVibGljYWNpb25lcy11c3VhcmlvL3B1YmxpY2FjaW9uZXMtdXN1YXJpby5wYWdlLnNjc3MiLCJzcmMvYXBwL3B1YmxpY2FjaW9uZXMtdXN1YXJpby9wdWJsaWNhY2lvbmVzLXVzdWFyaW8ucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUNBO0VBQ0ksaUJBQUE7RUFDQSxrQkFBQTtBQ0FKIiwiZmlsZSI6InNyYy9hcHAvcHVibGljYWNpb25lcy11c3VhcmlvL3B1YmxpY2FjaW9uZXMtdXN1YXJpby5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyJcclxuLmRhdG9zeyAgICBcclxuICAgIG1hcmdpbi1sZWZ0OiAxNXB4OyBcclxuICAgIG1hcmdpbi1yaWdodDogMTVweDtcclxufVxyXG5cclxuIiwiLmRhdG9zIHtcbiAgbWFyZ2luLWxlZnQ6IDE1cHg7XG4gIG1hcmdpbi1yaWdodDogMTVweDtcbn0iXX0= */";
    /***/
  },

  /***/
  "./src/app/publicaciones-usuario/publicaciones-usuario.page.ts":
  /*!*********************************************************************!*\
    !*** ./src/app/publicaciones-usuario/publicaciones-usuario.page.ts ***!
    \*********************************************************************/

  /*! exports provided: PublicacionesUsuarioPage */

  /***/
  function srcAppPublicacionesUsuarioPublicacionesUsuarioPageTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "PublicacionesUsuarioPage", function () {
      return PublicacionesUsuarioPage;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @ionic/angular */
    "./node_modules/@ionic/angular/dist/fesm5.js");
    /* harmony import */


    var _servicios_servicios_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! ../servicios/servicios.service */
    "./src/app/servicios/servicios.service.ts");

    var PublicacionesUsuarioPage =
    /*#__PURE__*/
    function () {
      function PublicacionesUsuarioPage(navCtrl, service, loadingController, toastController) {
        _classCallCheck(this, PublicacionesUsuarioPage);

        this.navCtrl = navCtrl;
        this.service = service;
        this.loadingController = loadingController;
        this.toastController = toastController;
      }

      _createClass(PublicacionesUsuarioPage, [{
        key: "ngOnInit",
        value: function ngOnInit() {
          return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0,
          /*#__PURE__*/
          regeneratorRuntime.mark(function _callee2() {
            var _this = this;

            var loading;
            return regeneratorRuntime.wrap(function _callee2$(_context2) {
              while (1) {
                switch (_context2.prev = _context2.next) {
                  case 0:
                    _context2.next = 2;
                    return this.loadingController.create({
                      message: 'Cargando...'
                    });

                  case 2:
                    loading = _context2.sent;
                    _context2.next = 5;
                    return loading.present().then(function () {
                      _this.service.getDatos().subscribe(function (data) {
                        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](_this, void 0, void 0,
                        /*#__PURE__*/
                        regeneratorRuntime.mark(function _callee() {
                          return regeneratorRuntime.wrap(function _callee$(_context) {
                            while (1) {
                              switch (_context.prev = _context.next) {
                                case 0:
                                  this.arrayPosts = data; // this.categorias = data.data;

                                  console.log(data);
                                  _context.next = 4;
                                  return loading.dismiss();

                                case 4:
                                  this.msjToast();

                                case 5:
                                case "end":
                                  return _context.stop();
                              }
                            }
                          }, _callee, this);
                        }));
                      }, function (err) {
                        console.log(err);
                        loading.dismiss();
                      });
                    });

                  case 5:
                  case "end":
                    return _context2.stop();
                }
              }
            }, _callee2, this);
          }));
        }
      }, {
        key: "msjToast",
        value: function msjToast() {
          return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0,
          /*#__PURE__*/
          regeneratorRuntime.mark(function _callee3() {
            var toast;
            return regeneratorRuntime.wrap(function _callee3$(_context3) {
              while (1) {
                switch (_context3.prev = _context3.next) {
                  case 0:
                    _context3.next = 2;
                    return this.toastController.create({
                      message: 'Datos mostrados.',
                      duration: 2000
                    });

                  case 2:
                    toast = _context3.sent;
                    toast.present();

                  case 4:
                  case "end":
                    return _context3.stop();
                }
              }
            }, _callee3, this);
          }));
        }
      }, {
        key: "DetalleAuto",
        value: function DetalleAuto() {
          this.navCtrl.navigateRoot('/detalle-auto');
        }
      }, {
        key: "AgregarAuto",
        value: function AgregarAuto() {
          this.navCtrl.navigateRoot('/agregar-auto');
        }
      }, {
        key: "favorite",
        value: function favorite() {
          this.presentToast('Guardó en favoritos');
        }
      }, {
        key: "presentToast",
        value: function presentToast(message) {
          return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0,
          /*#__PURE__*/
          regeneratorRuntime.mark(function _callee4() {
            var toast;
            return regeneratorRuntime.wrap(function _callee4$(_context4) {
              while (1) {
                switch (_context4.prev = _context4.next) {
                  case 0:
                    _context4.next = 2;
                    return this.toastController.create({
                      message: message,
                      duration: 2000
                    });

                  case 2:
                    toast = _context4.sent;
                    toast.present();

                  case 4:
                  case "end":
                    return _context4.stop();
                }
              }
            }, _callee4, this);
          }));
        }
      }, {
        key: "share",
        value: function share() {
          this.presentToast('Compartido!');
        }
      }, {
        key: "borrar",
        value: function borrar() {
          this.presentToast('Borrado!');
        }
      }]);

      return PublicacionesUsuarioPage;
    }();

    PublicacionesUsuarioPage.ctorParameters = function () {
      return [{
        type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["NavController"]
      }, {
        type: _servicios_servicios_service__WEBPACK_IMPORTED_MODULE_3__["ServiciosService"]
      }, {
        type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["LoadingController"]
      }, {
        type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ToastController"]
      }];
    };

    PublicacionesUsuarioPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
      selector: 'app-publicaciones-usuario',
      template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! raw-loader!./publicaciones-usuario.page.html */
      "./node_modules/raw-loader/dist/cjs.js!./src/app/publicaciones-usuario/publicaciones-usuario.page.html")).default,
      styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! ./publicaciones-usuario.page.scss */
      "./src/app/publicaciones-usuario/publicaciones-usuario.page.scss")).default]
    }), tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_angular__WEBPACK_IMPORTED_MODULE_2__["NavController"], _servicios_servicios_service__WEBPACK_IMPORTED_MODULE_3__["ServiciosService"], _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["LoadingController"], _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ToastController"]])], PublicacionesUsuarioPage);
    /***/
  }
}]);
//# sourceMappingURL=publicaciones-usuario-publicaciones-usuario-module-es5.js.map