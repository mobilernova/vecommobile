(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["inicio-sesion-inicio-sesion-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/inicio-sesion/inicio-sesion.page.html":
/*!*********************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/inicio-sesion/inicio-sesion.page.html ***!
  \*********************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-content class=\"animated fadeIn login auth-page\"> \n  <div class=\"auth-content\">\n\n    \n    <div style=\"margin-top: 100px;\">\n      <ion-text style=\"margin-left: 15px; font-size: xx-large;\">\n          <strong>Login</strong>\n        </ion-text>      \n    </div>\n    <br>\n    <br>    \n    <ion-item class=\"box\">\n      <ion-label>Email: </ion-label>\n      <ion-input [(ngModel)]=\"email\" type=\"email\"></ion-input>\n    </ion-item>\n    <br>\n    <ion-item class=\"box\">\n      <ion-label>Constraseña: </ion-label>\n      <ion-input [(ngModel)]=\"password\" type=\"password\"></ion-input>\n    </ion-item>\n    <br>\n    \n    <div id=\"buttons-link\">\n      <ion-button class=\"btns\" expand=\"block\" (click)=\"ingresar()\" >\n        Ingresar\n      </ion-button>\n      \n      <ion-grid class=\"btn-group\">\n        <ion-row>\n          <ion-col size=\"6\">\n            <ion-button class=\"btnfacebook\" shape=\"round\" expand=\"full\">\n              <ion-icon slot=\"icon-only\" name=\"logo-facebook\"></ion-icon>\n            </ion-button>\n          </ion-col>\n            \n          <ion-col size=\"6\">\n            <ion-button color=\"danger\" class=\"btngoogle\" shape=\"round\" expand=\"full\">\n              <ion-icon slot=\"icon-only\" name=\"logo-googleplus\"></ion-icon>\n            </ion-button>\n          </ion-col>\n        </ion-row>\n      </ion-grid>\n    </div>  \n  </div>\n  <br>\n  <br>\n  <div class=\"ion-text-center\">\n  <ion-text (click)=\"registrarse()\">\n    ¿No tines cuenta? <strong>Registrarse</strong>\n  </ion-text> \n</div>\n</ion-content>\n\n\n");

/***/ }),

/***/ "./src/app/inicio-sesion/inicio-sesion-routing.module.ts":
/*!***************************************************************!*\
  !*** ./src/app/inicio-sesion/inicio-sesion-routing.module.ts ***!
  \***************************************************************/
/*! exports provided: InicioSesionPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "InicioSesionPageRoutingModule", function() { return InicioSesionPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _inicio_sesion_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./inicio-sesion.page */ "./src/app/inicio-sesion/inicio-sesion.page.ts");




const routes = [
    {
        path: '',
        component: _inicio_sesion_page__WEBPACK_IMPORTED_MODULE_3__["InicioSesionPage"]
    }
];
let InicioSesionPageRoutingModule = class InicioSesionPageRoutingModule {
};
InicioSesionPageRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], InicioSesionPageRoutingModule);



/***/ }),

/***/ "./src/app/inicio-sesion/inicio-sesion.module.ts":
/*!*******************************************************!*\
  !*** ./src/app/inicio-sesion/inicio-sesion.module.ts ***!
  \*******************************************************/
/*! exports provided: InicioSesionPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "InicioSesionPageModule", function() { return InicioSesionPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _inicio_sesion_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./inicio-sesion-routing.module */ "./src/app/inicio-sesion/inicio-sesion-routing.module.ts");
/* harmony import */ var _inicio_sesion_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./inicio-sesion.page */ "./src/app/inicio-sesion/inicio-sesion.page.ts");







let InicioSesionPageModule = class InicioSesionPageModule {
};
InicioSesionPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _inicio_sesion_routing_module__WEBPACK_IMPORTED_MODULE_5__["InicioSesionPageRoutingModule"]
        ],
        declarations: [_inicio_sesion_page__WEBPACK_IMPORTED_MODULE_6__["InicioSesionPage"]]
    })
], InicioSesionPageModule);



/***/ }),

/***/ "./src/app/inicio-sesion/inicio-sesion.page.scss":
/*!*******************************************************!*\
  !*** ./src/app/inicio-sesion/inicio-sesion.page.scss ***!
  \*******************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2luaWNpby1zZXNpb24vaW5pY2lvLXNlc2lvbi5wYWdlLnNjc3MifQ== */");

/***/ }),

/***/ "./src/app/inicio-sesion/inicio-sesion.page.ts":
/*!*****************************************************!*\
  !*** ./src/app/inicio-sesion/inicio-sesion.page.ts ***!
  \*****************************************************/
/*! exports provided: InicioSesionPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "InicioSesionPage", function() { return InicioSesionPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _servicios_servicios_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../servicios/servicios.service */ "./src/app/servicios/servicios.service.ts");




let InicioSesionPage = class InicioSesionPage {
    constructor(nav, service, loadingController, toastCtrl, menuCtrl) {
        this.nav = nav;
        this.service = service;
        this.loadingController = loadingController;
        this.toastCtrl = toastCtrl;
        this.menuCtrl = menuCtrl;
    }
    ionViewWillEnter() {
        this.menuCtrl.enable(false);
    }
    ngOnInit() {
    }
    ingresar() {
        this.tipo_login = '1';
        this.service.loginVecom(this.tipo_login, this.email, this.nombre, this.telefono, this.password).subscribe(data => {
            this.user = data.data;
            console.log(data);
            localStorage.setItem('access_token', this.user.access_token);
            localStorage.setItem('idUsuario', this.user.id);
            this.succesSms(this.user.nombre_completo);
            this.nav.navigateRoot('/home');
        }, err => {
            console.log(err);
            if (err.status == 401) {
                this.errorSms(err.error.data.message);
            }
            else {
                this.errorConexion();
            }
        });
    }
    registrarse() {
        this.nav.navigateBack('/registrarse');
    }
    succesSms(msm) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            const toast = yield this.toastCtrl.create({
                message: "Bienvenido" + ' ' + msm,
                duration: 4000,
                cssClass: 'my-custom-class-success',
            });
            toast.present();
        });
    }
    errorSms(msm) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            const toast = yield this.toastCtrl.create({
                message: msm,
                duration: 4000,
                cssClass: 'my-custom-class',
            });
            toast.present();
        });
    }
    errorConexion() {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            const toast = yield this.toastCtrl.create({
                message: "error de conexion",
                duration: 3000,
                cssClass: 'my-custom-class',
            });
            toast.present();
        });
    }
};
InicioSesionPage.ctorParameters = () => [
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["NavController"] },
    { type: _servicios_servicios_service__WEBPACK_IMPORTED_MODULE_3__["ServiciosService"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["LoadingController"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ToastController"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["MenuController"] }
];
InicioSesionPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-inicio-sesion',
        template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./inicio-sesion.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/inicio-sesion/inicio-sesion.page.html")).default,
        styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./inicio-sesion.page.scss */ "./src/app/inicio-sesion/inicio-sesion.page.scss")).default]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_angular__WEBPACK_IMPORTED_MODULE_2__["NavController"],
        _servicios_servicios_service__WEBPACK_IMPORTED_MODULE_3__["ServiciosService"],
        _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["LoadingController"],
        _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ToastController"],
        _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["MenuController"]])
], InicioSesionPage);



/***/ })

}]);
//# sourceMappingURL=inicio-sesion-inicio-sesion-module-es2015.js.map