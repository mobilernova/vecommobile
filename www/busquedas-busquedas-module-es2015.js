(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["busquedas-busquedas-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/busquedas/busquedas.page.html":
/*!*************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/busquedas/busquedas.page.html ***!
  \*************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<app-header titulo= \"Buscador\"></app-header>\n\n<ion-content>\n  <br>\n\t<ion-item class=\"box\">\n\t  <ion-label class=\"fw500\">Ciudad</ion-label>\n\t  <ion-select [(ngModel)]=\"ciudad\" multiple=\"false\">\n\t    <ion-select-option>La Paz</ion-select-option>\n\t    <ion-select-option>Santa Cruz</ion-select-option>\n\t    <ion-select-option>Cochabamba</ion-select-option>\n\t    <ion-select-option>Potosi</ion-select-option>\n\t    <ion-select-option>Oruro</ion-select-option>\n\t    <ion-select-option>Beni</ion-select-option>\n\t    <ion-select-option>Pando</ion-select-option>\n\t    <ion-select-option>Brazilian</ion-select-option>\n\t    <ion-select-option>Thai</ion-select-option>\n\t  </ion-select>\n\t</ion-item>\n<br>\n<ion-item class=\"box\">\n  <ion-label class=\"fw500\">Tipo de Vehiculo</ion-label>\n  <ion-select [(ngModel)]=\"tipo\" multiple=\"false\">\n    <ion-select-option>Motocicleta</ion-select-option>\n    <ion-select-option>Automovil</ion-select-option>\n  </ion-select>\n</ion-item>\n<br>\n<ion-item class=\"box\">\n  <ion-label class=\"fw500\">Marca</ion-label>\n  <ion-select [(ngModel)]=\"marca\" multiple=\"false\">\n    <ion-select-option></ion-select-option>\n    <ion-select-option></ion-select-option>\n    <ion-select-option></ion-select-option>\n    <ion-select-option></ion-select-option>\n    <ion-select-option></ion-select-option>\n    <ion-select-option></ion-select-option>\n  </ion-select>\n</ion-item>\n<br>\n<ion-item class=\"box\">\n  <ion-label class=\"fw500\">Modelo</ion-label>\n  <ion-select [(ngModel)]=\"modelo\" multiple=\"false\">\n    <ion-select-option></ion-select-option>\n    <ion-select-option></ion-select-option>\n    <ion-select-option></ion-select-option>\n    <ion-select-option></ion-select-option>\n    <ion-select-option></ion-select-option>\n    <ion-select-option></ion-select-option>\n  </ion-select>\n</ion-item>\n<br>\n\t<ion-item ion-margin-bottom class=\"box\">\n\t  <ion-label>\n      <ion-text>Precio</ion-text>\n    </ion-label>\n      <ion-range dualKnobs=\"true\" [(ngModel)]=\"minmaxprice\" min=\"5000\" max=\"100000\" step=\"500\">\n        <ion-label slot=\"start\"><ion-text>$ {{minmaxprice.lower}}</ion-text></ion-label>\n        <ion-label slot=\"end\"><ion-text>$ {{minmaxprice.upper}}</ion-text></ion-label>\n      </ion-range>\n  </ion-item>  \n        <ion-grid>\n          <ion-row >\n            <ion-col size=\"6\">\n              <ion-item class=\"divbox\">\n                <ion-label style=\"font-size: small;\">Año Desde</ion-label>\n                <ion-datetime placeholder=\"YYYY\"\n                              display-format=\"YYYY\"\n                              min=\"1980\"\n                              max=\"2025\"\n                              value=\"2021\">\n                </ion-datetime>\n              </ion-item>     \n            </ion-col>              \n            <ion-col size=\"6\">\n              <ion-item class=\"divbox\">\n                <ion-label style=\"font-size: small;\">Año Hasta</ion-label>\n                <ion-datetime placeholder=\"YYYY\"\n                              display-format=\"YYYY\"\n                              min=\"1980\"\n                              max=\"2025\"\n                              value=\"2021\">\n                </ion-datetime>\n              </ion-item>\n            </ion-col>\n          </ion-row>\n        </ion-grid>\n\t<ion-button class=\"btns\" expand=\"block\" (click)=\"buscar()\">Buscar</ion-button>\n</ion-content>\n\n<app-footer></app-footer>");

/***/ }),

/***/ "./src/app/busquedas/busquedas-routing.module.ts":
/*!*******************************************************!*\
  !*** ./src/app/busquedas/busquedas-routing.module.ts ***!
  \*******************************************************/
/*! exports provided: BusquedasPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "BusquedasPageRoutingModule", function() { return BusquedasPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _busquedas_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./busquedas.page */ "./src/app/busquedas/busquedas.page.ts");




const routes = [
    {
        path: '',
        component: _busquedas_page__WEBPACK_IMPORTED_MODULE_3__["BusquedasPage"]
    }
];
let BusquedasPageRoutingModule = class BusquedasPageRoutingModule {
};
BusquedasPageRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], BusquedasPageRoutingModule);



/***/ }),

/***/ "./src/app/busquedas/busquedas.module.ts":
/*!***********************************************!*\
  !*** ./src/app/busquedas/busquedas.module.ts ***!
  \***********************************************/
/*! exports provided: BusquedasPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "BusquedasPageModule", function() { return BusquedasPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _busquedas_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./busquedas-routing.module */ "./src/app/busquedas/busquedas-routing.module.ts");
/* harmony import */ var _busquedas_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./busquedas.page */ "./src/app/busquedas/busquedas.page.ts");
/* harmony import */ var _components_components_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../components/components.module */ "./src/app/components/components.module.ts");








let BusquedasPageModule = class BusquedasPageModule {
};
BusquedasPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _components_components_module__WEBPACK_IMPORTED_MODULE_7__["ComponentsModule"],
            _busquedas_routing_module__WEBPACK_IMPORTED_MODULE_5__["BusquedasPageRoutingModule"]
        ],
        declarations: [_busquedas_page__WEBPACK_IMPORTED_MODULE_6__["BusquedasPage"]]
    })
], BusquedasPageModule);



/***/ }),

/***/ "./src/app/busquedas/busquedas.page.scss":
/*!***********************************************!*\
  !*** ./src/app/busquedas/busquedas.page.scss ***!
  \***********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("app-footer {\n  height: 60px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9pYmVyL0RvY3VtZW50cy9pYmVyL3Byb2plY3RzL3ZlY29tbW9iaWxlL3NyYy9hcHAvYnVzcXVlZGFzL2J1c3F1ZWRhcy5wYWdlLnNjc3MiLCJzcmMvYXBwL2J1c3F1ZWRhcy9idXNxdWVkYXMucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0ksWUFBQTtBQ0NKIiwiZmlsZSI6InNyYy9hcHAvYnVzcXVlZGFzL2J1c3F1ZWRhcy5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyJhcHAtZm9vdGVye1xuICAgIGhlaWdodDogNjBweDtcbiAgfSIsImFwcC1mb290ZXIge1xuICBoZWlnaHQ6IDYwcHg7XG59Il19 */");

/***/ }),

/***/ "./src/app/busquedas/busquedas.page.ts":
/*!*********************************************!*\
  !*** ./src/app/busquedas/busquedas.page.ts ***!
  \*********************************************/
/*! exports provided: BusquedasPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "BusquedasPage", function() { return BusquedasPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");



let BusquedasPage = class BusquedasPage {
    constructor(modalCtrl) {
        this.modalCtrl = modalCtrl;
        this.minmaxprice = {
            upper: 100000,
            lower: 5000
        };
    }
    ngOnInit() {
    }
    buscar() {
        this.modalCtrl.dismiss();
    }
};
BusquedasPage.ctorParameters = () => [
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ModalController"] }
];
BusquedasPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-busquedas',
        template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./busquedas.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/busquedas/busquedas.page.html")).default,
        styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./busquedas.page.scss */ "./src/app/busquedas/busquedas.page.scss")).default]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ModalController"]])
], BusquedasPage);



/***/ })

}]);
//# sourceMappingURL=busquedas-busquedas-module-es2015.js.map