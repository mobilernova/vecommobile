function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["detalle-publicidad-detalle-publicidad-module"], {
  /***/
  "./node_modules/raw-loader/dist/cjs.js!./src/app/detalle-publicidad/detalle-publicidad.page.html":
  /*!*******************************************************************************************************!*\
    !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/detalle-publicidad/detalle-publicidad.page.html ***!
    \*******************************************************************************************************/

  /*! exports provided: default */

  /***/
  function node_modulesRawLoaderDistCjsJsSrcAppDetallePublicidadDetallePublicidadPageHtml(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "\n<ion-content>\n  <ion-toolbar>\n    <ion-buttons slot=\"start\">\n      <ion-back-button defaultHref=\"home\"></ion-back-button>\n    </ion-buttons>\n  </ion-toolbar>\n \n</ion-content>\n<ion-footer align=\"center\">\n  <ion-button class=\"btns\" expand=\"round\" (click)=\"presentActionSheet()\"><ion-icon name=\"contact\" class=\"icow\"></ion-icon></ion-button>\n</ion-footer>\n  \n\n";
    /***/
  },

  /***/
  "./src/app/detalle-publicidad/detalle-publicidad-routing.module.ts":
  /*!*************************************************************************!*\
    !*** ./src/app/detalle-publicidad/detalle-publicidad-routing.module.ts ***!
    \*************************************************************************/

  /*! exports provided: DetallePublicidadPageRoutingModule */

  /***/
  function srcAppDetallePublicidadDetallePublicidadRoutingModuleTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "DetallePublicidadPageRoutingModule", function () {
      return DetallePublicidadPageRoutingModule;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/fesm2015/router.js");
    /* harmony import */


    var _detalle_publicidad_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! ./detalle-publicidad.page */
    "./src/app/detalle-publicidad/detalle-publicidad.page.ts");

    var routes = [{
      path: '',
      component: _detalle_publicidad_page__WEBPACK_IMPORTED_MODULE_3__["DetallePublicidadPage"]
    }];

    var DetallePublicidadPageRoutingModule = function DetallePublicidadPageRoutingModule() {
      _classCallCheck(this, DetallePublicidadPageRoutingModule);
    };

    DetallePublicidadPageRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
      imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
      exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
    })], DetallePublicidadPageRoutingModule);
    /***/
  },

  /***/
  "./src/app/detalle-publicidad/detalle-publicidad.module.ts":
  /*!*****************************************************************!*\
    !*** ./src/app/detalle-publicidad/detalle-publicidad.module.ts ***!
    \*****************************************************************/

  /*! exports provided: DetallePublicidadPageModule */

  /***/
  function srcAppDetallePublicidadDetallePublicidadModuleTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "DetallePublicidadPageModule", function () {
      return DetallePublicidadPageModule;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/common */
    "./node_modules/@angular/common/fesm2015/common.js");
    /* harmony import */


    var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! @angular/forms */
    "./node_modules/@angular/forms/fesm2015/forms.js");
    /* harmony import */


    var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! @ionic/angular */
    "./node_modules/@ionic/angular/dist/fesm5.js");
    /* harmony import */


    var _detalle_publicidad_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! ./detalle-publicidad-routing.module */
    "./src/app/detalle-publicidad/detalle-publicidad-routing.module.ts");
    /* harmony import */


    var _detalle_publicidad_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! ./detalle-publicidad.page */
    "./src/app/detalle-publicidad/detalle-publicidad.page.ts");

    var DetallePublicidadPageModule = function DetallePublicidadPageModule() {
      _classCallCheck(this, DetallePublicidadPageModule);
    };

    DetallePublicidadPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
      imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"], _detalle_publicidad_routing_module__WEBPACK_IMPORTED_MODULE_5__["DetallePublicidadPageRoutingModule"]],
      declarations: [_detalle_publicidad_page__WEBPACK_IMPORTED_MODULE_6__["DetallePublicidadPage"]]
    })], DetallePublicidadPageModule);
    /***/
  },

  /***/
  "./src/app/detalle-publicidad/detalle-publicidad.page.scss":
  /*!*****************************************************************!*\
    !*** ./src/app/detalle-publicidad/detalle-publicidad.page.scss ***!
    \*****************************************************************/

  /*! exports provided: default */

  /***/
  function srcAppDetallePublicidadDetallePublicidadPageScss(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2RldGFsbGUtcHVibGljaWRhZC9kZXRhbGxlLXB1YmxpY2lkYWQucGFnZS5zY3NzIn0= */";
    /***/
  },

  /***/
  "./src/app/detalle-publicidad/detalle-publicidad.page.ts":
  /*!***************************************************************!*\
    !*** ./src/app/detalle-publicidad/detalle-publicidad.page.ts ***!
    \***************************************************************/

  /*! exports provided: DetallePublicidadPage */

  /***/
  function srcAppDetallePublicidadDetallePublicidadPageTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "DetallePublicidadPage", function () {
      return DetallePublicidadPage;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @ionic/angular */
    "./node_modules/@ionic/angular/dist/fesm5.js");

    var DetallePublicidadPage =
    /*#__PURE__*/
    function () {
      function DetallePublicidadPage(actionSheetCtrl) {
        _classCallCheck(this, DetallePublicidadPage);

        this.actionSheetCtrl = actionSheetCtrl;
      }

      _createClass(DetallePublicidadPage, [{
        key: "ngOnInit",
        value: function ngOnInit() {}
      }, {
        key: "presentActionSheet",
        value: function presentActionSheet() {
          return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0,
          /*#__PURE__*/
          regeneratorRuntime.mark(function _callee() {
            var actionSheet;
            return regeneratorRuntime.wrap(function _callee$(_context) {
              while (1) {
                switch (_context.prev = _context.next) {
                  case 0:
                    _context.next = 2;
                    return this.actionSheetCtrl.create({
                      backdropDismiss: false,
                      buttons: [{
                        text: 'Llamar',
                        icon: 'call',
                        handler: function handler() {
                          console.log('Call clicked');
                        }
                      }, {
                        text: 'Instagram',
                        icon: 'logo-instagram',
                        handler: function handler() {
                          console.log('Instagram clicked');
                        }
                      }, {
                        text: 'Facebook',
                        icon: 'logo-facebook',
                        handler: function handler() {
                          console.log('Facebook clicked');
                        }
                      }, {
                        text: 'Cancelar',
                        icon: 'close',
                        role: 'cancelar',
                        cssClass: 'secondary',
                        handler: function handler() {
                          console.log('Cancel clicked');
                        }
                      }]
                    });

                  case 2:
                    actionSheet = _context.sent;
                    _context.next = 5;
                    return actionSheet.present();

                  case 5:
                  case "end":
                    return _context.stop();
                }
              }
            }, _callee, this);
          }));
        }
      }]);

      return DetallePublicidadPage;
    }();

    DetallePublicidadPage.ctorParameters = function () {
      return [{
        type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ActionSheetController"]
      }];
    };

    DetallePublicidadPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
      selector: 'app-detalle-publicidad',
      template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! raw-loader!./detalle-publicidad.page.html */
      "./node_modules/raw-loader/dist/cjs.js!./src/app/detalle-publicidad/detalle-publicidad.page.html")).default,
      styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! ./detalle-publicidad.page.scss */
      "./src/app/detalle-publicidad/detalle-publicidad.page.scss")).default]
    }), tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ActionSheetController"]])], DetallePublicidadPage);
    /***/
  }
}]);
//# sourceMappingURL=detalle-publicidad-detalle-publicidad-module-es5.js.map