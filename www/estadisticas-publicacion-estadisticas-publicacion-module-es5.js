function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["estadisticas-publicacion-estadisticas-publicacion-module"], {
  /***/
  "./node_modules/raw-loader/dist/cjs.js!./src/app/estadisticas-publicacion/estadisticas-publicacion.page.html":
  /*!*******************************************************************************************************************!*\
    !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/estadisticas-publicacion/estadisticas-publicacion.page.html ***!
    \*******************************************************************************************************************/

  /*! exports provided: default */

  /***/
  function node_modulesRawLoaderDistCjsJsSrcAppEstadisticasPublicacionEstadisticasPublicacionPageHtml(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "<ion-header>\n  <ion-toolbar>\n    <ion-title>estadisticas-publicacion</ion-title>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content>\n\n</ion-content>\n";
    /***/
  },

  /***/
  "./src/app/estadisticas-publicacion/estadisticas-publicacion-routing.module.ts":
  /*!*************************************************************************************!*\
    !*** ./src/app/estadisticas-publicacion/estadisticas-publicacion-routing.module.ts ***!
    \*************************************************************************************/

  /*! exports provided: EstadisticasPublicacionPageRoutingModule */

  /***/
  function srcAppEstadisticasPublicacionEstadisticasPublicacionRoutingModuleTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "EstadisticasPublicacionPageRoutingModule", function () {
      return EstadisticasPublicacionPageRoutingModule;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/fesm2015/router.js");
    /* harmony import */


    var _estadisticas_publicacion_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! ./estadisticas-publicacion.page */
    "./src/app/estadisticas-publicacion/estadisticas-publicacion.page.ts");

    var routes = [{
      path: '',
      component: _estadisticas_publicacion_page__WEBPACK_IMPORTED_MODULE_3__["EstadisticasPublicacionPage"]
    }];

    var EstadisticasPublicacionPageRoutingModule = function EstadisticasPublicacionPageRoutingModule() {
      _classCallCheck(this, EstadisticasPublicacionPageRoutingModule);
    };

    EstadisticasPublicacionPageRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
      imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
      exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
    })], EstadisticasPublicacionPageRoutingModule);
    /***/
  },

  /***/
  "./src/app/estadisticas-publicacion/estadisticas-publicacion.module.ts":
  /*!*****************************************************************************!*\
    !*** ./src/app/estadisticas-publicacion/estadisticas-publicacion.module.ts ***!
    \*****************************************************************************/

  /*! exports provided: EstadisticasPublicacionPageModule */

  /***/
  function srcAppEstadisticasPublicacionEstadisticasPublicacionModuleTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "EstadisticasPublicacionPageModule", function () {
      return EstadisticasPublicacionPageModule;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/common */
    "./node_modules/@angular/common/fesm2015/common.js");
    /* harmony import */


    var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! @angular/forms */
    "./node_modules/@angular/forms/fesm2015/forms.js");
    /* harmony import */


    var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! @ionic/angular */
    "./node_modules/@ionic/angular/dist/fesm5.js");
    /* harmony import */


    var _estadisticas_publicacion_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! ./estadisticas-publicacion-routing.module */
    "./src/app/estadisticas-publicacion/estadisticas-publicacion-routing.module.ts");
    /* harmony import */


    var _estadisticas_publicacion_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! ./estadisticas-publicacion.page */
    "./src/app/estadisticas-publicacion/estadisticas-publicacion.page.ts");

    var EstadisticasPublicacionPageModule = function EstadisticasPublicacionPageModule() {
      _classCallCheck(this, EstadisticasPublicacionPageModule);
    };

    EstadisticasPublicacionPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
      imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"], _estadisticas_publicacion_routing_module__WEBPACK_IMPORTED_MODULE_5__["EstadisticasPublicacionPageRoutingModule"]],
      declarations: [_estadisticas_publicacion_page__WEBPACK_IMPORTED_MODULE_6__["EstadisticasPublicacionPage"]]
    })], EstadisticasPublicacionPageModule);
    /***/
  },

  /***/
  "./src/app/estadisticas-publicacion/estadisticas-publicacion.page.scss":
  /*!*****************************************************************************!*\
    !*** ./src/app/estadisticas-publicacion/estadisticas-publicacion.page.scss ***!
    \*****************************************************************************/

  /*! exports provided: default */

  /***/
  function srcAppEstadisticasPublicacionEstadisticasPublicacionPageScss(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2VzdGFkaXN0aWNhcy1wdWJsaWNhY2lvbi9lc3RhZGlzdGljYXMtcHVibGljYWNpb24ucGFnZS5zY3NzIn0= */";
    /***/
  },

  /***/
  "./src/app/estadisticas-publicacion/estadisticas-publicacion.page.ts":
  /*!***************************************************************************!*\
    !*** ./src/app/estadisticas-publicacion/estadisticas-publicacion.page.ts ***!
    \***************************************************************************/

  /*! exports provided: EstadisticasPublicacionPage */

  /***/
  function srcAppEstadisticasPublicacionEstadisticasPublicacionPageTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "EstadisticasPublicacionPage", function () {
      return EstadisticasPublicacionPage;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");

    var EstadisticasPublicacionPage =
    /*#__PURE__*/
    function () {
      function EstadisticasPublicacionPage() {
        _classCallCheck(this, EstadisticasPublicacionPage);
      }

      _createClass(EstadisticasPublicacionPage, [{
        key: "ngOnInit",
        value: function ngOnInit() {}
      }]);

      return EstadisticasPublicacionPage;
    }();

    EstadisticasPublicacionPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
      selector: 'app-estadisticas-publicacion',
      template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! raw-loader!./estadisticas-publicacion.page.html */
      "./node_modules/raw-loader/dist/cjs.js!./src/app/estadisticas-publicacion/estadisticas-publicacion.page.html")).default,
      styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! ./estadisticas-publicacion.page.scss */
      "./src/app/estadisticas-publicacion/estadisticas-publicacion.page.scss")).default]
    }), tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])], EstadisticasPublicacionPage);
    /***/
  }
}]);
//# sourceMappingURL=estadisticas-publicacion-estadisticas-publicacion-module-es5.js.map