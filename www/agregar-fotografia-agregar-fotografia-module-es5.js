function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["agregar-fotografia-agregar-fotografia-module"], {
  /***/
  "./node_modules/raw-loader/dist/cjs.js!./src/app/agregar-fotografia/agregar-fotografia.page.html":
  /*!*******************************************************************************************************!*\
    !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/agregar-fotografia/agregar-fotografia.page.html ***!
    \*******************************************************************************************************/

  /*! exports provided: default */

  /***/
  function node_modulesRawLoaderDistCjsJsSrcAppAgregarFotografiaAgregarFotografiaPageHtml(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "<ion-header>\n  <ion-toolbar class=\"head\">\n      <ion-buttons slot=\"start\">\n        <ion-back-button color=\"light\"></ion-back-button>\n      </ion-buttons>\n    <ion-title align=\"center\" color=\"light\">Agregar Imagen</ion-title>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content>\n  <div align=\"center\">\n      <img  [src]=\"imgOficial\">      \n  </div>\n    <div align=\"center\">\n            <ion-button  class=\"head\" (click)=\"takephoto()\" >\n                <ion-icon name=\"camera\"></ion-icon>\n                &nbsp;&nbsp;Camara\n            </ion-button>\n            <ion-button  class=\"head\" (click)=\"getimage()\" >\n                <ion-icon name=\"images\"></ion-icon>\n                &nbsp;&nbsp;Album\n            </ion-button>\n    </div>\n    <div align=\"center\">\n      <ion-button [disabled]=\"buttonOn\" class=\"head\" (click)=\"saveFotografia()\">Guardar</ion-button>\n    </div>\n\n</ion-content>\n";
    /***/
  },

  /***/
  "./src/app/agregar-fotografia/agregar-fotografia-routing.module.ts":
  /*!*************************************************************************!*\
    !*** ./src/app/agregar-fotografia/agregar-fotografia-routing.module.ts ***!
    \*************************************************************************/

  /*! exports provided: AgregarFotografiaPageRoutingModule */

  /***/
  function srcAppAgregarFotografiaAgregarFotografiaRoutingModuleTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "AgregarFotografiaPageRoutingModule", function () {
      return AgregarFotografiaPageRoutingModule;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/fesm2015/router.js");
    /* harmony import */


    var _agregar_fotografia_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! ./agregar-fotografia.page */
    "./src/app/agregar-fotografia/agregar-fotografia.page.ts");

    var routes = [{
      path: '',
      component: _agregar_fotografia_page__WEBPACK_IMPORTED_MODULE_3__["AgregarFotografiaPage"]
    }];

    var AgregarFotografiaPageRoutingModule = function AgregarFotografiaPageRoutingModule() {
      _classCallCheck(this, AgregarFotografiaPageRoutingModule);
    };

    AgregarFotografiaPageRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
      imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
      exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
    })], AgregarFotografiaPageRoutingModule);
    /***/
  },

  /***/
  "./src/app/agregar-fotografia/agregar-fotografia.module.ts":
  /*!*****************************************************************!*\
    !*** ./src/app/agregar-fotografia/agregar-fotografia.module.ts ***!
    \*****************************************************************/

  /*! exports provided: AgregarFotografiaPageModule */

  /***/
  function srcAppAgregarFotografiaAgregarFotografiaModuleTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "AgregarFotografiaPageModule", function () {
      return AgregarFotografiaPageModule;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/common */
    "./node_modules/@angular/common/fesm2015/common.js");
    /* harmony import */


    var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! @angular/forms */
    "./node_modules/@angular/forms/fesm2015/forms.js");
    /* harmony import */


    var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! @ionic/angular */
    "./node_modules/@ionic/angular/dist/fesm5.js");
    /* harmony import */


    var _agregar_fotografia_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! ./agregar-fotografia-routing.module */
    "./src/app/agregar-fotografia/agregar-fotografia-routing.module.ts");
    /* harmony import */


    var _agregar_fotografia_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! ./agregar-fotografia.page */
    "./src/app/agregar-fotografia/agregar-fotografia.page.ts");

    var AgregarFotografiaPageModule = function AgregarFotografiaPageModule() {
      _classCallCheck(this, AgregarFotografiaPageModule);
    };

    AgregarFotografiaPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
      imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"], _agregar_fotografia_routing_module__WEBPACK_IMPORTED_MODULE_5__["AgregarFotografiaPageRoutingModule"]],
      declarations: [_agregar_fotografia_page__WEBPACK_IMPORTED_MODULE_6__["AgregarFotografiaPage"]]
    })], AgregarFotografiaPageModule);
    /***/
  },

  /***/
  "./src/app/agregar-fotografia/agregar-fotografia.page.scss":
  /*!*****************************************************************!*\
    !*** ./src/app/agregar-fotografia/agregar-fotografia.page.scss ***!
    \*****************************************************************/

  /*! exports provided: default */

  /***/
  function srcAppAgregarFotografiaAgregarFotografiaPageScss(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "img {\n  width: 800px;\n  height: 300px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9pYmVyL0RvY3VtZW50cy9pYmVyL3Byb2plY3RzL3ZlY29tbW9iaWxlL3NyYy9hcHAvYWdyZWdhci1mb3RvZ3JhZmlhL2FncmVnYXItZm90b2dyYWZpYS5wYWdlLnNjc3MiLCJzcmMvYXBwL2FncmVnYXItZm90b2dyYWZpYS9hZ3JlZ2FyLWZvdG9ncmFmaWEucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0ksWUFBQTtFQUNBLGFBQUE7QUNDSiIsImZpbGUiOiJzcmMvYXBwL2FncmVnYXItZm90b2dyYWZpYS9hZ3JlZ2FyLWZvdG9ncmFmaWEucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiaW1ne1xuICAgIHdpZHRoOiA4MDBweDtcbiAgICBoZWlnaHQ6IDMwMHB4O1xufSIsImltZyB7XG4gIHdpZHRoOiA4MDBweDtcbiAgaGVpZ2h0OiAzMDBweDtcbn0iXX0= */";
    /***/
  },

  /***/
  "./src/app/agregar-fotografia/agregar-fotografia.page.ts":
  /*!***************************************************************!*\
    !*** ./src/app/agregar-fotografia/agregar-fotografia.page.ts ***!
    \***************************************************************/

  /*! exports provided: AgregarFotografiaPage */

  /***/
  function srcAppAgregarFotografiaAgregarFotografiaPageTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "AgregarFotografiaPage", function () {
      return AgregarFotografiaPage;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _ionic_native_camera_ngx__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @ionic-native/camera/ngx */
    "./node_modules/@ionic-native/camera/ngx/index.js");
    /* harmony import */


    var _ionic_angular__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! @ionic/angular */
    "./node_modules/@ionic/angular/dist/fesm5.js");
    /* harmony import */


    var src_app_servicios_servicios_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! src/app/servicios/servicios.service */
    "./src/app/servicios/servicios.service.ts");

    var AgregarFotografiaPage =
    /*#__PURE__*/
    function () {
      function AgregarFotografiaPage(modalController, navCtrl, camera, loadingCtrl, service) {
        _classCallCheck(this, AgregarFotografiaPage);

        this.modalController = modalController;
        this.navCtrl = navCtrl;
        this.camera = camera;
        this.loadingCtrl = loadingCtrl;
        this.service = service;
        this.imgOficial = 'assets/images/notfound.jpg';
        this.buttonOn = true;
      }

      _createClass(AgregarFotografiaPage, [{
        key: "ngOnInit",
        value: function ngOnInit() {
          this.idVehiculo = localStorage.getItem("preventIdAutomovil");
        }
      }, {
        key: "takephoto",
        value: function takephoto() {
          var _this = this;

          var options = {
            quality: 100,
            destinationType: this.camera.DestinationType.DATA_URL,
            encodingType: this.camera.EncodingType.JPEG,
            mediaType: this.camera.MediaType.PICTURE,
            correctOrientation: true,
            allowEdit: true
          };
          this.camera.getPicture(options).then(function (imageData) {
            _this.imgOficial = "data:image/jpeg;base64," + imageData;
            _this.buttonOn = false;
          }, function (err) {});
        }
      }, {
        key: "getimage",
        value: function getimage() {
          var _this2 = this;

          var options = {
            quality: 100,
            destinationType: this.camera.DestinationType.DATA_URL,
            sourceType: this.camera.PictureSourceType.PHOTOLIBRARY,
            saveToPhotoAlbum: false,
            correctOrientation: true,
            allowEdit: true
          };
          this.camera.getPicture(options).then(function (imageData) {
            _this2.imgOficial = "data:image/jpeg;base64," + imageData;
            _this2.buttonOn = false;
          }, function (err) {});
        }
      }, {
        key: "saveFotografia",
        value: function saveFotografia() {
          return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0,
          /*#__PURE__*/
          regeneratorRuntime.mark(function _callee2() {
            var _this3 = this;

            var loading;
            return regeneratorRuntime.wrap(function _callee2$(_context2) {
              while (1) {
                switch (_context2.prev = _context2.next) {
                  case 0:
                    _context2.next = 2;
                    return this.loadingCtrl.create({
                      message: 'Agregando...'
                    });

                  case 2:
                    loading = _context2.sent;
                    _context2.next = 5;
                    return loading.present().then(function () {
                      _this3.service.anhadirFotografiaVehiculo(_this3.idVehiculo, _this3.imgOficial).subscribe(function (data) {
                        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](_this3, void 0, void 0,
                        /*#__PURE__*/
                        regeneratorRuntime.mark(function _callee() {
                          return regeneratorRuntime.wrap(function _callee$(_context) {
                            while (1) {
                              switch (_context.prev = _context.next) {
                                case 0:
                                  _context.next = 2;
                                  return loading.dismiss();

                                case 2:
                                  this.modalController.dismiss();
                                  this.navCtrl.navigateRoot("/galleria-automovil");

                                case 4:
                                case "end":
                                  return _context.stop();
                              }
                            }
                          }, _callee, this);
                        }));
                      }, function (err) {
                        loading.dismiss();

                        _this3.message();
                      });
                    });

                  case 5:
                  case "end":
                    return _context2.stop();
                }
              }
            }, _callee2, this);
          }));
        }
      }, {
        key: "message",
        value: function message() {
          return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0,
          /*#__PURE__*/
          regeneratorRuntime.mark(function _callee3() {
            var loading, _ref, role, data;

            return regeneratorRuntime.wrap(function _callee3$(_context3) {
              while (1) {
                switch (_context3.prev = _context3.next) {
                  case 0:
                    _context3.next = 2;
                    return this.loadingCtrl.create({
                      message: 'Error de Conexion'
                    });

                  case 2:
                    loading = _context3.sent;
                    _context3.next = 5;
                    return loading.present();

                  case 5:
                    _context3.next = 7;
                    return loading.onDidDismiss();

                  case 7:
                    _ref = _context3.sent;
                    role = _ref.role;
                    data = _ref.data;

                  case 10:
                  case "end":
                    return _context3.stop();
                }
              }
            }, _callee3, this);
          }));
        }
      }, {
        key: "back",
        value: function back() {
          this.modalController.dismiss();
        }
      }]);

      return AgregarFotografiaPage;
    }();

    AgregarFotografiaPage.ctorParameters = function () {
      return [{
        type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["ModalController"]
      }, {
        type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["NavController"]
      }, {
        type: _ionic_native_camera_ngx__WEBPACK_IMPORTED_MODULE_2__["Camera"]
      }, {
        type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["LoadingController"]
      }, {
        type: src_app_servicios_servicios_service__WEBPACK_IMPORTED_MODULE_4__["ServiciosService"]
      }];
    };

    AgregarFotografiaPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
      selector: 'app-agregar-fotografia',
      template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! raw-loader!./agregar-fotografia.page.html */
      "./node_modules/raw-loader/dist/cjs.js!./src/app/agregar-fotografia/agregar-fotografia.page.html")).default,
      styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! ./agregar-fotografia.page.scss */
      "./src/app/agregar-fotografia/agregar-fotografia.page.scss")).default]
    }), tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_angular__WEBPACK_IMPORTED_MODULE_3__["ModalController"], _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["NavController"], _ionic_native_camera_ngx__WEBPACK_IMPORTED_MODULE_2__["Camera"], _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["LoadingController"], src_app_servicios_servicios_service__WEBPACK_IMPORTED_MODULE_4__["ServiciosService"]])], AgregarFotografiaPage);
    /***/
  }
}]);
//# sourceMappingURL=agregar-fotografia-agregar-fotografia-module-es5.js.map