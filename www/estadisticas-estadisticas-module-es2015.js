(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["estadisticas-estadisticas-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/estadisticas/estadisticas.page.html":
/*!*******************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/estadisticas/estadisticas.page.html ***!
  \*******************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-header>\n  <ion-toolbar class=\"head\">\n    <ion-buttons slot=\"start\">\n      <ion-back-button color=\"light\"></ion-back-button>\n    </ion-buttons>\n    <ion-title align=\"center\" color=\"light\">Estadisticas</ion-title>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content  ion-padding>  \n    <ion-grid>\n      <ion-row >\n        <ion-col size=\"6\" *ngFor=\"let item of loadEstadisticas\">\n        <ion-card class=\"box\"  >\n          <div align=\"center\">\n              <ion-icon  name=\"{{item.icon}}\"></ion-icon>\n              <p>{{item.cantidad}}</p>\n              <h2>{{item.titulo}}</h2>\n          </div>\n          <!-- <img [src]=\"post.foto\" alt=\"\"/>        -->\n        <!-- <label for=\"\" >{{item.titulo}}</label> -->\n     \n        </ion-card> \n      </ion-col>        \n      </ion-row>\n    </ion-grid>\n    <ion-card>\n      <ion-card-content>\n        <div align=\"center\">\n            <h2>Una vez que el vehiculo sea vendido no olvide cambiar el estado del motorizado</h2>\n            <hr>\n          <ion-label>{{estado}}\n          </ion-label>\n          <ion-toggle slot=\"start\" [(ngModel)]=\"estadoAuto\" (ionFocus)=checkState($event) >\n          </ion-toggle>\n        </div>\n        <hr>\n      </ion-card-content>\n    </ion-card>\n  </ion-content>\n\n");

/***/ }),

/***/ "./src/app/estadisticas/estadisticas-routing.module.ts":
/*!*************************************************************!*\
  !*** ./src/app/estadisticas/estadisticas-routing.module.ts ***!
  \*************************************************************/
/*! exports provided: EstadisticasPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EstadisticasPageRoutingModule", function() { return EstadisticasPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _estadisticas_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./estadisticas.page */ "./src/app/estadisticas/estadisticas.page.ts");




const routes = [
    {
        path: '',
        component: _estadisticas_page__WEBPACK_IMPORTED_MODULE_3__["EstadisticasPage"]
    }
];
let EstadisticasPageRoutingModule = class EstadisticasPageRoutingModule {
};
EstadisticasPageRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], EstadisticasPageRoutingModule);



/***/ }),

/***/ "./src/app/estadisticas/estadisticas.module.ts":
/*!*****************************************************!*\
  !*** ./src/app/estadisticas/estadisticas.module.ts ***!
  \*****************************************************/
/*! exports provided: EstadisticasPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EstadisticasPageModule", function() { return EstadisticasPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _estadisticas_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./estadisticas-routing.module */ "./src/app/estadisticas/estadisticas-routing.module.ts");
/* harmony import */ var _estadisticas_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./estadisticas.page */ "./src/app/estadisticas/estadisticas.page.ts");







let EstadisticasPageModule = class EstadisticasPageModule {
};
EstadisticasPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _estadisticas_routing_module__WEBPACK_IMPORTED_MODULE_5__["EstadisticasPageRoutingModule"]
        ],
        declarations: [_estadisticas_page__WEBPACK_IMPORTED_MODULE_6__["EstadisticasPage"]]
    })
], EstadisticasPageModule);



/***/ }),

/***/ "./src/app/estadisticas/estadisticas.page.scss":
/*!*****************************************************!*\
  !*** ./src/app/estadisticas/estadisticas.page.scss ***!
  \*****************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("ion-icon {\n  height: 100px;\n  width: 100px;\n}\n\n.box {\n  color: #f28f29;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9pYmVyL0RvY3VtZW50cy9pYmVyL3Byb2plY3RzL3ZlY29tbW9iaWxlL3NyYy9hcHAvZXN0YWRpc3RpY2FzL2VzdGFkaXN0aWNhcy5wYWdlLnNjc3MiLCJzcmMvYXBwL2VzdGFkaXN0aWNhcy9lc3RhZGlzdGljYXMucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0ksYUFBQTtFQUNBLFlBQUE7QUNDSjs7QURFQTtFQUNJLGNBQUE7QUNDSiIsImZpbGUiOiJzcmMvYXBwL2VzdGFkaXN0aWNhcy9lc3RhZGlzdGljYXMucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiaW9uLWljb257XG4gICAgaGVpZ2h0OiAxMDBweDtcbiAgICB3aWR0aDogMTAwcHg7XG59XG5cbi5ib3h7XG4gICAgY29sb3I6ICNmMjhmMjk7XG59IiwiaW9uLWljb24ge1xuICBoZWlnaHQ6IDEwMHB4O1xuICB3aWR0aDogMTAwcHg7XG59XG5cbi5ib3gge1xuICBjb2xvcjogI2YyOGYyOTtcbn0iXX0= */");

/***/ }),

/***/ "./src/app/estadisticas/estadisticas.page.ts":
/*!***************************************************!*\
  !*** ./src/app/estadisticas/estadisticas.page.ts ***!
  \***************************************************/
/*! exports provided: EstadisticasPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EstadisticasPage", function() { return EstadisticasPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _servicios_servicios_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../servicios/servicios.service */ "./src/app/servicios/servicios.service.ts");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");





let EstadisticasPage = class EstadisticasPage {
    constructor(service, loadingCtrl, route, alertController, toastCtrl, nav) {
        this.service = service;
        this.loadingCtrl = loadingCtrl;
        this.route = route;
        this.alertController = alertController;
        this.toastCtrl = toastCtrl;
        this.nav = nav;
        this.data = this.route.snapshot.paramMap.get('detalleAutoVecom');
        this.estado = this.route.snapshot.paramMap.get('estado');
    }
    ngOnInit() {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            if (this.estado == 'No vendido') {
                this.estadoAuto = false;
            }
            else {
                this.estadoAuto = true;
            }
            const loading = yield this.loadingCtrl.create({
                message: 'Cargando...',
            });
            yield loading.present().then(() => {
                this.service.estadisticasVehiculoVecom(this.data).subscribe((data) => tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
                    this.loadEstadisticas = data.data;
                    console.log(this.loadEstadisticas);
                    yield loading.dismiss();
                }), err => {
                    loading.dismiss();
                    // this.message();
                });
            });
        });
    }
    checkState(e) {
        console.log(e);
        if (e.target.checked) {
            this.estadoAuto = true;
            this.presentAlertConfirm();
        }
        else {
            this.estadoAuto = false;
            this.presentAlertConfirm();
        }
    }
    presentAlertConfirm() {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            const alert = yield this.alertController.create({
                header: 'Confirmar!',
                message: 'Desea cambiar el estado de venta del automovil?',
                buttons: [
                    {
                        text: 'Cancelar',
                        role: 'cancel',
                        cssClass: 'secondary',
                        handler: (blah) => {
                            console.log('Confirm Cancel: blah');
                        }
                    }, {
                        text: 'Si',
                        handler: () => {
                            this.service.estadoVehiculoVecom(this.data).subscribe((data) => tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
                                this.successEstado = data.data;
                                this.succesSms(this.successEstado);
                                this.nav.navigateRoot('/perfil-usuario');
                            }), err => {
                                console.log(err);
                                // this.message();
                            });
                        }
                    }
                ]
            });
            yield alert.present();
        });
    }
    succesSms(msm) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            const toast = yield this.toastCtrl.create({
                message: msm,
                duration: 4000,
                cssClass: 'my-custom-class-success',
            });
            toast.present();
        });
    }
    errorSms(msm) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            const toast = yield this.toastCtrl.create({
                message: msm,
                duration: 4000,
                cssClass: 'my-custom-class',
            });
            toast.present();
        });
    }
};
EstadisticasPage.ctorParameters = () => [
    { type: _servicios_servicios_service__WEBPACK_IMPORTED_MODULE_2__["ServiciosService"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["LoadingController"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_4__["ActivatedRoute"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["AlertController"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["ToastController"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["NavController"] }
];
EstadisticasPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-estadisticas',
        template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./estadisticas.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/estadisticas/estadisticas.page.html")).default,
        styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./estadisticas.page.scss */ "./src/app/estadisticas/estadisticas.page.scss")).default]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_servicios_servicios_service__WEBPACK_IMPORTED_MODULE_2__["ServiciosService"],
        _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["LoadingController"],
        _angular_router__WEBPACK_IMPORTED_MODULE_4__["ActivatedRoute"],
        _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["AlertController"],
        _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["ToastController"],
        _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["NavController"]])
], EstadisticasPage);



/***/ })

}]);
//# sourceMappingURL=estadisticas-estadisticas-module-es2015.js.map