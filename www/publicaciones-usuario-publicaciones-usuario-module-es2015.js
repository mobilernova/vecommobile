(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["publicaciones-usuario-publicaciones-usuario-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/publicaciones-usuario/publicaciones-usuario.page.html":
/*!*************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/publicaciones-usuario/publicaciones-usuario.page.html ***!
  \*************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<app-header titulo=\"Publicaciones\"></app-header>\n\n<ion-content class=\"ion-padding\">\n  <ion-list #lista>\n    <ion-item-sliding *ngFor=\"let post of arrayPosts\">\n      <ion-item>\n        <ion-img\n          src=\"https://cdn.aarp.net/content/dam/aarp/auto/2017/07/1140-hyundai-ioniq-great-cars-road-trips-esp.imgcache.revfd63981c76a67e8a4ed28622bb76883e.web.900.513.jpg\"\n          (click)=\"DetalleAuto()\" style=\"width: 110px; height: 90px;\"></ion-img>\n        <ion-label class=\"datos\">\n          <h3>{{ post.address.city }}</h3>\n          <p>{{ post.username }}</p>\n          <ion-button class=\"precio\">{{ post.address.zipcode }} $</ion-button>\n         \n        </ion-label>\n      </ion-item>\n\n      <ion-item-options side=\"start\">\n        <ion-item-option (click)=\"favorite()\">\n          <ion-icon slot=\"icon-only\" name=\"heart\"></ion-icon>\n        </ion-item-option>\n        <ion-item-option color=\"secondary\" (click)=\"share()\">\n          <ion-icon slot=\"icon-only\" name=\"share\"></ion-icon>\n        </ion-item-option>\n\n      </ion-item-options>\n\n      <ion-item-options side=\"end\">\n\n        <ion-item-option color=\"danger\" (click)=\"borrar()\">\n          <ion-icon slot=\"icon-only\" name=\"trash\"></ion-icon>\n        </ion-item-option>\n      </ion-item-options>\n    </ion-item-sliding>\n  </ion-list>\n</ion-content>\n\n<app-footer></app-footer>");

/***/ }),

/***/ "./src/app/publicaciones-usuario/publicaciones-usuario-routing.module.ts":
/*!*******************************************************************************!*\
  !*** ./src/app/publicaciones-usuario/publicaciones-usuario-routing.module.ts ***!
  \*******************************************************************************/
/*! exports provided: PublicacionesUsuarioPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PublicacionesUsuarioPageRoutingModule", function() { return PublicacionesUsuarioPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _publicaciones_usuario_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./publicaciones-usuario.page */ "./src/app/publicaciones-usuario/publicaciones-usuario.page.ts");




const routes = [
    {
        path: '',
        component: _publicaciones_usuario_page__WEBPACK_IMPORTED_MODULE_3__["PublicacionesUsuarioPage"]
    }
];
let PublicacionesUsuarioPageRoutingModule = class PublicacionesUsuarioPageRoutingModule {
};
PublicacionesUsuarioPageRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], PublicacionesUsuarioPageRoutingModule);



/***/ }),

/***/ "./src/app/publicaciones-usuario/publicaciones-usuario.module.ts":
/*!***********************************************************************!*\
  !*** ./src/app/publicaciones-usuario/publicaciones-usuario.module.ts ***!
  \***********************************************************************/
/*! exports provided: PublicacionesUsuarioPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PublicacionesUsuarioPageModule", function() { return PublicacionesUsuarioPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _publicaciones_usuario_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./publicaciones-usuario-routing.module */ "./src/app/publicaciones-usuario/publicaciones-usuario-routing.module.ts");
/* harmony import */ var _publicaciones_usuario_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./publicaciones-usuario.page */ "./src/app/publicaciones-usuario/publicaciones-usuario.page.ts");
/* harmony import */ var _components_components_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../components/components.module */ "./src/app/components/components.module.ts");








let PublicacionesUsuarioPageModule = class PublicacionesUsuarioPageModule {
};
PublicacionesUsuarioPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _components_components_module__WEBPACK_IMPORTED_MODULE_7__["ComponentsModule"],
            _publicaciones_usuario_routing_module__WEBPACK_IMPORTED_MODULE_5__["PublicacionesUsuarioPageRoutingModule"]
        ],
        declarations: [_publicaciones_usuario_page__WEBPACK_IMPORTED_MODULE_6__["PublicacionesUsuarioPage"]]
    })
], PublicacionesUsuarioPageModule);



/***/ }),

/***/ "./src/app/publicaciones-usuario/publicaciones-usuario.page.scss":
/*!***********************************************************************!*\
  !*** ./src/app/publicaciones-usuario/publicaciones-usuario.page.scss ***!
  \***********************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".datos {\n  margin-left: 15px;\n  margin-right: 15px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9pYmVyL0RvY3VtZW50cy9pYmVyL3Byb2plY3RzL3ZlY29tbW9iaWxlL3NyYy9hcHAvcHVibGljYWNpb25lcy11c3VhcmlvL3B1YmxpY2FjaW9uZXMtdXN1YXJpby5wYWdlLnNjc3MiLCJzcmMvYXBwL3B1YmxpY2FjaW9uZXMtdXN1YXJpby9wdWJsaWNhY2lvbmVzLXVzdWFyaW8ucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUNBO0VBQ0ksaUJBQUE7RUFDQSxrQkFBQTtBQ0FKIiwiZmlsZSI6InNyYy9hcHAvcHVibGljYWNpb25lcy11c3VhcmlvL3B1YmxpY2FjaW9uZXMtdXN1YXJpby5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyJcclxuLmRhdG9zeyAgICBcclxuICAgIG1hcmdpbi1sZWZ0OiAxNXB4OyBcclxuICAgIG1hcmdpbi1yaWdodDogMTVweDtcclxufVxyXG5cclxuIiwiLmRhdG9zIHtcbiAgbWFyZ2luLWxlZnQ6IDE1cHg7XG4gIG1hcmdpbi1yaWdodDogMTVweDtcbn0iXX0= */");

/***/ }),

/***/ "./src/app/publicaciones-usuario/publicaciones-usuario.page.ts":
/*!*********************************************************************!*\
  !*** ./src/app/publicaciones-usuario/publicaciones-usuario.page.ts ***!
  \*********************************************************************/
/*! exports provided: PublicacionesUsuarioPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PublicacionesUsuarioPage", function() { return PublicacionesUsuarioPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _servicios_servicios_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../servicios/servicios.service */ "./src/app/servicios/servicios.service.ts");




let PublicacionesUsuarioPage = class PublicacionesUsuarioPage {
    constructor(navCtrl, service, loadingController, toastController) {
        this.navCtrl = navCtrl;
        this.service = service;
        this.loadingController = loadingController;
        this.toastController = toastController;
    }
    ngOnInit() {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            const loading = yield this.loadingController.create({
                message: 'Cargando...',
            });
            yield loading.present().then(() => {
                this.service.getDatos().subscribe((data) => tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
                    this.arrayPosts = data;
                    // this.categorias = data.data;
                    console.log(data);
                    yield loading.dismiss();
                    this.msjToast();
                }), err => {
                    console.log(err);
                    loading.dismiss();
                });
            });
        });
    }
    msjToast() {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            const toast = yield this.toastController.create({
                message: 'Datos mostrados.',
                duration: 2000
            });
            toast.present();
        });
    }
    DetalleAuto() {
        this.navCtrl.navigateRoot('/detalle-auto');
    }
    AgregarAuto() {
        this.navCtrl.navigateRoot('/agregar-auto');
    }
    favorite() {
        this.presentToast('Guardó en favoritos');
    }
    presentToast(message) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            const toast = yield this.toastController.create({
                message,
                duration: 2000
            });
            toast.present();
        });
    }
    share() {
        this.presentToast('Compartido!');
    }
    borrar() {
        this.presentToast('Borrado!');
    }
};
PublicacionesUsuarioPage.ctorParameters = () => [
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["NavController"] },
    { type: _servicios_servicios_service__WEBPACK_IMPORTED_MODULE_3__["ServiciosService"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["LoadingController"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ToastController"] }
];
PublicacionesUsuarioPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-publicaciones-usuario',
        template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./publicaciones-usuario.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/publicaciones-usuario/publicaciones-usuario.page.html")).default,
        styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./publicaciones-usuario.page.scss */ "./src/app/publicaciones-usuario/publicaciones-usuario.page.scss")).default]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_angular__WEBPACK_IMPORTED_MODULE_2__["NavController"],
        _servicios_servicios_service__WEBPACK_IMPORTED_MODULE_3__["ServiciosService"],
        _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["LoadingController"],
        _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ToastController"]])
], PublicacionesUsuarioPage);



/***/ })

}]);
//# sourceMappingURL=publicaciones-usuario-publicaciones-usuario-module-es2015.js.map