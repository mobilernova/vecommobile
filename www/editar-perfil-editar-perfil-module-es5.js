function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["editar-perfil-editar-perfil-module"], {
  /***/
  "./node_modules/raw-loader/dist/cjs.js!./src/app/editar-perfil/editar-perfil.page.html":
  /*!*********************************************************************************************!*\
    !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/editar-perfil/editar-perfil.page.html ***!
    \*********************************************************************************************/

  /*! exports provided: default */

  /***/
  function node_modulesRawLoaderDistCjsJsSrcAppEditarPerfilEditarPerfilPageHtml(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "<ion-header >\n    <ion-toolbar class=\"head\">\n      <ion-buttons slot=\"start\">\n        <ion-back-button color=\"light\"></ion-back-button>\n      </ion-buttons>\n      <ion-title  align=\"center\" color=\"light\">Mi Perfil</ion-title>\n    </ion-toolbar>\n  </ion-header>\n  <br>\n<ion-content>\n<div align=\"center\">\n  <div>\n  <img [src]=\"data.foto ? data.foto : imageOficial\">\n</div>\n  <ion-icon (click)=\"changePhoto()\" name=\"camera\" class=\"icod\" ></ion-icon>\n\n</div>\n<div >\n<ion-item class=\"box\">\n  <ion-label >Nombre:</ion-label>\n  <ion-input [(ngModel)]=\"data.nombre_completo\"></ion-input>\n</ion-item>\n<br>\n\n<ion-item class=\"box\">\n  <ion-label>E-mail:</ion-label>\n  <ion-input [(ngModel)]=\"data.email\"></ion-input>\n</ion-item>\n<br>\n<ion-item class=\"box\">\n  <ion-label>Numero de Celular:</ion-label>\n  <ion-input [(ngModel)]=\"data.celular\"></ion-input>\n</ion-item>\n<br>\n<ion-item class=\"box\">\n    <ion-label>Cumpleaños:</ion-label>\n    <!-- <ion-datetime displayFormat=\"YYYY-MM-DD\" ></ion-datetime> -->\n    <input type=\"date\" [(ngModel)]=\"data.fecha_nacimiento\" placeholder=\"Cumpleaños\" >\n  </ion-item>\n  <br>\n</div>\n<div>\n  <ion-button expand=\"block\" style=\"--background: linear-gradient(135deg, var(--ion-color-theme), var(--ion-color-themelight)); margin-left: 15px; \n  margin-right: 15px;\" (click)=\"guardar()\">\n    Guardar Cambios\n  </ion-button>\n</div>\n</ion-content>\n";
    /***/
  },

  /***/
  "./src/app/editar-perfil/editar-perfil-routing.module.ts":
  /*!***************************************************************!*\
    !*** ./src/app/editar-perfil/editar-perfil-routing.module.ts ***!
    \***************************************************************/

  /*! exports provided: EditarPerfilPageRoutingModule */

  /***/
  function srcAppEditarPerfilEditarPerfilRoutingModuleTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "EditarPerfilPageRoutingModule", function () {
      return EditarPerfilPageRoutingModule;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/fesm2015/router.js");
    /* harmony import */


    var _editar_perfil_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! ./editar-perfil.page */
    "./src/app/editar-perfil/editar-perfil.page.ts");

    var routes = [{
      path: '',
      component: _editar_perfil_page__WEBPACK_IMPORTED_MODULE_3__["EditarPerfilPage"]
    }];

    var EditarPerfilPageRoutingModule = function EditarPerfilPageRoutingModule() {
      _classCallCheck(this, EditarPerfilPageRoutingModule);
    };

    EditarPerfilPageRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
      imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
      exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
    })], EditarPerfilPageRoutingModule);
    /***/
  },

  /***/
  "./src/app/editar-perfil/editar-perfil.module.ts":
  /*!*******************************************************!*\
    !*** ./src/app/editar-perfil/editar-perfil.module.ts ***!
    \*******************************************************/

  /*! exports provided: EditarPerfilPageModule */

  /***/
  function srcAppEditarPerfilEditarPerfilModuleTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "EditarPerfilPageModule", function () {
      return EditarPerfilPageModule;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/common */
    "./node_modules/@angular/common/fesm2015/common.js");
    /* harmony import */


    var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! @angular/forms */
    "./node_modules/@angular/forms/fesm2015/forms.js");
    /* harmony import */


    var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! @ionic/angular */
    "./node_modules/@ionic/angular/dist/fesm5.js");
    /* harmony import */


    var _editar_perfil_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! ./editar-perfil-routing.module */
    "./src/app/editar-perfil/editar-perfil-routing.module.ts");
    /* harmony import */


    var _editar_perfil_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! ./editar-perfil.page */
    "./src/app/editar-perfil/editar-perfil.page.ts");
    /* harmony import */


    var _components_components_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
    /*! ../components/components.module */
    "./src/app/components/components.module.ts");

    var EditarPerfilPageModule = function EditarPerfilPageModule() {
      _classCallCheck(this, EditarPerfilPageModule);
    };

    EditarPerfilPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
      imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"], _components_components_module__WEBPACK_IMPORTED_MODULE_7__["ComponentsModule"], _editar_perfil_routing_module__WEBPACK_IMPORTED_MODULE_5__["EditarPerfilPageRoutingModule"]],
      declarations: [_editar_perfil_page__WEBPACK_IMPORTED_MODULE_6__["EditarPerfilPage"]]
    })], EditarPerfilPageModule);
    /***/
  },

  /***/
  "./src/app/editar-perfil/editar-perfil.page.scss":
  /*!*******************************************************!*\
    !*** ./src/app/editar-perfil/editar-perfil.page.scss ***!
    \*******************************************************/

  /*! exports provided: default */

  /***/
  function srcAppEditarPerfilEditarPerfilPageScss(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "img {\n  height: 120px;\n  width: 120px;\n  border-radius: 50%;\n  border: solid 3px #505050;\n  display: inline;\n  box-shadow: 0 0 28px rgba(255, 255, 255, 0.65);\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9pYmVyL0RvY3VtZW50cy9pYmVyL3Byb2plY3RzL3ZlY29tbW9iaWxlL3NyYy9hcHAvZWRpdGFyLXBlcmZpbC9lZGl0YXItcGVyZmlsLnBhZ2Uuc2NzcyIsInNyYy9hcHAvZWRpdGFyLXBlcmZpbC9lZGl0YXItcGVyZmlsLnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUVJLGFBQUE7RUFDQSxZQUFBO0VBQ0Esa0JBQUE7RUFDQSx5QkFBQTtFQUNBLGVBQUE7RUFDQSw4Q0FBQTtBQ0FKIiwiZmlsZSI6InNyYy9hcHAvZWRpdGFyLXBlcmZpbC9lZGl0YXItcGVyZmlsLnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbImltZyB7XHJcbiAgICBcclxuICAgIGhlaWdodDogMTIwcHg7XHJcbiAgICB3aWR0aDogMTIwcHg7XHJcbiAgICBib3JkZXItcmFkaXVzOiA1MCU7XHJcbiAgICBib3JkZXI6IHNvbGlkIDNweCAjNTA1MDUwO1xyXG4gICAgZGlzcGxheTogaW5saW5lO1xyXG4gICAgYm94LXNoYWRvdzogMCAwIDI4cHggcmdiYSgyNTUsMjU1LDI1NSwgLjY1KTsgICAgXHJcbiAgICBcclxuICB9XHJcbiAgICAgICAgIFxyXG4iLCJpbWcge1xuICBoZWlnaHQ6IDEyMHB4O1xuICB3aWR0aDogMTIwcHg7XG4gIGJvcmRlci1yYWRpdXM6IDUwJTtcbiAgYm9yZGVyOiBzb2xpZCAzcHggIzUwNTA1MDtcbiAgZGlzcGxheTogaW5saW5lO1xuICBib3gtc2hhZG93OiAwIDAgMjhweCByZ2JhKDI1NSwgMjU1LCAyNTUsIDAuNjUpO1xufSJdfQ== */";
    /***/
  },

  /***/
  "./src/app/editar-perfil/editar-perfil.page.ts":
  /*!*****************************************************!*\
    !*** ./src/app/editar-perfil/editar-perfil.page.ts ***!
    \*****************************************************/

  /*! exports provided: EditarPerfilPage */

  /***/
  function srcAppEditarPerfilEditarPerfilPageTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "EditarPerfilPage", function () {
      return EditarPerfilPage;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @ionic/angular */
    "./node_modules/@ionic/angular/dist/fesm5.js");
    /* harmony import */


    var _ionic_native_camera_ngx__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! @ionic-native/camera/ngx */
    "./node_modules/@ionic-native/camera/ngx/index.js");
    /* harmony import */


    var _servicios_servicios_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! ../servicios/servicios.service */
    "./src/app/servicios/servicios.service.ts");

    var EditarPerfilPage =
    /*#__PURE__*/
    function () {
      function EditarPerfilPage(navCtrl, loadingController, toastCtrl, alertController, actionSheetController, camera, service) {
        _classCallCheck(this, EditarPerfilPage);

        this.navCtrl = navCtrl;
        this.loadingController = loadingController;
        this.toastCtrl = toastCtrl;
        this.alertController = alertController;
        this.actionSheetController = actionSheetController;
        this.camera = camera;
        this.service = service;
        this.imageOficial = "assets/images/perfil.jpg";
        this.data = {
          nombre_completo: "",
          fecha_nacimiento: "",
          email: '',
          celular: "",
          foto: ""
        };
      }

      _createClass(EditarPerfilPage, [{
        key: "ngOnInit",
        value: function ngOnInit() {
          var _this = this;

          this.service.perfilUsuario().subscribe(function (data) {
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](_this, void 0, void 0,
            /*#__PURE__*/
            regeneratorRuntime.mark(function _callee() {
              return regeneratorRuntime.wrap(function _callee$(_context) {
                while (1) {
                  switch (_context.prev = _context.next) {
                    case 0:
                      this.data = data.data;

                    case 1:
                    case "end":
                      return _context.stop();
                  }
                }
              }, _callee, this);
            }));
          }, function (err) {});
        }
      }, {
        key: "guardar",
        value: function guardar() {
          return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0,
          /*#__PURE__*/
          regeneratorRuntime.mark(function _callee3() {
            var _this2 = this;

            var loading;
            return regeneratorRuntime.wrap(function _callee3$(_context3) {
              while (1) {
                switch (_context3.prev = _context3.next) {
                  case 0:
                    _context3.next = 2;
                    return this.loadingController.create({
                      message: 'Editando información....'
                    });

                  case 2:
                    loading = _context3.sent;
                    _context3.next = 5;
                    return loading.present().then(function () {
                      _this2.service.editarUsuarioVecom(_this2.data.nombre_completo, _this2.data.email, _this2.data.celular, _this2.data.fecha_nacimiento, _this2.data.foto).subscribe(function (data) {
                        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](_this2, void 0, void 0,
                        /*#__PURE__*/
                        regeneratorRuntime.mark(function _callee2() {
                          return regeneratorRuntime.wrap(function _callee2$(_context2) {
                            while (1) {
                              switch (_context2.prev = _context2.next) {
                                case 0:
                                  this.succesSms(data.data.data);
                                  this.navCtrl.navigateRoot('/perfil-usuario');
                                  _context2.next = 4;
                                  return loading.dismiss();

                                case 4:
                                case "end":
                                  return _context2.stop();
                              }
                            }
                          }, _callee2, this);
                        }));
                      }, function (err) {
                        _this2.errorRequest(err.error.data.message);

                        loading.dismiss();
                      });
                    });

                  case 5:
                  case "end":
                    return _context3.stop();
                }
              }
            }, _callee3, this);
          }));
        }
      }, {
        key: "succesSms",
        value: function succesSms(msm) {
          return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0,
          /*#__PURE__*/
          regeneratorRuntime.mark(function _callee4() {
            var toast;
            return regeneratorRuntime.wrap(function _callee4$(_context4) {
              while (1) {
                switch (_context4.prev = _context4.next) {
                  case 0:
                    _context4.next = 2;
                    return this.toastCtrl.create({
                      message: msm,
                      duration: 4000,
                      cssClass: 'my-custom-class-success'
                    });

                  case 2:
                    toast = _context4.sent;
                    toast.present();

                  case 4:
                  case "end":
                    return _context4.stop();
                }
              }
            }, _callee4, this);
          }));
        }
      }, {
        key: "errorRequest",
        value: function errorRequest(sms) {
          return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0,
          /*#__PURE__*/
          regeneratorRuntime.mark(function _callee5() {
            var toast;
            return regeneratorRuntime.wrap(function _callee5$(_context5) {
              while (1) {
                switch (_context5.prev = _context5.next) {
                  case 0:
                    _context5.next = 2;
                    return this.toastCtrl.create({
                      message: sms,
                      duration: 3000,
                      cssClass: "my-custom-class-alert"
                    });

                  case 2:
                    toast = _context5.sent;
                    toast.present();

                  case 4:
                  case "end":
                    return _context5.stop();
                }
              }
            }, _callee5, this);
          }));
        }
      }, {
        key: "changePhoto",
        value: function changePhoto() {
          return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0,
          /*#__PURE__*/
          regeneratorRuntime.mark(function _callee6() {
            var _this3 = this;

            var actionSheet;
            return regeneratorRuntime.wrap(function _callee6$(_context6) {
              while (1) {
                switch (_context6.prev = _context6.next) {
                  case 0:
                    _context6.next = 2;
                    return this.actionSheetController.create({
                      header: 'Fotografias',
                      buttons: [{
                        text: 'Tomar fotografia',
                        icon: 'camera',
                        handler: function handler() {
                          var options = {
                            quality: 100,
                            destinationType: _this3.camera.DestinationType.DATA_URL,
                            encodingType: _this3.camera.EncodingType.JPEG,
                            mediaType: _this3.camera.MediaType.PICTURE,
                            correctOrientation: true,
                            allowEdit: false,
                            targetHeight: 300,
                            targetWidth: 300
                          };

                          _this3.camera.getPicture(options).then(function (imageData) {
                            _this3.data.foto = "data:image/jpeg;base64," + imageData;
                          }, function (err) {});
                        }
                      }, {
                        text: 'Galleria',
                        icon: 'images',
                        handler: function handler() {
                          var options = {
                            quality: 100,
                            destinationType: _this3.camera.DestinationType.DATA_URL,
                            sourceType: _this3.camera.PictureSourceType.PHOTOLIBRARY,
                            saveToPhotoAlbum: false,
                            correctOrientation: true,
                            allowEdit: false,
                            targetHeight: 300,
                            targetWidth: 300
                          };

                          _this3.camera.getPicture(options).then(function (imageData) {
                            _this3.data.foto = "data:image/jpeg;base64," + imageData;
                          }, function (err) {});
                        }
                      }, {
                        text: 'Cancel',
                        icon: 'close',
                        role: 'cancel',
                        handler: function handler() {}
                      }]
                    });

                  case 2:
                    actionSheet = _context6.sent;
                    _context6.next = 5;
                    return actionSheet.present();

                  case 5:
                  case "end":
                    return _context6.stop();
                }
              }
            }, _callee6, this);
          }));
        }
      }]);

      return EditarPerfilPage;
    }();

    EditarPerfilPage.ctorParameters = function () {
      return [{
        type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["NavController"]
      }, {
        type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["LoadingController"]
      }, {
        type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ToastController"]
      }, {
        type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["AlertController"]
      }, {
        type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ActionSheetController"]
      }, {
        type: _ionic_native_camera_ngx__WEBPACK_IMPORTED_MODULE_3__["Camera"]
      }, {
        type: _servicios_servicios_service__WEBPACK_IMPORTED_MODULE_4__["ServiciosService"]
      }];
    };

    EditarPerfilPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
      selector: 'app-editar-perfil',
      template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! raw-loader!./editar-perfil.page.html */
      "./node_modules/raw-loader/dist/cjs.js!./src/app/editar-perfil/editar-perfil.page.html")).default,
      styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! ./editar-perfil.page.scss */
      "./src/app/editar-perfil/editar-perfil.page.scss")).default]
    }), tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_angular__WEBPACK_IMPORTED_MODULE_2__["NavController"], _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["LoadingController"], _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ToastController"], _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["AlertController"], _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ActionSheetController"], _ionic_native_camera_ngx__WEBPACK_IMPORTED_MODULE_3__["Camera"], _servicios_servicios_service__WEBPACK_IMPORTED_MODULE_4__["ServiciosService"]])], EditarPerfilPage);
    /***/
  }
}]);
//# sourceMappingURL=editar-perfil-editar-perfil-module-es5.js.map