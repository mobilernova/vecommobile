(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["detalle-empresa-detalle-empresa-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/detalle-empresa/detalle-empresa.page.html":
/*!*************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/detalle-empresa/detalle-empresa.page.html ***!
  \*************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-header align=\"center\">\n  <ion-toolbar class=\"head\">\n      <ion-buttons slot=\"start\">\n        <ion-back-button style=\"color: #ffffff;\"></ion-back-button>\n      </ion-buttons>    \n    <ion-title class=\"title\" style=\"margin-top: 5px;\">{{arrayPosts.nombre}}</ion-title>\n    <ion-icon name=\"contact\" class=\"ion-float-end\" style=\"margin-top: -15px; margin-right: 5px; color: #ffffff; zoom: 2;\" (click)=\"presentActionSheet()\"></ion-icon>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content>\n    <img [src]=\"arrayPosts.foto?arrayPosts.foto:imgNotCar\" height=\"250px\">\n  <div>  \n    <ion-item>    \n        <p align=\"justify\">\n          Lorem ipsum dolor sit amet consectetur adipisicing elit. Quia illum quisquam dolorem beatae! Enim saepe quia aspernatur distinctio officia neque, ea harum asperiores voluptas facilis repudiandae soluta hic maxime cumque.\n        </p>\n    </ion-item>     \n    <ion-item>\n      <ion-icon name=\"pin\" style=\"zoom: 2;\"></ion-icon>\n      <ion-label>\n        <h3>Cuidad:</h3>        \n        <h3>Dirección</h3> \n         \n      </ion-label>\n    </ion-item>\n    <ion-item>\n      <ion-icon name=\"call\" style=\"zoom: 2;\"></ion-icon>      \n      <ion-label>\n        <h3>Contacto</h3>        \n      <p>\n        Numero de Telefono\n      </p>\n      <p>\n        Numero de Celular\n      </p>\n      </ion-label>\n    </ion-item>    \n  </div>\n  <div>\n    <ion-grid>\n      <ion-row >\n        <ion-col size=\"6\" *ngFor=\"let item of arrayPosts.publicaciones\">\n        <ion-card align=\"center\" (click)=\"detallePublicacion(item)\">\n          <ion-card-title>{{item.marca}}</ion-card-title>\n          <img [src]=\"item.foto?item.foto:imgNotCar\" alt=\"\" height=\"150px\"/>       \n          <ion-card-subtitle>Precio {{item.precio}} $</ion-card-subtitle>\n        </ion-card>        \n      </ion-col>        \n      </ion-row>\n    </ion-grid>\n  </div>\n\n  \n</ion-content>\n\n\n");

/***/ }),

/***/ "./src/app/detalle-empresa/detalle-empresa-routing.module.ts":
/*!*******************************************************************!*\
  !*** ./src/app/detalle-empresa/detalle-empresa-routing.module.ts ***!
  \*******************************************************************/
/*! exports provided: DetalleEmpresaPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DetalleEmpresaPageRoutingModule", function() { return DetalleEmpresaPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _detalle_empresa_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./detalle-empresa.page */ "./src/app/detalle-empresa/detalle-empresa.page.ts");




const routes = [
    {
        path: '',
        component: _detalle_empresa_page__WEBPACK_IMPORTED_MODULE_3__["DetalleEmpresaPage"]
    }
];
let DetalleEmpresaPageRoutingModule = class DetalleEmpresaPageRoutingModule {
};
DetalleEmpresaPageRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], DetalleEmpresaPageRoutingModule);



/***/ }),

/***/ "./src/app/detalle-empresa/detalle-empresa.module.ts":
/*!***********************************************************!*\
  !*** ./src/app/detalle-empresa/detalle-empresa.module.ts ***!
  \***********************************************************/
/*! exports provided: DetalleEmpresaPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DetalleEmpresaPageModule", function() { return DetalleEmpresaPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _detalle_empresa_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./detalle-empresa-routing.module */ "./src/app/detalle-empresa/detalle-empresa-routing.module.ts");
/* harmony import */ var _detalle_empresa_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./detalle-empresa.page */ "./src/app/detalle-empresa/detalle-empresa.page.ts");
/* harmony import */ var _components_components_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../components/components.module */ "./src/app/components/components.module.ts");








let DetalleEmpresaPageModule = class DetalleEmpresaPageModule {
};
DetalleEmpresaPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _components_components_module__WEBPACK_IMPORTED_MODULE_7__["ComponentsModule"],
            _detalle_empresa_routing_module__WEBPACK_IMPORTED_MODULE_5__["DetalleEmpresaPageRoutingModule"]
        ],
        declarations: [_detalle_empresa_page__WEBPACK_IMPORTED_MODULE_6__["DetalleEmpresaPage"]]
    })
], DetalleEmpresaPageModule);



/***/ }),

/***/ "./src/app/detalle-empresa/detalle-empresa.page.scss":
/*!***********************************************************!*\
  !*** ./src/app/detalle-empresa/detalle-empresa.page.scss ***!
  \***********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".datos {\n  margin-left: 15px;\n  margin-right: 15px;\n}\n\nimg {\n  width: 100%;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9pYmVyL0RvY3VtZW50cy9pYmVyL3Byb2plY3RzL3ZlY29tbW9iaWxlL3NyYy9hcHAvZGV0YWxsZS1lbXByZXNhL2RldGFsbGUtZW1wcmVzYS5wYWdlLnNjc3MiLCJzcmMvYXBwL2RldGFsbGUtZW1wcmVzYS9kZXRhbGxlLWVtcHJlc2EucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0ksaUJBQUE7RUFDQSxrQkFBQTtBQ0NKOztBRENBO0VBQ0UsV0FBQTtBQ0VGIiwiZmlsZSI6InNyYy9hcHAvZGV0YWxsZS1lbXByZXNhL2RldGFsbGUtZW1wcmVzYS5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIuZGF0b3N7ICAgIFxyXG4gICAgbWFyZ2luLWxlZnQ6IDE1cHg7IFxyXG4gICAgbWFyZ2luLXJpZ2h0OiAxNXB4O1xyXG59XHJcbmltZ3tcclxuICB3aWR0aDogMTAwJTtcclxufVxyXG5cclxuXHJcbiAgICAgICAgIFxyXG4iLCIuZGF0b3Mge1xuICBtYXJnaW4tbGVmdDogMTVweDtcbiAgbWFyZ2luLXJpZ2h0OiAxNXB4O1xufVxuXG5pbWcge1xuICB3aWR0aDogMTAwJTtcbn0iXX0= */");

/***/ }),

/***/ "./src/app/detalle-empresa/detalle-empresa.page.ts":
/*!*********************************************************!*\
  !*** ./src/app/detalle-empresa/detalle-empresa.page.ts ***!
  \*********************************************************/
/*! exports provided: DetalleEmpresaPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DetalleEmpresaPage", function() { return DetalleEmpresaPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _servicios_empresas_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../servicios/empresas.service */ "./src/app/servicios/empresas.service.ts");
/* harmony import */ var _model_empresa__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./../model/empresa */ "./src/app/model/empresa.ts");






let DetalleEmpresaPage = class DetalleEmpresaPage {
    constructor(navCtrl, service, loadingController, toastController, route, router) {
        this.navCtrl = navCtrl;
        this.service = service;
        this.loadingController = loadingController;
        this.toastController = toastController;
        this.route = route;
        this.router = router;
        this.arrayPosts = new _model_empresa__WEBPACK_IMPORTED_MODULE_5__["Empresa"]();
        this.imgNotCar = 'assets/images/notfound.jpg';
        this.idEmpresa = this.route.snapshot.paramMap.get('item');
    }
    ngOnInit() {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            const loading = yield this.loadingController.create({
                message: 'Cargando...',
            });
            yield loading.present().then(() => {
                this.service.detalleEmpresasVecom(this.idEmpresa).subscribe((data) => tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
                    this.arrayPosts = data.data;
                    // this.categorias = data.data;
                    console.log(this.arrayPosts);
                    yield loading.dismiss();
                    this.msjToast();
                }), err => {
                    console.log(err);
                    loading.dismiss();
                });
            });
        });
    }
    msjToast() {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            const toast = yield this.toastController.create({
                message: 'Datos mostrados.',
                duration: 2000
            });
            toast.present();
        });
    }
    detallePublicacion(c) {
        const idVehiculo = c.id;
        this.router.navigate(['detalle-auto', { detalleAutoVecom: idVehiculo }]);
    }
};
DetalleEmpresaPage.ctorParameters = () => [
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["NavController"] },
    { type: _servicios_empresas_service__WEBPACK_IMPORTED_MODULE_4__["EmpresasService"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["LoadingController"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ToastController"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_3__["ActivatedRoute"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"] }
];
DetalleEmpresaPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-detalle-empresa',
        template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./detalle-empresa.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/detalle-empresa/detalle-empresa.page.html")).default,
        styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./detalle-empresa.page.scss */ "./src/app/detalle-empresa/detalle-empresa.page.scss")).default]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_angular__WEBPACK_IMPORTED_MODULE_2__["NavController"],
        _servicios_empresas_service__WEBPACK_IMPORTED_MODULE_4__["EmpresasService"],
        _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["LoadingController"],
        _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ToastController"],
        _angular_router__WEBPACK_IMPORTED_MODULE_3__["ActivatedRoute"],
        _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"]])
], DetalleEmpresaPage);



/***/ }),

/***/ "./src/app/model/empresa.ts":
/*!**********************************!*\
  !*** ./src/app/model/empresa.ts ***!
  \**********************************/
/*! exports provided: Empresa */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Empresa", function() { return Empresa; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");

//   arrayPosts = [{nombre:"",foto:"",ciudad:"",direccion:"",descripcion:"",pagina_web:"",facebook:"",instagram:"",telefono:[],celular:[],watsapp:[],publicaciones:[]}];
class Empresa {
}


/***/ })

}]);
//# sourceMappingURL=detalle-empresa-detalle-empresa-module-es2015.js.map