function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["autoventa-autoventa-module"], {
  /***/
  "./node_modules/raw-loader/dist/cjs.js!./src/app/autoventa/autoventa.page.html":
  /*!*************************************************************************************!*\
    !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/autoventa/autoventa.page.html ***!
    \*************************************************************************************/

  /*! exports provided: default */

  /***/
  function node_modulesRawLoaderDistCjsJsSrcAppAutoventaAutoventaPageHtml(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "<app-header titulo= \"AutoVenta\"></app-header>\n\n<ion-content class=\"ion-padding\">\n  <ion-list>\n      <ion-item *ngFor=\"let item of arrayPosts\" (click)=\"detallePublicacion(item)\">\n        <ion-thumbnail>\n          <img [src]=\"item.foto ? item.foto: imgNotCar\" />\n        </ion-thumbnail>\n          <!-- <ion-img [src]=\"item.imagenes_autos[0]\" (click)=\"detalles()\" style=\"width: 100px; height: 90px;\"></ion-img> -->\n          <ion-label class=\"datos\">\n            <h3>{{ item.nombre }}</h3>\n            <p>{{ item.ciudad }}</p>\n            <ion-button class=\"head\" >{{ item.precio }} $</ion-button>\n           \n                     <!-- <ion-icon name=\"heart-half\" class=\"ion-float-end\" style=\"zoom:2.0;\"></ion-icon> -->\n          </ion-label>\n          <div align=\"right\">\n            <label >Ver</label> \n           </div>\n        </ion-item>\n  </ion-list>\n  <ion-infinite-scroll threshold=\"100px\" (ionInfinite)=\"loadData($event)\">\n      <ion-infinite-scroll-content\n        loadingSpinner=\"crescent\"\n        loadingText=\"Cargando mas...\">\n      </ion-infinite-scroll-content>\n    </ion-infinite-scroll>\n</ion-content>\n\n<app-footer></app-footer>";
    /***/
  },

  /***/
  "./src/app/autoventa/autoventa-routing.module.ts":
  /*!*******************************************************!*\
    !*** ./src/app/autoventa/autoventa-routing.module.ts ***!
    \*******************************************************/

  /*! exports provided: AutoventaPageRoutingModule */

  /***/
  function srcAppAutoventaAutoventaRoutingModuleTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "AutoventaPageRoutingModule", function () {
      return AutoventaPageRoutingModule;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/fesm2015/router.js");
    /* harmony import */


    var _autoventa_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! ./autoventa.page */
    "./src/app/autoventa/autoventa.page.ts");

    var routes = [{
      path: '',
      component: _autoventa_page__WEBPACK_IMPORTED_MODULE_3__["AutoventaPage"]
    }];

    var AutoventaPageRoutingModule = function AutoventaPageRoutingModule() {
      _classCallCheck(this, AutoventaPageRoutingModule);
    };

    AutoventaPageRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
      imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
      exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
    })], AutoventaPageRoutingModule);
    /***/
  },

  /***/
  "./src/app/autoventa/autoventa.module.ts":
  /*!***********************************************!*\
    !*** ./src/app/autoventa/autoventa.module.ts ***!
    \***********************************************/

  /*! exports provided: AutoventaPageModule */

  /***/
  function srcAppAutoventaAutoventaModuleTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "AutoventaPageModule", function () {
      return AutoventaPageModule;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/common */
    "./node_modules/@angular/common/fesm2015/common.js");
    /* harmony import */


    var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! @angular/forms */
    "./node_modules/@angular/forms/fesm2015/forms.js");
    /* harmony import */


    var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! @ionic/angular */
    "./node_modules/@ionic/angular/dist/fesm5.js");
    /* harmony import */


    var _autoventa_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! ./autoventa-routing.module */
    "./src/app/autoventa/autoventa-routing.module.ts");
    /* harmony import */


    var _autoventa_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! ./autoventa.page */
    "./src/app/autoventa/autoventa.page.ts");
    /* harmony import */


    var _components_components_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
    /*! ../components/components.module */
    "./src/app/components/components.module.ts");

    var AutoventaPageModule = function AutoventaPageModule() {
      _classCallCheck(this, AutoventaPageModule);
    };

    AutoventaPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
      imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"], _components_components_module__WEBPACK_IMPORTED_MODULE_7__["ComponentsModule"], _autoventa_routing_module__WEBPACK_IMPORTED_MODULE_5__["AutoventaPageRoutingModule"]],
      declarations: [_autoventa_page__WEBPACK_IMPORTED_MODULE_6__["AutoventaPage"]]
    })], AutoventaPageModule);
    /***/
  },

  /***/
  "./src/app/autoventa/autoventa.page.scss":
  /*!***********************************************!*\
    !*** ./src/app/autoventa/autoventa.page.scss ***!
    \***********************************************/

  /*! exports provided: default */

  /***/
  function srcAppAutoventaAutoventaPageScss(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = ".datos {\n  margin-left: 15px;\n  margin-right: 15px;\n}\n\nion-thumbnail {\n  width: 150px;\n  height: 100px;\n}\n\n.head {\n  --background: #f28f29;\n}\n\napp-footer {\n  height: 60px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9pYmVyL0RvY3VtZW50cy9pYmVyL3Byb2plY3RzL3ZlY29tbW9iaWxlL3NyYy9hcHAvYXV0b3ZlbnRhL2F1dG92ZW50YS5wYWdlLnNjc3MiLCJzcmMvYXBwL2F1dG92ZW50YS9hdXRvdmVudGEucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0ksaUJBQUE7RUFDQSxrQkFBQTtBQ0NKOztBRENBO0VBQ0ksWUFBQTtFQUNBLGFBQUE7QUNFSjs7QURBQTtFQUNJLHFCQUFBO0FDR0o7O0FEREE7RUFDSSxZQUFBO0FDSUoiLCJmaWxlIjoic3JjL2FwcC9hdXRvdmVudGEvYXV0b3ZlbnRhLnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi5kYXRvc3sgICAgXHJcbiAgICBtYXJnaW4tbGVmdDogMTVweDsgXHJcbiAgICBtYXJnaW4tcmlnaHQ6IDE1cHg7XHJcbn1cclxuaW9uLXRodW1ibmFpbHtcclxuICAgIHdpZHRoOiAxNTBweDtcclxuICAgIGhlaWdodDogMTAwcHg7XHJcbn1cclxuLmhlYWR7XHJcbiAgICAtLWJhY2tncm91bmQ6ICNmMjhmMjk7XHJcbn1cclxuYXBwLWZvb3RlcntcclxuICAgIGhlaWdodDogNjBweDtcclxuICB9XHJcblxyXG4iLCIuZGF0b3Mge1xuICBtYXJnaW4tbGVmdDogMTVweDtcbiAgbWFyZ2luLXJpZ2h0OiAxNXB4O1xufVxuXG5pb24tdGh1bWJuYWlsIHtcbiAgd2lkdGg6IDE1MHB4O1xuICBoZWlnaHQ6IDEwMHB4O1xufVxuXG4uaGVhZCB7XG4gIC0tYmFja2dyb3VuZDogI2YyOGYyOTtcbn1cblxuYXBwLWZvb3RlciB7XG4gIGhlaWdodDogNjBweDtcbn0iXX0= */";
    /***/
  },

  /***/
  "./src/app/autoventa/autoventa.page.ts":
  /*!*********************************************!*\
    !*** ./src/app/autoventa/autoventa.page.ts ***!
    \*********************************************/

  /*! exports provided: AutoventaPage */

  /***/
  function srcAppAutoventaAutoventaPageTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "AutoventaPage", function () {
      return AutoventaPage;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @ionic/angular */
    "./node_modules/@ionic/angular/dist/fesm5.js");
    /* harmony import */


    var _servicios_servicios_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! ../servicios/servicios.service */
    "./src/app/servicios/servicios.service.ts");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/fesm2015/router.js");
    /* harmony import */


    var _servicios_publicidad_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! ../servicios/publicidad.service */
    "./src/app/servicios/publicidad.service.ts");

    var AutoventaPage =
    /*#__PURE__*/
    function () {
      function AutoventaPage(navCtrl, loadingController, toastController, service, service2, router) {
        _classCallCheck(this, AutoventaPage);

        this.navCtrl = navCtrl;
        this.loadingController = loadingController;
        this.toastController = toastController;
        this.service = service;
        this.service2 = service2;
        this.router = router;
        this.imgNotCar = 'assets/images/notfound.jpg';
      }

      _createClass(AutoventaPage, [{
        key: "ionViewWillEnter",
        value: function ionViewWillEnter() {
          return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0,
          /*#__PURE__*/
          regeneratorRuntime.mark(function _callee2() {
            var _this = this;

            var loading;
            return regeneratorRuntime.wrap(function _callee2$(_context2) {
              while (1) {
                switch (_context2.prev = _context2.next) {
                  case 0:
                    this.autito();
                    this.page = 1;
                    _context2.next = 4;
                    return this.loadingController.create({
                      message: 'Cargando lista VECOM'
                    });

                  case 4:
                    loading = _context2.sent;
                    _context2.next = 7;
                    return loading.present().then(function () {
                      _this.service.listaVehiculoVecom(_this.page).subscribe(function (data) {
                        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](_this, void 0, void 0,
                        /*#__PURE__*/
                        regeneratorRuntime.mark(function _callee() {
                          return regeneratorRuntime.wrap(function _callee$(_context) {
                            while (1) {
                              switch (_context.prev = _context.next) {
                                case 0:
                                  this.arrayPosts = data.data; // this.categorias = data.data;

                                  console.log(this.arrayPosts);
                                  _context.next = 4;
                                  return loading.dismiss();

                                case 4:
                                  this.msjToast();

                                case 5:
                                case "end":
                                  return _context.stop();
                              }
                            }
                          }, _callee, this);
                        }));
                      }, function (err) {
                        console.log(err);
                        loading.dismiss();
                      });
                    });

                  case 7:
                  case "end":
                    return _context2.stop();
                }
              }
            }, _callee2, this);
          }));
        }
      }, {
        key: "loadData",
        value: function loadData(event) {
          var _this2 = this;

          setTimeout(function () {
            _this2.page++;

            _this2.service.listaVehiculoVecom(_this2.page).subscribe(function (data) {
              data = data.data;
              console.log(data);
              var list = data;

              if (list.length > 0) {
                var _iteratorNormalCompletion = true;
                var _didIteratorError = false;
                var _iteratorError = undefined;

                try {
                  for (var _iterator = list[Symbol.iterator](), _step; !(_iteratorNormalCompletion = (_step = _iterator.next()).done); _iteratorNormalCompletion = true) {
                    var item = _step.value;

                    _this2.arrayPosts.push(item);
                  }
                } catch (err) {
                  _didIteratorError = true;
                  _iteratorError = err;
                } finally {
                  try {
                    if (!_iteratorNormalCompletion && _iterator.return != null) {
                      _iterator.return();
                    }
                  } finally {
                    if (_didIteratorError) {
                      throw _iteratorError;
                    }
                  }
                }

                event.target.complete();
              } else {
                event.target.disabled = true;
                event.target.complete();
              }
            }, function (err) {
              _this2.message();
            });
          }, 500);
        }
      }, {
        key: "detallePublicacion",
        value: function detallePublicacion(c) {
          var idVehiculo = c.id;
          this.router.navigate(['detalle-auto', {
            detalleAutoVecom: idVehiculo
          }]);
        }
      }, {
        key: "message",
        value: function message() {
          return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0,
          /*#__PURE__*/
          regeneratorRuntime.mark(function _callee3() {
            var loading, _ref, role, data;

            return regeneratorRuntime.wrap(function _callee3$(_context3) {
              while (1) {
                switch (_context3.prev = _context3.next) {
                  case 0:
                    _context3.next = 2;
                    return this.loadingController.create({
                      message: 'Error de Conexion'
                    });

                  case 2:
                    loading = _context3.sent;
                    _context3.next = 5;
                    return loading.present();

                  case 5:
                    _context3.next = 7;
                    return loading.onDidDismiss();

                  case 7:
                    _ref = _context3.sent;
                    role = _ref.role;
                    data = _ref.data;

                  case 10:
                  case "end":
                    return _context3.stop();
                }
              }
            }, _callee3, this);
          }));
        }
      }, {
        key: "msjToast",
        value: function msjToast() {
          return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0,
          /*#__PURE__*/
          regeneratorRuntime.mark(function _callee4() {
            var toast;
            return regeneratorRuntime.wrap(function _callee4$(_context4) {
              while (1) {
                switch (_context4.prev = _context4.next) {
                  case 0:
                    _context4.next = 2;
                    return this.toastController.create({
                      message: 'Datos mostrados.',
                      duration: 2000
                    });

                  case 2:
                    toast = _context4.sent;
                    toast.present();

                  case 4:
                  case "end":
                    return _context4.stop();
                }
              }
            }, _callee4, this);
          }));
        }
      }, {
        key: "autito",
        value: function autito() {
          return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0,
          /*#__PURE__*/
          regeneratorRuntime.mark(function _callee6() {
            var _this3 = this;

            var loading;
            return regeneratorRuntime.wrap(function _callee6$(_context6) {
              while (1) {
                switch (_context6.prev = _context6.next) {
                  case 0:
                    _context6.next = 2;
                    return this.loadingController.create({
                      message: 'Cargando...'
                    });

                  case 2:
                    loading = _context6.sent;
                    _context6.next = 5;
                    return loading.present().then(function () {
                      _this3.service2.listaPublicidadesVecom().subscribe(function (data) {
                        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](_this3, void 0, void 0,
                        /*#__PURE__*/
                        regeneratorRuntime.mark(function _callee5() {
                          return regeneratorRuntime.wrap(function _callee5$(_context5) {
                            while (1) {
                              switch (_context5.prev = _context5.next) {
                                case 0:
                                  this.arrayPosts = data; // this.categorias = data.data;

                                  console.log(this.arrayPosts);
                                  _context5.next = 4;
                                  return loading.dismiss();

                                case 4:
                                case "end":
                                  return _context5.stop();
                              }
                            }
                          }, _callee5, this);
                        }));
                      }, function (err) {
                        console.log(err);
                        loading.dismiss();
                      });
                    });

                  case 5:
                  case "end":
                    return _context6.stop();
                }
              }
            }, _callee6, this);
          }));
        }
      }]);

      return AutoventaPage;
    }();

    AutoventaPage.ctorParameters = function () {
      return [{
        type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["NavController"]
      }, {
        type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["LoadingController"]
      }, {
        type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ToastController"]
      }, {
        type: _servicios_servicios_service__WEBPACK_IMPORTED_MODULE_3__["ServiciosService"]
      }, {
        type: _servicios_publicidad_service__WEBPACK_IMPORTED_MODULE_5__["PublicidadService"]
      }, {
        type: _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"]
      }];
    };

    AutoventaPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
      selector: 'app-autoventa',
      template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! raw-loader!./autoventa.page.html */
      "./node_modules/raw-loader/dist/cjs.js!./src/app/autoventa/autoventa.page.html")).default,
      styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! ./autoventa.page.scss */
      "./src/app/autoventa/autoventa.page.scss")).default]
    }), tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_angular__WEBPACK_IMPORTED_MODULE_2__["NavController"], _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["LoadingController"], _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ToastController"], _servicios_servicios_service__WEBPACK_IMPORTED_MODULE_3__["ServiciosService"], _servicios_publicidad_service__WEBPACK_IMPORTED_MODULE_5__["PublicidadService"], _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"]])], AutoventaPage);
    /***/
  }
}]);
//# sourceMappingURL=autoventa-autoventa-module-es5.js.map