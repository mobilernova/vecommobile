(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["default~agregar-auto-agregar-auto-module~agregar-fotografia-agregar-fotografia-module~autoventa-auto~5893b857"],{

/***/ "./src/app/servicios/servicios.service.ts":
/*!************************************************!*\
  !*** ./src/app/servicios/servicios.service.ts ***!
  \************************************************/
/*! exports provided: ServiciosService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ServiciosService", function() { return ServiciosService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm2015/http.js");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm2015/index.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm2015/operators/index.js");





let ServiciosService = class ServiciosService {
    constructor(http) {
        this.http = http;
        this.url = 'https://jsonplaceholder.typicode.com/';
        this.objectSource = new rxjs__WEBPACK_IMPORTED_MODULE_3__["BehaviorSubject"]({});
        this.$getObjectSource = this.objectSource.asObservable();
        this.items = [];
        this.path = 'https://vecom.academiamodernadallas.com/api';
        this.page_size = 10;
    }
    sendObjectSource(data) {
        this.objectSource.next(data);
    }
    getDatos() {
        return this.http.get(this.url + 'users')
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["tap"])((data) => {
            return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["of"])(data);
        }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["catchError"])((err) => {
            console.log(err);
            return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["throwError"])(err);
        }));
    }
    /**
    * service login
    */
    loginVecom(value, email, nombre = '', celular = '', password = '', foto = '') {
        this.httpOptions = {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"]({
                'Content-Type': 'application/json',
            }),
        };
        var datoaEnviar = {
            "tipo_login": value,
            "email": email,
            "nombre_completo": nombre,
            "telefono_celular": celular,
            "password": password,
            "foto": foto
        };
        return this.http.post(this.path + "/user/login", datoaEnviar, this.httpOptions)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["tap"])((data) => {
            return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["of"])(data);
        }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["catchError"])((err) => {
            return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["throwError"])(err);
        }));
    }
    /**
    * service Registro usuario
    */
    registroVecom(nombre, celular, email, password) {
        this.httpOptions = {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"]({
                'Content-Type': 'application/json',
            }),
        };
        var datoaEnviar = {
            "nombre_completo": nombre,
            "email": email,
            "celular": celular,
            "password": password,
        };
        return this.http.post(this.path + "/user/signup", datoaEnviar, this.httpOptions)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["tap"])((data) => {
            return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["of"])(data);
        }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["catchError"])((err) => {
            return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["throwError"])(err);
        }));
    }
    /**
    * service Perfil
    */
    perfilUsuario() {
        this.accesToken = localStorage.getItem('access_token');
        this.httpOptions = {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"]({
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + this.accesToken
            }),
        };
        return this.http.post(this.path + '/user/perfil', null, this.httpOptions)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["tap"])((data) => {
            return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["of"])(data);
        }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["catchError"])((err) => {
            return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["throwError"])(err);
        }));
    }
    /**
    * service buscar automovil
    */
    searchVecom(player_id, value, email, nombre = '', celular = '', password = '', foto = '') {
        this.httpOptions = {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"]({
                'Content-Type': 'application/json',
            }),
        };
        var datoaEnviar = {
            "tipo_login": value,
            "email": email,
            "nombre_completo": nombre,
            "telefono_celular": celular,
            "password": password,
            "player_id": player_id,
            "foto": foto
        };
        return this.http.post(this.path + "/user/login", datoaEnviar, this.httpOptions)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["tap"])((data) => {
            return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["of"])(data);
        }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["catchError"])((err) => {
            return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["throwError"])(err);
        }));
    }
    /**
    * service Editar Usuario
    */
    editarUsuarioVecom(nombre, email, celular, fecha_nacimiento, foto) {
        this.accesToken = localStorage.getItem('access_token');
        this.httpOptions = {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"]({
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + this.accesToken
            }),
        };
        var datoaEnviar = {
            "nombre_completo": nombre,
            "email": email,
            "celular": celular,
            "fecha_nacimiento": fecha_nacimiento,
            "foto": foto
        };
        return this.http.post(this.path + "/user/usuarioedit", datoaEnviar, this.httpOptions)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["tap"])((data) => {
            return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["of"])(data);
        }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["catchError"])((err) => {
            return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["throwError"])(err);
        }));
    }
    /*
    / Servicio de modelos
    */
    getModelos(id) {
        this.accesToken = localStorage.getItem('access_token');
        this.httpOptions = {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"]({
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + this.accesToken
            }),
        };
        return this.http.get(this.path + '/vehiculo/modelos/?marca_id=' + id + '', this.httpOptions)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["tap"])((data) => {
            return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["of"])(data);
        }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["catchError"])((err) => {
            console.log(err);
            return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["throwError"])(err);
        }));
    }
    /*
      / Servicio de marcas
      */
    getMarcas() {
        this.accesToken = localStorage.getItem('access_token');
        this.httpOptions = {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"]({
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + this.accesToken
            }),
        };
        return this.http.get(this.path + '/vehiculo/marcas', this.httpOptions)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["tap"])((data) => {
            return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["of"])(data);
        }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["catchError"])((err) => {
            console.log(err);
            return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["throwError"])(err);
        }));
    }
    /*
      / Servicio de tipos vehiculos
      */
    getTipoVehiculo() {
        this.accesToken = localStorage.getItem('access_token');
        this.httpOptions = {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"]({
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + this.accesToken
            }),
        };
        return this.http.get(this.path + '/vehiculo/tipovehiculo', this.httpOptions)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["tap"])((data) => {
            return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["of"])(data);
        }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["catchError"])((err) => {
            console.log(err);
            return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["throwError"])(err);
        }));
    }
    addVehiculoVecom(nombre, color, puertas, cilindrada, tipo_combustible, caracteristicas, ciudad, precio, anho, marca_id, modelo_id, tipo_vehiculo_id, user_id, foto) {
        this.accesToken = localStorage.getItem('access_token');
        this.httpOptions = {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"]({
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + this.accesToken
            }),
        };
        var datoaEnviar = {
            "nombre": nombre,
            "color": color,
            "puertas": puertas,
            "cilindrada": cilindrada,
            "tipo_combustible": tipo_combustible,
            "caracteristicas": caracteristicas,
            "ciudad": ciudad,
            "precio": precio,
            "año": anho,
            "marcas_id": marca_id,
            "empresa_id": "",
            "modelo_id": modelo_id,
            "tipo_vehiculo_id": tipo_vehiculo_id,
            "usuario_app_id": user_id,
            "datos": foto
        };
        return this.http.post(this.path + "/vehiculo/crearvehiculo", datoaEnviar, this.httpOptions)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["tap"])((data) => {
            return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["of"])(data);
        }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["catchError"])((err) => {
            return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["throwError"])(err);
        }));
    }
    editVehiculoVecom(id, nombre, color, puertas, cilindrada, tipo_combustible, caracteristicas, ciudad, precio, anho, marca_id, modelo_id, tipo_vehiculo_id, caja, user_id) {
        this.accesToken = localStorage.getItem('access_token');
        this.httpOptions = {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"]({
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + this.accesToken
            }),
        };
        var datoaEnviar = {
            "nombre": nombre,
            "color": color,
            "puertas": puertas,
            "cilindrada": cilindrada,
            "tipo_combustible": tipo_combustible,
            "caracteristicas": caracteristicas,
            "ciudad": ciudad,
            "precio": precio,
            "anho": anho,
            "marcas_id": marca_id,
            "modelo_id": modelo_id,
            "tipo_vehiculo_id": tipo_vehiculo_id,
            "caja": caja,
            "usuario_app_id": user_id,
        };
        return this.http.post(this.path + "/vehiculo/editarvehiculo?id=" + id + '', datoaEnviar, this.httpOptions)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["tap"])((data) => {
            return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["of"])(data);
        }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["catchError"])((err) => {
            return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["throwError"])(err);
        }));
    }
    addPhotography(foto) {
        this.items.push({ "foto": foto });
    }
    loadAll() {
        return Promise.resolve(this.items);
    }
    ;
    removeItem(ndx) {
        this.items.splice(ndx, 1);
    }
    deleteItem() {
        this.items = [];
    }
    /*
    / Servicio de mis publicaciones
    */
    getMisPublicaciones() {
        this.accesToken = localStorage.getItem('access_token');
        this.httpOptions = {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"]({
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + this.accesToken
            }),
        };
        return this.http.get(this.path + '/vehiculo/publicaciones', this.httpOptions)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["tap"])((data) => {
            return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["of"])(data);
        }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["catchError"])((err) => {
            console.log(err);
            return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["throwError"])(err);
        }));
    }
    /*
    / Servicio de mis favoritos
    */
    getMisFavoritos() {
        this.accesToken = localStorage.getItem('access_token');
        this.httpOptions = {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"]({
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + this.accesToken
            }),
        };
        return this.http.get(this.path + '/favorito/favoritos', this.httpOptions)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["tap"])((data) => {
            return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["of"])(data);
        }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["catchError"])((err) => {
            console.log(err);
            return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["throwError"])(err);
        }));
    }
    /*
      / Servicio detalle automovil
      */
    getDetalleAutomovil(idAutomovil) {
        this.accesToken = localStorage.getItem('access_token');
        this.httpOptions = {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"]({
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + this.accesToken
            }),
        };
        return this.http.post(this.path + '/vehiculo/detalleauto?id=' + idAutomovil + '', null, this.httpOptions)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["tap"])((data) => {
            return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["of"])(data);
        }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["catchError"])((err) => {
            console.log(err);
            return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["throwError"])(err);
        }));
    }
    /*
      / Servicio editar automovil
      */
    getEditarAutomovil(idAutomovil) {
        this.accesToken = localStorage.getItem('access_token');
        this.httpOptions = {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"]({
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + this.accesToken
            }),
        };
        return this.http.get(this.path + '/vehiculo/edit?id=' + idAutomovil + '', this.httpOptions)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["tap"])((data) => {
            return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["of"])(data);
        }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["catchError"])((err) => {
            console.log(err);
            return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["throwError"])(err);
        }));
    }
    /*
      / Servicio de galleria vehiculo
      */
    galleriaVehiculo(id) {
        this.accesToken = localStorage.getItem('access_token');
        this.httpOptions = {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"]({
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + this.accesToken
            }),
        };
        return this.http.get(this.path + '/vehiculo/galeria?id=' + id + '', this.httpOptions)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["tap"])((data) => {
            return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["of"])(data);
        }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["catchError"])((err) => {
            console.log(err);
            return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["throwError"])(err);
        }));
    }
    /*
    / Servicio de eliminar fotografia vehiculo
    */
    eliminarFotografiaVehiculo(id) {
        this.accesToken = localStorage.getItem('access_token');
        this.httpOptions = {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"]({
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + this.accesToken
            }),
        };
        return this.http.post(this.path + '/vehiculo/quitar-imagen?id=' + id + '', null, this.httpOptions)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["tap"])((data) => {
            return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["of"])(data);
        }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["catchError"])((err) => {
            console.log(err);
            return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["throwError"])(err);
        }));
    }
    /*
     / Servicio de añadir fotografia vehiculo
     */
    anhadirFotografiaVehiculo(idVehiculo, foto) {
        this.accesToken = localStorage.getItem('access_token');
        this.httpOptions = {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"]({
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + this.accesToken
            }),
        };
        var datoaEnviar = {
            "foto": foto,
        };
        return this.http.post(this.path + '/vehiculo/agregarfoto?id=' + idVehiculo + '', datoaEnviar, this.httpOptions)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["tap"])((data) => {
            return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["of"])(data);
        }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["catchError"])((err) => {
            console.log(err);
            return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["throwError"])(err);
        }));
    }
    /*
    / Servicio de estadisticas vehiculo
    */
    estadisticasVehiculoVecom(idAutomovil) {
        this.accesToken = localStorage.getItem('access_token');
        this.httpOptions = {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"]({
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + this.accesToken
            }),
        };
        return this.http.get(this.path + '/vehiculo/count?id=' + idAutomovil + '', this.httpOptions)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["tap"])((data) => {
            return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["of"])(data);
        }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["catchError"])((err) => {
            console.log(err);
            return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["throwError"])(err);
        }));
    }
    /*
      / Servicio de cambiar estado de vehiculo
      */
    estadoVehiculoVecom(idAutomovil) {
        this.accesToken = localStorage.getItem('access_token');
        this.httpOptions = {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"]({
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + this.accesToken
            }),
        };
        return this.http.post(this.path + '/vehiculo/estado?id=' + idAutomovil + '', null, this.httpOptions)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["tap"])((data) => {
            return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["of"])(data);
        }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["catchError"])((err) => {
            console.log(err);
            return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["throwError"])(err);
        }));
    }
    /*
  / Servicio de lista de  vehiculos
  */
    listaVehiculoVecom(page) {
        this.page_size = 6;
        this.accesToken = localStorage.getItem('access_token');
        this.httpOptions = {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"]({
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + this.accesToken
            }),
        };
        return this.http.get(this.path + "/vehiculo/vehiculos" + "?page=" + page + "&page_size=" + this.page_size + "", this.httpOptions)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["tap"])((data) => {
            return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["of"])(data);
        }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["catchError"])((err) => {
            console.log(err);
            return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["throwError"])(err);
        }));
    }
    /*
    / Servicio de crear favorito
    */
    anhadirFavoritoVehiculoVecom(idAutomovil) {
        this.accesToken = localStorage.getItem('access_token');
        this.httpOptions = {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"]({
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + this.accesToken
            }),
        };
        return this.http.post(this.path + '/favorito/create-favorito?id=' + idAutomovil + '', null, this.httpOptions)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["tap"])((data) => {
            return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["of"])(data);
        }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["catchError"])((err) => {
            console.log(err);
            return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["throwError"])(err);
        }));
    }
    /*
      / Servicio de eliminar favorito
      */
    eliminarFavoritoVehiculoVecom(idAutomovil) {
        this.accesToken = localStorage.getItem('access_token');
        this.httpOptions = {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"]({
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + this.accesToken
            }),
        };
        return this.http.post(this.path + '/favorito/delete-favorito?id=' + idAutomovil + '', null, this.httpOptions)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["tap"])((data) => {
            return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["of"])(data);
        }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["catchError"])((err) => {
            console.log(err);
            return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["throwError"])(err);
        }));
    }
};
ServiciosService.ctorParameters = () => [
    { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"] }
];
ServiciosService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
        providedIn: 'root'
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"]])
], ServiciosService);



/***/ })

}]);
//# sourceMappingURL=default~agregar-auto-agregar-auto-module~agregar-fotografia-agregar-fotografia-module~autoventa-auto~5893b857-es2015.js.map