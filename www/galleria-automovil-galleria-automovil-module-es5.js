function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["galleria-automovil-galleria-automovil-module"], {
  /***/
  "./node_modules/raw-loader/dist/cjs.js!./src/app/galleria-automovil/galleria-automovil.page.html":
  /*!*******************************************************************************************************!*\
    !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/galleria-automovil/galleria-automovil.page.html ***!
    \*******************************************************************************************************/

  /*! exports provided: default */

  /***/
  function node_modulesRawLoaderDistCjsJsSrcAppGalleriaAutomovilGalleriaAutomovilPageHtml(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "<ion-header>\n  <ion-toolbar class=\"head\">\n    <ion-buttons slot=\"start\" (click)=\"back()\">\n      <!-- <ion-back-button color=\"light\"></ion-back-button> -->\n      <ion-icon slot=\"start\" name=\"arrow-back\" color=\"light\"></ion-icon>\n    </ion-buttons>\n  <ion-title align=\"center\" color=\"light\">Galeria de Fotos</ion-title>\n  <ion-buttons slot=\"end\">\n    <ion-menu-button  style=\"color: #ffffff;\"></ion-menu-button>\n  </ion-buttons>\n</ion-toolbar>\n</ion-header>\n\n<ion-content>\n  <ion-grid>\n      <ion-row>\n        <ion-col size=\"6\" *ngFor=\"let item of imageAllProducto\" >\n          <ion-card >\n            <img [src]=\"item.foto\" />\n          </ion-card>\n          <div align=\"center\">\n              <ion-button icon-left color=\"danger\" (click)=\"deleteFotography(item.id)\">\n                  <ion-icon name=\"close\"></ion-icon> \n              </ion-button>\n          </div>\n        </ion-col>\n      </ion-row>\n    </ion-grid>\n\n\n    <ion-fab class=\"head\" horizontal=\"end\" vertical=\"bottom\" slot=\"fixed\">\n      <ion-fab-button  (click)=\"addImage()\">\n        <ion-icon name=\"add-circle-outline\"></ion-icon>\n    </ion-fab-button>\n    </ion-fab>\n\n    \n</ion-content>\n";
    /***/
  },

  /***/
  "./src/app/galleria-automovil/galleria-automovil-routing.module.ts":
  /*!*************************************************************************!*\
    !*** ./src/app/galleria-automovil/galleria-automovil-routing.module.ts ***!
    \*************************************************************************/

  /*! exports provided: GalleriaAutomovilPageRoutingModule */

  /***/
  function srcAppGalleriaAutomovilGalleriaAutomovilRoutingModuleTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "GalleriaAutomovilPageRoutingModule", function () {
      return GalleriaAutomovilPageRoutingModule;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/fesm2015/router.js");
    /* harmony import */


    var _galleria_automovil_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! ./galleria-automovil.page */
    "./src/app/galleria-automovil/galleria-automovil.page.ts");

    var routes = [{
      path: '',
      component: _galleria_automovil_page__WEBPACK_IMPORTED_MODULE_3__["GalleriaAutomovilPage"]
    }];

    var GalleriaAutomovilPageRoutingModule = function GalleriaAutomovilPageRoutingModule() {
      _classCallCheck(this, GalleriaAutomovilPageRoutingModule);
    };

    GalleriaAutomovilPageRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
      imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
      exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
    })], GalleriaAutomovilPageRoutingModule);
    /***/
  },

  /***/
  "./src/app/galleria-automovil/galleria-automovil.module.ts":
  /*!*****************************************************************!*\
    !*** ./src/app/galleria-automovil/galleria-automovil.module.ts ***!
    \*****************************************************************/

  /*! exports provided: GalleriaAutomovilPageModule */

  /***/
  function srcAppGalleriaAutomovilGalleriaAutomovilModuleTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "GalleriaAutomovilPageModule", function () {
      return GalleriaAutomovilPageModule;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/common */
    "./node_modules/@angular/common/fesm2015/common.js");
    /* harmony import */


    var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! @angular/forms */
    "./node_modules/@angular/forms/fesm2015/forms.js");
    /* harmony import */


    var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! @ionic/angular */
    "./node_modules/@ionic/angular/dist/fesm5.js");
    /* harmony import */


    var _galleria_automovil_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! ./galleria-automovil-routing.module */
    "./src/app/galleria-automovil/galleria-automovil-routing.module.ts");
    /* harmony import */


    var _galleria_automovil_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! ./galleria-automovil.page */
    "./src/app/galleria-automovil/galleria-automovil.page.ts");

    var GalleriaAutomovilPageModule = function GalleriaAutomovilPageModule() {
      _classCallCheck(this, GalleriaAutomovilPageModule);
    };

    GalleriaAutomovilPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
      imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"], _galleria_automovil_routing_module__WEBPACK_IMPORTED_MODULE_5__["GalleriaAutomovilPageRoutingModule"]],
      declarations: [_galleria_automovil_page__WEBPACK_IMPORTED_MODULE_6__["GalleriaAutomovilPage"]]
    })], GalleriaAutomovilPageModule);
    /***/
  },

  /***/
  "./src/app/galleria-automovil/galleria-automovil.page.scss":
  /*!*****************************************************************!*\
    !*** ./src/app/galleria-automovil/galleria-automovil.page.scss ***!
    \*****************************************************************/

  /*! exports provided: default */

  /***/
  function srcAppGalleriaAutomovilGalleriaAutomovilPageScss(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "ion-fab-button {\n  --background: #0ECFA8;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9pYmVyL0RvY3VtZW50cy9pYmVyL3Byb2plY3RzL3ZlY29tbW9iaWxlL3NyYy9hcHAvZ2FsbGVyaWEtYXV0b21vdmlsL2dhbGxlcmlhLWF1dG9tb3ZpbC5wYWdlLnNjc3MiLCJzcmMvYXBwL2dhbGxlcmlhLWF1dG9tb3ZpbC9nYWxsZXJpYS1hdXRvbW92aWwucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0kscUJBQUE7QUNDSiIsImZpbGUiOiJzcmMvYXBwL2dhbGxlcmlhLWF1dG9tb3ZpbC9nYWxsZXJpYS1hdXRvbW92aWwucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiaW9uLWZhYi1idXR0b257XG4gICAgLS1iYWNrZ3JvdW5kOiAjMEVDRkE4O1xufSIsImlvbi1mYWItYnV0dG9uIHtcbiAgLS1iYWNrZ3JvdW5kOiAjMEVDRkE4O1xufSJdfQ== */";
    /***/
  },

  /***/
  "./src/app/galleria-automovil/galleria-automovil.page.ts":
  /*!***************************************************************!*\
    !*** ./src/app/galleria-automovil/galleria-automovil.page.ts ***!
    \***************************************************************/

  /*! exports provided: GalleriaAutomovilPage */

  /***/
  function srcAppGalleriaAutomovilGalleriaAutomovilPageTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "GalleriaAutomovilPage", function () {
      return GalleriaAutomovilPage;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/fesm2015/router.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _ionic_angular__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! @ionic/angular */
    "./node_modules/@ionic/angular/dist/fesm5.js");
    /* harmony import */


    var _servicios_servicios_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! ../servicios/servicios.service */
    "./src/app/servicios/servicios.service.ts"); // import { MainService } from './../services/main.service';


    var GalleriaAutomovilPage =
    /*#__PURE__*/
    function () {
      function GalleriaAutomovilPage(modalController, alertController, service, route, navCtrl, loadingCtrl, toastCtrl, platform) {
        var _this = this;

        _classCallCheck(this, GalleriaAutomovilPage);

        this.modalController = modalController;
        this.alertController = alertController;
        this.service = service;
        this.route = route;
        this.navCtrl = navCtrl;
        this.loadingCtrl = loadingCtrl;
        this.toastCtrl = toastCtrl;
        this.platform = platform;
        this.platform.backButton.subscribe(function () {
          _this.navCtrl.navigateRoot('/perfil-usuario');

          localStorage.removeItem('preventIdAutomovil');
        });
      }

      _createClass(GalleriaAutomovilPage, [{
        key: "ionViewWillEnter",
        value: function ionViewWillEnter() {
          this.galeria();
        }
      }, {
        key: "galeria",
        value: function galeria() {
          return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0,
          /*#__PURE__*/
          regeneratorRuntime.mark(function _callee2() {
            var _this2 = this;

            var loading;
            return regeneratorRuntime.wrap(function _callee2$(_context2) {
              while (1) {
                switch (_context2.prev = _context2.next) {
                  case 0:
                    this.idVehiculo = localStorage.getItem('preventIdAutomovil');
                    _context2.next = 3;
                    return this.loadingCtrl.create({
                      message: 'Cargando...'
                    });

                  case 3:
                    loading = _context2.sent;
                    _context2.next = 6;
                    return loading.present().then(function () {
                      _this2.service.galleriaVehiculo(_this2.idVehiculo).subscribe(function (data) {
                        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](_this2, void 0, void 0,
                        /*#__PURE__*/
                        regeneratorRuntime.mark(function _callee() {
                          return regeneratorRuntime.wrap(function _callee$(_context) {
                            while (1) {
                              switch (_context.prev = _context.next) {
                                case 0:
                                  this.imageAllProducto = data.data;
                                  console.log(this.imageAllProducto);
                                  _context.next = 4;
                                  return loading.dismiss();

                                case 4:
                                case "end":
                                  return _context.stop();
                              }
                            }
                          }, _callee, this);
                        }));
                      }, function (err) {
                        loading.dismiss();

                        _this2.message();
                      });
                    });

                  case 6:
                  case "end":
                    return _context2.stop();
                }
              }
            }, _callee2, this);
          }));
        }
      }, {
        key: "deleteFotography",
        value: function deleteFotography(id) {
          return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0,
          /*#__PURE__*/
          regeneratorRuntime.mark(function _callee4() {
            var _this3 = this;

            var alert;
            return regeneratorRuntime.wrap(function _callee4$(_context4) {
              while (1) {
                switch (_context4.prev = _context4.next) {
                  case 0:
                    _context4.next = 2;
                    return this.alertController.create({
                      header: 'Eliminar',
                      message: '<strong>Esta seguro de eliminar esta fotografia?</strong>!!!',
                      buttons: [{
                        text: 'Si',
                        // role: 'cancel',
                        // cssClass: 'secondary',
                        handler: function handler(blah) {
                          _this3.service.eliminarFotografiaVehiculo(id).subscribe(function (data) {
                            return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](_this3, void 0, void 0,
                            /*#__PURE__*/
                            regeneratorRuntime.mark(function _callee3() {
                              return regeneratorRuntime.wrap(function _callee3$(_context3) {
                                while (1) {
                                  switch (_context3.prev = _context3.next) {
                                    case 0:
                                      this.smsSuccessTrue = data.data;
                                      console.log(this.smsSuccessTrue);
                                      this.successRequest(this.smsSuccessTrue.data);
                                      this.galeria();

                                    case 4:
                                    case "end":
                                      return _context3.stop();
                                  }
                                }
                              }, _callee3, this);
                            }));
                          }, function (err) {
                            _this3.message();
                          });
                        }
                      }, {
                        text: 'No',
                        role: 'cancel',
                        handler: function handler() {}
                      }]
                    });

                  case 2:
                    alert = _context4.sent;
                    _context4.next = 5;
                    return alert.present();

                  case 5:
                  case "end":
                    return _context4.stop();
                }
              }
            }, _callee4, this);
          }));
        }
      }, {
        key: "message",
        value: function message() {
          return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0,
          /*#__PURE__*/
          regeneratorRuntime.mark(function _callee5() {
            var loading, _ref, role, data;

            return regeneratorRuntime.wrap(function _callee5$(_context5) {
              while (1) {
                switch (_context5.prev = _context5.next) {
                  case 0:
                    _context5.next = 2;
                    return this.loadingCtrl.create({
                      message: 'Error de Conexion'
                    });

                  case 2:
                    loading = _context5.sent;
                    _context5.next = 5;
                    return loading.present();

                  case 5:
                    _context5.next = 7;
                    return loading.onDidDismiss();

                  case 7:
                    _ref = _context5.sent;
                    role = _ref.role;
                    data = _ref.data;

                  case 10:
                  case "end":
                    return _context5.stop();
                }
              }
            }, _callee5, this);
          }));
        }
      }, {
        key: "successRequest",
        value: function successRequest(sms) {
          return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0,
          /*#__PURE__*/
          regeneratorRuntime.mark(function _callee6() {
            var toast;
            return regeneratorRuntime.wrap(function _callee6$(_context6) {
              while (1) {
                switch (_context6.prev = _context6.next) {
                  case 0:
                    _context6.next = 2;
                    return this.toastCtrl.create({
                      message: sms,
                      duration: 1000,
                      cssClass: "my-custom-class-success"
                    });

                  case 2:
                    toast = _context6.sent;
                    toast.present();

                  case 4:
                  case "end":
                    return _context6.stop();
                }
              }
            }, _callee6, this);
          }));
        } // async addImage() {
        //   const modal = await this.modalController.create({
        //     component: AnhadirFotografiaComponent,
        //     componentProps:{
        //       idVehiculo:this.idVehiculo
        //     },
        //     keyboardClose: true,
        //     showBackdrop: true
        //   });
        //   return await modal.present();
        // }

      }, {
        key: "addImage",
        value: function addImage() {
          // localStorage.setItem("preventIdAutomovil", item);
          this.route.navigate(['/agregar-fotografia']);
        }
      }, {
        key: "back",
        value: function back() {
          this.navCtrl.navigateRoot('/perfil');
          localStorage.removeItem('preventIdAutomovil');
        }
      }]);

      return GalleriaAutomovilPage;
    }();

    GalleriaAutomovilPage.ctorParameters = function () {
      return [{
        type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["ModalController"]
      }, {
        type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["AlertController"]
      }, {
        type: _servicios_servicios_service__WEBPACK_IMPORTED_MODULE_4__["ServiciosService"]
      }, {
        type: _angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"]
      }, {
        type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["NavController"]
      }, {
        type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["LoadingController"]
      }, {
        type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["ToastController"]
      }, {
        type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["Platform"]
      }];
    };

    GalleriaAutomovilPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["Component"])({
      selector: 'app-galleria-automovil',
      template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! raw-loader!./galleria-automovil.page.html */
      "./node_modules/raw-loader/dist/cjs.js!./src/app/galleria-automovil/galleria-automovil.page.html")).default,
      styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! ./galleria-automovil.page.scss */
      "./src/app/galleria-automovil/galleria-automovil.page.scss")).default]
    }), tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_angular__WEBPACK_IMPORTED_MODULE_3__["ModalController"], _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["AlertController"], _servicios_servicios_service__WEBPACK_IMPORTED_MODULE_4__["ServiciosService"], _angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"], _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["NavController"], _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["LoadingController"], _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["ToastController"], _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["Platform"]])], GalleriaAutomovilPage);
    /***/
  }
}]);
//# sourceMappingURL=galleria-automovil-galleria-automovil-module-es5.js.map