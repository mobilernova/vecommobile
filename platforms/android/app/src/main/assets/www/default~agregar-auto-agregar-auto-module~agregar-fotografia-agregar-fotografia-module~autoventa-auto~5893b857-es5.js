function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["default~agregar-auto-agregar-auto-module~agregar-fotografia-agregar-fotografia-module~autoventa-auto~5893b857"], {
  /***/
  "./src/app/servicios/servicios.service.ts":
  /*!************************************************!*\
    !*** ./src/app/servicios/servicios.service.ts ***!
    \************************************************/

  /*! exports provided: ServiciosService */

  /***/
  function srcAppServiciosServiciosServiceTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "ServiciosService", function () {
      return ServiciosService;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/common/http */
    "./node_modules/@angular/common/fesm2015/http.js");
    /* harmony import */


    var rxjs__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! rxjs */
    "./node_modules/rxjs/_esm2015/index.js");
    /* harmony import */


    var rxjs_operators__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! rxjs/operators */
    "./node_modules/rxjs/_esm2015/operators/index.js");

    var ServiciosService =
    /*#__PURE__*/
    function () {
      function ServiciosService(http) {
        _classCallCheck(this, ServiciosService);

        this.http = http;
        this.url = 'https://jsonplaceholder.typicode.com/';
        this.objectSource = new rxjs__WEBPACK_IMPORTED_MODULE_3__["BehaviorSubject"]({});
        this.$getObjectSource = this.objectSource.asObservable();
        this.items = [];
        this.path = 'https://vecom.academiamodernadallas.com/api';
        this.page_size = 10;
      }

      _createClass(ServiciosService, [{
        key: "sendObjectSource",
        value: function sendObjectSource(data) {
          this.objectSource.next(data);
        }
      }, {
        key: "getDatos",
        value: function getDatos() {
          return this.http.get(this.url + 'users').pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["tap"])(function (data) {
            return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["of"])(data);
          }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["catchError"])(function (err) {
            console.log(err);
            return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["throwError"])(err);
          }));
        }
        /**
        * service login
        */

      }, {
        key: "loginVecom",
        value: function loginVecom(value, email) {
          var nombre = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : '';
          var celular = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : '';
          var password = arguments.length > 4 && arguments[4] !== undefined ? arguments[4] : '';
          var foto = arguments.length > 5 && arguments[5] !== undefined ? arguments[5] : '';
          this.httpOptions = {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"]({
              'Content-Type': 'application/json'
            })
          };
          var datoaEnviar = {
            "tipo_login": value,
            "email": email,
            "nombre_completo": nombre,
            "telefono_celular": celular,
            "password": password,
            "foto": foto
          };
          return this.http.post(this.path + "/user/login", datoaEnviar, this.httpOptions).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["tap"])(function (data) {
            return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["of"])(data);
          }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["catchError"])(function (err) {
            return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["throwError"])(err);
          }));
        }
        /**
        * service Registro usuario
        */

      }, {
        key: "registroVecom",
        value: function registroVecom(nombre, celular, email, password) {
          this.httpOptions = {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"]({
              'Content-Type': 'application/json'
            })
          };
          var datoaEnviar = {
            "nombre_completo": nombre,
            "email": email,
            "celular": celular,
            "password": password
          };
          return this.http.post(this.path + "/user/signup", datoaEnviar, this.httpOptions).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["tap"])(function (data) {
            return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["of"])(data);
          }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["catchError"])(function (err) {
            return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["throwError"])(err);
          }));
        }
        /**
        * service Perfil
        */

      }, {
        key: "perfilUsuario",
        value: function perfilUsuario() {
          this.accesToken = localStorage.getItem('access_token');
          this.httpOptions = {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"]({
              'Content-Type': 'application/json',
              'Authorization': 'Bearer ' + this.accesToken
            })
          };
          return this.http.post(this.path + '/user/perfil', null, this.httpOptions).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["tap"])(function (data) {
            return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["of"])(data);
          }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["catchError"])(function (err) {
            return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["throwError"])(err);
          }));
        }
        /**
        * service buscar automovil
        */

      }, {
        key: "searchVecom",
        value: function searchVecom(player_id, value, email) {
          var nombre = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : '';
          var celular = arguments.length > 4 && arguments[4] !== undefined ? arguments[4] : '';
          var password = arguments.length > 5 && arguments[5] !== undefined ? arguments[5] : '';
          var foto = arguments.length > 6 && arguments[6] !== undefined ? arguments[6] : '';
          this.httpOptions = {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"]({
              'Content-Type': 'application/json'
            })
          };
          var datoaEnviar = {
            "tipo_login": value,
            "email": email,
            "nombre_completo": nombre,
            "telefono_celular": celular,
            "password": password,
            "player_id": player_id,
            "foto": foto
          };
          return this.http.post(this.path + "/user/login", datoaEnviar, this.httpOptions).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["tap"])(function (data) {
            return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["of"])(data);
          }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["catchError"])(function (err) {
            return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["throwError"])(err);
          }));
        }
        /**
        * service Editar Usuario
        */

      }, {
        key: "editarUsuarioVecom",
        value: function editarUsuarioVecom(nombre, email, celular, fecha_nacimiento, foto) {
          this.accesToken = localStorage.getItem('access_token');
          this.httpOptions = {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"]({
              'Content-Type': 'application/json',
              'Authorization': 'Bearer ' + this.accesToken
            })
          };
          var datoaEnviar = {
            "nombre_completo": nombre,
            "email": email,
            "celular": celular,
            "fecha_nacimiento": fecha_nacimiento,
            "foto": foto
          };
          return this.http.post(this.path + "/user/usuarioedit", datoaEnviar, this.httpOptions).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["tap"])(function (data) {
            return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["of"])(data);
          }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["catchError"])(function (err) {
            return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["throwError"])(err);
          }));
        }
        /*
        / Servicio de modelos
        */

      }, {
        key: "getModelos",
        value: function getModelos(id) {
          this.accesToken = localStorage.getItem('access_token');
          this.httpOptions = {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"]({
              'Content-Type': 'application/json',
              'Authorization': 'Bearer ' + this.accesToken
            })
          };
          return this.http.get(this.path + '/vehiculo/modelos/?marca_id=' + id + '', this.httpOptions).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["tap"])(function (data) {
            return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["of"])(data);
          }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["catchError"])(function (err) {
            console.log(err);
            return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["throwError"])(err);
          }));
        }
        /*
          / Servicio de marcas
          */

      }, {
        key: "getMarcas",
        value: function getMarcas() {
          this.accesToken = localStorage.getItem('access_token');
          this.httpOptions = {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"]({
              'Content-Type': 'application/json',
              'Authorization': 'Bearer ' + this.accesToken
            })
          };
          return this.http.get(this.path + '/vehiculo/marcas', this.httpOptions).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["tap"])(function (data) {
            return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["of"])(data);
          }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["catchError"])(function (err) {
            console.log(err);
            return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["throwError"])(err);
          }));
        }
        /*
          / Servicio de tipos vehiculos
          */

      }, {
        key: "getTipoVehiculo",
        value: function getTipoVehiculo() {
          this.accesToken = localStorage.getItem('access_token');
          this.httpOptions = {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"]({
              'Content-Type': 'application/json',
              'Authorization': 'Bearer ' + this.accesToken
            })
          };
          return this.http.get(this.path + '/vehiculo/tipovehiculo', this.httpOptions).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["tap"])(function (data) {
            return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["of"])(data);
          }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["catchError"])(function (err) {
            console.log(err);
            return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["throwError"])(err);
          }));
        }
      }, {
        key: "addVehiculoVecom",
        value: function addVehiculoVecom(nombre, color, puertas, cilindrada, tipo_combustible, caracteristicas, ciudad, precio, anho, marca_id, modelo_id, tipo_vehiculo_id, user_id, foto) {
          this.accesToken = localStorage.getItem('access_token');
          this.httpOptions = {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"]({
              'Content-Type': 'application/json',
              'Authorization': 'Bearer ' + this.accesToken
            })
          };
          var datoaEnviar = {
            "nombre": nombre,
            "color": color,
            "puertas": puertas,
            "cilindrada": cilindrada,
            "tipo_combustible": tipo_combustible,
            "caracteristicas": caracteristicas,
            "ciudad": ciudad,
            "precio": precio,
            "año": anho,
            "marcas_id": marca_id,
            "empresa_id": "",
            "modelo_id": modelo_id,
            "tipo_vehiculo_id": tipo_vehiculo_id,
            "usuario_app_id": user_id,
            "datos": foto
          };
          return this.http.post(this.path + "/vehiculo/crearvehiculo", datoaEnviar, this.httpOptions).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["tap"])(function (data) {
            return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["of"])(data);
          }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["catchError"])(function (err) {
            return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["throwError"])(err);
          }));
        }
      }, {
        key: "editVehiculoVecom",
        value: function editVehiculoVecom(id, nombre, color, puertas, cilindrada, tipo_combustible, caracteristicas, ciudad, precio, anho, marca_id, modelo_id, tipo_vehiculo_id, caja, user_id) {
          this.accesToken = localStorage.getItem('access_token');
          this.httpOptions = {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"]({
              'Content-Type': 'application/json',
              'Authorization': 'Bearer ' + this.accesToken
            })
          };
          var datoaEnviar = {
            "nombre": nombre,
            "color": color,
            "puertas": puertas,
            "cilindrada": cilindrada,
            "tipo_combustible": tipo_combustible,
            "caracteristicas": caracteristicas,
            "ciudad": ciudad,
            "precio": precio,
            "anho": anho,
            "marcas_id": marca_id,
            "modelo_id": modelo_id,
            "tipo_vehiculo_id": tipo_vehiculo_id,
            "caja": caja,
            "usuario_app_id": user_id
          };
          return this.http.post(this.path + "/vehiculo/editarvehiculo?id=" + id + '', datoaEnviar, this.httpOptions).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["tap"])(function (data) {
            return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["of"])(data);
          }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["catchError"])(function (err) {
            return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["throwError"])(err);
          }));
        }
      }, {
        key: "addPhotography",
        value: function addPhotography(foto) {
          this.items.push({
            "foto": foto
          });
        }
      }, {
        key: "loadAll",
        value: function loadAll() {
          return Promise.resolve(this.items);
        }
      }, {
        key: "removeItem",
        value: function removeItem(ndx) {
          this.items.splice(ndx, 1);
        }
      }, {
        key: "deleteItem",
        value: function deleteItem() {
          this.items = [];
        }
        /*
        / Servicio de mis publicaciones
        */

      }, {
        key: "getMisPublicaciones",
        value: function getMisPublicaciones() {
          this.accesToken = localStorage.getItem('access_token');
          this.httpOptions = {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"]({
              'Content-Type': 'application/json',
              'Authorization': 'Bearer ' + this.accesToken
            })
          };
          return this.http.get(this.path + '/vehiculo/publicaciones', this.httpOptions).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["tap"])(function (data) {
            return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["of"])(data);
          }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["catchError"])(function (err) {
            console.log(err);
            return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["throwError"])(err);
          }));
        }
        /*
        / Servicio de mis favoritos
        */

      }, {
        key: "getMisFavoritos",
        value: function getMisFavoritos() {
          this.accesToken = localStorage.getItem('access_token');
          this.httpOptions = {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"]({
              'Content-Type': 'application/json',
              'Authorization': 'Bearer ' + this.accesToken
            })
          };
          return this.http.get(this.path + '/favorito/favoritos', this.httpOptions).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["tap"])(function (data) {
            return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["of"])(data);
          }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["catchError"])(function (err) {
            console.log(err);
            return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["throwError"])(err);
          }));
        }
        /*
          / Servicio detalle automovil
          */

      }, {
        key: "getDetalleAutomovil",
        value: function getDetalleAutomovil(idAutomovil) {
          this.accesToken = localStorage.getItem('access_token');
          this.httpOptions = {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"]({
              'Content-Type': 'application/json',
              'Authorization': 'Bearer ' + this.accesToken
            })
          };
          return this.http.post(this.path + '/vehiculo/detalleauto?id=' + idAutomovil + '', null, this.httpOptions).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["tap"])(function (data) {
            return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["of"])(data);
          }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["catchError"])(function (err) {
            console.log(err);
            return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["throwError"])(err);
          }));
        }
        /*
          / Servicio editar automovil
          */

      }, {
        key: "getEditarAutomovil",
        value: function getEditarAutomovil(idAutomovil) {
          this.accesToken = localStorage.getItem('access_token');
          this.httpOptions = {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"]({
              'Content-Type': 'application/json',
              'Authorization': 'Bearer ' + this.accesToken
            })
          };
          return this.http.get(this.path + '/vehiculo/edit?id=' + idAutomovil + '', this.httpOptions).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["tap"])(function (data) {
            return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["of"])(data);
          }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["catchError"])(function (err) {
            console.log(err);
            return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["throwError"])(err);
          }));
        }
        /*
          / Servicio de galleria vehiculo
          */

      }, {
        key: "galleriaVehiculo",
        value: function galleriaVehiculo(id) {
          this.accesToken = localStorage.getItem('access_token');
          this.httpOptions = {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"]({
              'Content-Type': 'application/json',
              'Authorization': 'Bearer ' + this.accesToken
            })
          };
          return this.http.get(this.path + '/vehiculo/galeria?id=' + id + '', this.httpOptions).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["tap"])(function (data) {
            return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["of"])(data);
          }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["catchError"])(function (err) {
            console.log(err);
            return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["throwError"])(err);
          }));
        }
        /*
        / Servicio de eliminar fotografia vehiculo
        */

      }, {
        key: "eliminarFotografiaVehiculo",
        value: function eliminarFotografiaVehiculo(id) {
          this.accesToken = localStorage.getItem('access_token');
          this.httpOptions = {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"]({
              'Content-Type': 'application/json',
              'Authorization': 'Bearer ' + this.accesToken
            })
          };
          return this.http.post(this.path + '/vehiculo/quitar-imagen?id=' + id + '', null, this.httpOptions).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["tap"])(function (data) {
            return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["of"])(data);
          }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["catchError"])(function (err) {
            console.log(err);
            return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["throwError"])(err);
          }));
        }
        /*
         / Servicio de añadir fotografia vehiculo
         */

      }, {
        key: "anhadirFotografiaVehiculo",
        value: function anhadirFotografiaVehiculo(idVehiculo, foto) {
          this.accesToken = localStorage.getItem('access_token');
          this.httpOptions = {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"]({
              'Content-Type': 'application/json',
              'Authorization': 'Bearer ' + this.accesToken
            })
          };
          var datoaEnviar = {
            "foto": foto
          };
          return this.http.post(this.path + '/vehiculo/agregarfoto?id=' + idVehiculo + '', datoaEnviar, this.httpOptions).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["tap"])(function (data) {
            return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["of"])(data);
          }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["catchError"])(function (err) {
            console.log(err);
            return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["throwError"])(err);
          }));
        }
        /*
        / Servicio de estadisticas vehiculo
        */

      }, {
        key: "estadisticasVehiculoVecom",
        value: function estadisticasVehiculoVecom(idAutomovil) {
          this.accesToken = localStorage.getItem('access_token');
          this.httpOptions = {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"]({
              'Content-Type': 'application/json',
              'Authorization': 'Bearer ' + this.accesToken
            })
          };
          return this.http.get(this.path + '/vehiculo/count?id=' + idAutomovil + '', this.httpOptions).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["tap"])(function (data) {
            return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["of"])(data);
          }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["catchError"])(function (err) {
            console.log(err);
            return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["throwError"])(err);
          }));
        }
        /*
          / Servicio de cambiar estado de vehiculo
          */

      }, {
        key: "estadoVehiculoVecom",
        value: function estadoVehiculoVecom(idAutomovil) {
          this.accesToken = localStorage.getItem('access_token');
          this.httpOptions = {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"]({
              'Content-Type': 'application/json',
              'Authorization': 'Bearer ' + this.accesToken
            })
          };
          return this.http.post(this.path + '/vehiculo/estado?id=' + idAutomovil + '', null, this.httpOptions).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["tap"])(function (data) {
            return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["of"])(data);
          }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["catchError"])(function (err) {
            console.log(err);
            return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["throwError"])(err);
          }));
        }
        /*
        / Servicio de lista de  vehiculos
        */

      }, {
        key: "listaVehiculoVecom",
        value: function listaVehiculoVecom(page) {
          this.page_size = 6;
          this.accesToken = localStorage.getItem('access_token');
          this.httpOptions = {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"]({
              'Content-Type': 'application/json',
              'Authorization': 'Bearer ' + this.accesToken
            })
          };
          return this.http.get(this.path + "/vehiculo/vehiculos" + "?page=" + page + "&page_size=" + this.page_size + "", this.httpOptions).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["tap"])(function (data) {
            return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["of"])(data);
          }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["catchError"])(function (err) {
            console.log(err);
            return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["throwError"])(err);
          }));
        }
        /*
        / Servicio de crear favorito
        */

      }, {
        key: "anhadirFavoritoVehiculoVecom",
        value: function anhadirFavoritoVehiculoVecom(idAutomovil) {
          this.accesToken = localStorage.getItem('access_token');
          this.httpOptions = {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"]({
              'Content-Type': 'application/json',
              'Authorization': 'Bearer ' + this.accesToken
            })
          };
          return this.http.post(this.path + '/favorito/create-favorito?id=' + idAutomovil + '', null, this.httpOptions).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["tap"])(function (data) {
            return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["of"])(data);
          }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["catchError"])(function (err) {
            console.log(err);
            return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["throwError"])(err);
          }));
        }
        /*
          / Servicio de eliminar favorito
          */

      }, {
        key: "eliminarFavoritoVehiculoVecom",
        value: function eliminarFavoritoVehiculoVecom(idAutomovil) {
          this.accesToken = localStorage.getItem('access_token');
          this.httpOptions = {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"]({
              'Content-Type': 'application/json',
              'Authorization': 'Bearer ' + this.accesToken
            })
          };
          return this.http.post(this.path + '/favorito/delete-favorito?id=' + idAutomovil + '', null, this.httpOptions).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["tap"])(function (data) {
            return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["of"])(data);
          }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["catchError"])(function (err) {
            console.log(err);
            return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["throwError"])(err);
          }));
        }
      }]);

      return ServiciosService;
    }();

    ServiciosService.ctorParameters = function () {
      return [{
        type: _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"]
      }];
    };

    ServiciosService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
      providedIn: 'root'
    }), tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"]])], ServiciosService);
    /***/
  }
}]);
//# sourceMappingURL=default~agregar-auto-agregar-auto-module~agregar-fotografia-agregar-fotografia-module~autoventa-auto~5893b857-es5.js.map