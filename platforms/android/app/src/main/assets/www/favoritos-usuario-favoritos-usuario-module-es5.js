function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["favoritos-usuario-favoritos-usuario-module"], {
  /***/
  "./node_modules/raw-loader/dist/cjs.js!./src/app/favoritos-usuario/favoritos-usuario.page.html":
  /*!*****************************************************************************************************!*\
    !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/favoritos-usuario/favoritos-usuario.page.html ***!
    \*****************************************************************************************************/

  /*! exports provided: default */

  /***/
  function node_modulesRawLoaderDistCjsJsSrcAppFavoritosUsuarioFavoritosUsuarioPageHtml(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "<app-header titulo=\"Favoritos\"></app-header>\n\n<ion-content> \n    <ion-grid>\n      <ion-row>\n        <ion-col size=\"6\" *ngFor=\"let post of arrayPosts\">\n          <ion-card class=\"box\" (click)=\"DetalleAuto()\">\n            <img src=\"/assets/search.png\" alt=\"\" />\n            <h3 ion-text-center>Auto-Venta</h3>\n          </ion-card>\n        </ion-col>\n      </ion-row>\n    </ion-grid>\n</ion-content>\n\n<app-footer></app-footer>\n";
    /***/
  },

  /***/
  "./src/app/favoritos-usuario/favoritos-usuario-routing.module.ts":
  /*!***********************************************************************!*\
    !*** ./src/app/favoritos-usuario/favoritos-usuario-routing.module.ts ***!
    \***********************************************************************/

  /*! exports provided: FavoritosUsuarioPageRoutingModule */

  /***/
  function srcAppFavoritosUsuarioFavoritosUsuarioRoutingModuleTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "FavoritosUsuarioPageRoutingModule", function () {
      return FavoritosUsuarioPageRoutingModule;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/fesm2015/router.js");
    /* harmony import */


    var _favoritos_usuario_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! ./favoritos-usuario.page */
    "./src/app/favoritos-usuario/favoritos-usuario.page.ts");

    var routes = [{
      path: '',
      component: _favoritos_usuario_page__WEBPACK_IMPORTED_MODULE_3__["FavoritosUsuarioPage"]
    }];

    var FavoritosUsuarioPageRoutingModule = function FavoritosUsuarioPageRoutingModule() {
      _classCallCheck(this, FavoritosUsuarioPageRoutingModule);
    };

    FavoritosUsuarioPageRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
      imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
      exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
    })], FavoritosUsuarioPageRoutingModule);
    /***/
  },

  /***/
  "./src/app/favoritos-usuario/favoritos-usuario.module.ts":
  /*!***************************************************************!*\
    !*** ./src/app/favoritos-usuario/favoritos-usuario.module.ts ***!
    \***************************************************************/

  /*! exports provided: FavoritosUsuarioPageModule */

  /***/
  function srcAppFavoritosUsuarioFavoritosUsuarioModuleTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "FavoritosUsuarioPageModule", function () {
      return FavoritosUsuarioPageModule;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/common */
    "./node_modules/@angular/common/fesm2015/common.js");
    /* harmony import */


    var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! @angular/forms */
    "./node_modules/@angular/forms/fesm2015/forms.js");
    /* harmony import */


    var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! @ionic/angular */
    "./node_modules/@ionic/angular/dist/fesm5.js");
    /* harmony import */


    var _favoritos_usuario_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! ./favoritos-usuario-routing.module */
    "./src/app/favoritos-usuario/favoritos-usuario-routing.module.ts");
    /* harmony import */


    var _favoritos_usuario_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! ./favoritos-usuario.page */
    "./src/app/favoritos-usuario/favoritos-usuario.page.ts");
    /* harmony import */


    var _components_components_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
    /*! ../components/components.module */
    "./src/app/components/components.module.ts");

    var FavoritosUsuarioPageModule = function FavoritosUsuarioPageModule() {
      _classCallCheck(this, FavoritosUsuarioPageModule);
    };

    FavoritosUsuarioPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
      imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"], _components_components_module__WEBPACK_IMPORTED_MODULE_7__["ComponentsModule"], _favoritos_usuario_routing_module__WEBPACK_IMPORTED_MODULE_5__["FavoritosUsuarioPageRoutingModule"]],
      declarations: [_favoritos_usuario_page__WEBPACK_IMPORTED_MODULE_6__["FavoritosUsuarioPage"]]
    })], FavoritosUsuarioPageModule);
    /***/
  },

  /***/
  "./src/app/favoritos-usuario/favoritos-usuario.page.scss":
  /*!***************************************************************!*\
    !*** ./src/app/favoritos-usuario/favoritos-usuario.page.scss ***!
    \***************************************************************/

  /*! exports provided: default */

  /***/
  function srcAppFavoritosUsuarioFavoritosUsuarioPageScss(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2Zhdm9yaXRvcy11c3VhcmlvL2Zhdm9yaXRvcy11c3VhcmlvLnBhZ2Uuc2NzcyJ9 */";
    /***/
  },

  /***/
  "./src/app/favoritos-usuario/favoritos-usuario.page.ts":
  /*!*************************************************************!*\
    !*** ./src/app/favoritos-usuario/favoritos-usuario.page.ts ***!
    \*************************************************************/

  /*! exports provided: FavoritosUsuarioPage */

  /***/
  function srcAppFavoritosUsuarioFavoritosUsuarioPageTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "FavoritosUsuarioPage", function () {
      return FavoritosUsuarioPage;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @ionic/angular */
    "./node_modules/@ionic/angular/dist/fesm5.js");
    /* harmony import */


    var _servicios_servicios_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! ../servicios/servicios.service */
    "./src/app/servicios/servicios.service.ts");

    var FavoritosUsuarioPage =
    /*#__PURE__*/
    function () {
      function FavoritosUsuarioPage(navCtrl, service, loadingController, toastController) {
        _classCallCheck(this, FavoritosUsuarioPage);

        this.navCtrl = navCtrl;
        this.service = service;
        this.loadingController = loadingController;
        this.toastController = toastController;
      }

      _createClass(FavoritosUsuarioPage, [{
        key: "ngOnInit",
        value: function ngOnInit() {
          return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0,
          /*#__PURE__*/
          regeneratorRuntime.mark(function _callee2() {
            var _this = this;

            var loading;
            return regeneratorRuntime.wrap(function _callee2$(_context2) {
              while (1) {
                switch (_context2.prev = _context2.next) {
                  case 0:
                    _context2.next = 2;
                    return this.loadingController.create({
                      message: 'Cargando...'
                    });

                  case 2:
                    loading = _context2.sent;
                    _context2.next = 5;
                    return loading.present().then(function () {
                      _this.service.getDatos().subscribe(function (data) {
                        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](_this, void 0, void 0,
                        /*#__PURE__*/
                        regeneratorRuntime.mark(function _callee() {
                          return regeneratorRuntime.wrap(function _callee$(_context) {
                            while (1) {
                              switch (_context.prev = _context.next) {
                                case 0:
                                  this.arrayPosts = data; // this.categorias = data.data;

                                  console.log(data);
                                  _context.next = 4;
                                  return loading.dismiss();

                                case 4:
                                  this.msjToast();

                                case 5:
                                case "end":
                                  return _context.stop();
                              }
                            }
                          }, _callee, this);
                        }));
                      }, function (err) {
                        console.log(err);
                        loading.dismiss();
                      });
                    });

                  case 5:
                  case "end":
                    return _context2.stop();
                }
              }
            }, _callee2, this);
          }));
        }
      }, {
        key: "msjToast",
        value: function msjToast() {
          return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0,
          /*#__PURE__*/
          regeneratorRuntime.mark(function _callee3() {
            var toast;
            return regeneratorRuntime.wrap(function _callee3$(_context3) {
              while (1) {
                switch (_context3.prev = _context3.next) {
                  case 0:
                    _context3.next = 2;
                    return this.toastController.create({
                      message: 'Favoritos Cargados.',
                      duration: 2000
                    });

                  case 2:
                    toast = _context3.sent;
                    toast.present();

                  case 4:
                  case "end":
                    return _context3.stop();
                }
              }
            }, _callee3, this);
          }));
        }
      }, {
        key: "DetalleAuto",
        value: function DetalleAuto() {
          this.navCtrl.navigateRoot('/detalle-auto');
        }
      }]);

      return FavoritosUsuarioPage;
    }();

    FavoritosUsuarioPage.ctorParameters = function () {
      return [{
        type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["NavController"]
      }, {
        type: _servicios_servicios_service__WEBPACK_IMPORTED_MODULE_3__["ServiciosService"]
      }, {
        type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["LoadingController"]
      }, {
        type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ToastController"]
      }];
    };

    FavoritosUsuarioPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
      selector: 'app-favoritos-usuario',
      template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! raw-loader!./favoritos-usuario.page.html */
      "./node_modules/raw-loader/dist/cjs.js!./src/app/favoritos-usuario/favoritos-usuario.page.html")).default,
      styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! ./favoritos-usuario.page.scss */
      "./src/app/favoritos-usuario/favoritos-usuario.page.scss")).default]
    }), tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_angular__WEBPACK_IMPORTED_MODULE_2__["NavController"], _servicios_servicios_service__WEBPACK_IMPORTED_MODULE_3__["ServiciosService"], _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["LoadingController"], _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ToastController"]])], FavoritosUsuarioPage);
    /***/
  }
}]);
//# sourceMappingURL=favoritos-usuario-favoritos-usuario-module-es5.js.map