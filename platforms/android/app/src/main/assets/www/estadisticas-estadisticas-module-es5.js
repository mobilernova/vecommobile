function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["estadisticas-estadisticas-module"], {
  /***/
  "./node_modules/raw-loader/dist/cjs.js!./src/app/estadisticas/estadisticas.page.html":
  /*!*******************************************************************************************!*\
    !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/estadisticas/estadisticas.page.html ***!
    \*******************************************************************************************/

  /*! exports provided: default */

  /***/
  function node_modulesRawLoaderDistCjsJsSrcAppEstadisticasEstadisticasPageHtml(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "<ion-header>\n  <ion-toolbar class=\"head\">\n    <ion-buttons slot=\"start\">\n      <ion-back-button color=\"light\"></ion-back-button>\n    </ion-buttons>\n    <ion-title align=\"center\" color=\"light\">Estadisticas</ion-title>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content  ion-padding>  \n    <ion-grid>\n      <ion-row >\n        <ion-col size=\"6\" *ngFor=\"let item of loadEstadisticas\">\n        <ion-card class=\"box\"  >\n          <div align=\"center\">\n              <ion-icon  name=\"{{item.icon}}\"></ion-icon>\n              <p>{{item.cantidad}}</p>\n              <h2>{{item.titulo}}</h2>\n          </div>\n          <!-- <img [src]=\"post.foto\" alt=\"\"/>        -->\n        <!-- <label for=\"\" >{{item.titulo}}</label> -->\n     \n        </ion-card> \n      </ion-col>        \n      </ion-row>\n    </ion-grid>\n    <ion-card>\n      <ion-card-content>\n        <div align=\"center\">\n            <h2>Una vez que el vehiculo sea vendido no olvide cambiar el estado del motorizado</h2>\n            <hr>\n          <ion-label>{{estado}}\n          </ion-label>\n          <ion-toggle slot=\"start\" [(ngModel)]=\"estadoAuto\" (ionFocus)=checkState($event) >\n          </ion-toggle>\n        </div>\n        <hr>\n      </ion-card-content>\n    </ion-card>\n  </ion-content>\n\n";
    /***/
  },

  /***/
  "./src/app/estadisticas/estadisticas-routing.module.ts":
  /*!*************************************************************!*\
    !*** ./src/app/estadisticas/estadisticas-routing.module.ts ***!
    \*************************************************************/

  /*! exports provided: EstadisticasPageRoutingModule */

  /***/
  function srcAppEstadisticasEstadisticasRoutingModuleTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "EstadisticasPageRoutingModule", function () {
      return EstadisticasPageRoutingModule;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/fesm2015/router.js");
    /* harmony import */


    var _estadisticas_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! ./estadisticas.page */
    "./src/app/estadisticas/estadisticas.page.ts");

    var routes = [{
      path: '',
      component: _estadisticas_page__WEBPACK_IMPORTED_MODULE_3__["EstadisticasPage"]
    }];

    var EstadisticasPageRoutingModule = function EstadisticasPageRoutingModule() {
      _classCallCheck(this, EstadisticasPageRoutingModule);
    };

    EstadisticasPageRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
      imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
      exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
    })], EstadisticasPageRoutingModule);
    /***/
  },

  /***/
  "./src/app/estadisticas/estadisticas.module.ts":
  /*!*****************************************************!*\
    !*** ./src/app/estadisticas/estadisticas.module.ts ***!
    \*****************************************************/

  /*! exports provided: EstadisticasPageModule */

  /***/
  function srcAppEstadisticasEstadisticasModuleTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "EstadisticasPageModule", function () {
      return EstadisticasPageModule;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/common */
    "./node_modules/@angular/common/fesm2015/common.js");
    /* harmony import */


    var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! @angular/forms */
    "./node_modules/@angular/forms/fesm2015/forms.js");
    /* harmony import */


    var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! @ionic/angular */
    "./node_modules/@ionic/angular/dist/fesm5.js");
    /* harmony import */


    var _estadisticas_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! ./estadisticas-routing.module */
    "./src/app/estadisticas/estadisticas-routing.module.ts");
    /* harmony import */


    var _estadisticas_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! ./estadisticas.page */
    "./src/app/estadisticas/estadisticas.page.ts");

    var EstadisticasPageModule = function EstadisticasPageModule() {
      _classCallCheck(this, EstadisticasPageModule);
    };

    EstadisticasPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
      imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"], _estadisticas_routing_module__WEBPACK_IMPORTED_MODULE_5__["EstadisticasPageRoutingModule"]],
      declarations: [_estadisticas_page__WEBPACK_IMPORTED_MODULE_6__["EstadisticasPage"]]
    })], EstadisticasPageModule);
    /***/
  },

  /***/
  "./src/app/estadisticas/estadisticas.page.scss":
  /*!*****************************************************!*\
    !*** ./src/app/estadisticas/estadisticas.page.scss ***!
    \*****************************************************/

  /*! exports provided: default */

  /***/
  function srcAppEstadisticasEstadisticasPageScss(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "ion-icon {\n  height: 100px;\n  width: 100px;\n}\n\n.box {\n  color: #f28f29;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9pYmVyL0RvY3VtZW50cy9pYmVyL3Byb2plY3RzL3ZlY29tbW9iaWxlL3NyYy9hcHAvZXN0YWRpc3RpY2FzL2VzdGFkaXN0aWNhcy5wYWdlLnNjc3MiLCJzcmMvYXBwL2VzdGFkaXN0aWNhcy9lc3RhZGlzdGljYXMucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0ksYUFBQTtFQUNBLFlBQUE7QUNDSjs7QURFQTtFQUNJLGNBQUE7QUNDSiIsImZpbGUiOiJzcmMvYXBwL2VzdGFkaXN0aWNhcy9lc3RhZGlzdGljYXMucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiaW9uLWljb257XG4gICAgaGVpZ2h0OiAxMDBweDtcbiAgICB3aWR0aDogMTAwcHg7XG59XG5cbi5ib3h7XG4gICAgY29sb3I6ICNmMjhmMjk7XG59IiwiaW9uLWljb24ge1xuICBoZWlnaHQ6IDEwMHB4O1xuICB3aWR0aDogMTAwcHg7XG59XG5cbi5ib3gge1xuICBjb2xvcjogI2YyOGYyOTtcbn0iXX0= */";
    /***/
  },

  /***/
  "./src/app/estadisticas/estadisticas.page.ts":
  /*!***************************************************!*\
    !*** ./src/app/estadisticas/estadisticas.page.ts ***!
    \***************************************************/

  /*! exports provided: EstadisticasPage */

  /***/
  function srcAppEstadisticasEstadisticasPageTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "EstadisticasPage", function () {
      return EstadisticasPage;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _servicios_servicios_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! ../servicios/servicios.service */
    "./src/app/servicios/servicios.service.ts");
    /* harmony import */


    var _ionic_angular__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! @ionic/angular */
    "./node_modules/@ionic/angular/dist/fesm5.js");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/fesm2015/router.js");

    var EstadisticasPage =
    /*#__PURE__*/
    function () {
      function EstadisticasPage(service, loadingCtrl, route, alertController, toastCtrl, nav) {
        _classCallCheck(this, EstadisticasPage);

        this.service = service;
        this.loadingCtrl = loadingCtrl;
        this.route = route;
        this.alertController = alertController;
        this.toastCtrl = toastCtrl;
        this.nav = nav;
        this.data = this.route.snapshot.paramMap.get('detalleAutoVecom');
        this.estado = this.route.snapshot.paramMap.get('estado');
      }

      _createClass(EstadisticasPage, [{
        key: "ngOnInit",
        value: function ngOnInit() {
          return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0,
          /*#__PURE__*/
          regeneratorRuntime.mark(function _callee2() {
            var _this = this;

            var loading;
            return regeneratorRuntime.wrap(function _callee2$(_context2) {
              while (1) {
                switch (_context2.prev = _context2.next) {
                  case 0:
                    if (this.estado == 'No vendido') {
                      this.estadoAuto = false;
                    } else {
                      this.estadoAuto = true;
                    }

                    _context2.next = 3;
                    return this.loadingCtrl.create({
                      message: 'Cargando...'
                    });

                  case 3:
                    loading = _context2.sent;
                    _context2.next = 6;
                    return loading.present().then(function () {
                      _this.service.estadisticasVehiculoVecom(_this.data).subscribe(function (data) {
                        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](_this, void 0, void 0,
                        /*#__PURE__*/
                        regeneratorRuntime.mark(function _callee() {
                          return regeneratorRuntime.wrap(function _callee$(_context) {
                            while (1) {
                              switch (_context.prev = _context.next) {
                                case 0:
                                  this.loadEstadisticas = data.data;
                                  console.log(this.loadEstadisticas);
                                  _context.next = 4;
                                  return loading.dismiss();

                                case 4:
                                case "end":
                                  return _context.stop();
                              }
                            }
                          }, _callee, this);
                        }));
                      }, function (err) {
                        loading.dismiss(); // this.message();
                      });
                    });

                  case 6:
                  case "end":
                    return _context2.stop();
                }
              }
            }, _callee2, this);
          }));
        }
      }, {
        key: "checkState",
        value: function checkState(e) {
          console.log(e);

          if (e.target.checked) {
            this.estadoAuto = true;
            this.presentAlertConfirm();
          } else {
            this.estadoAuto = false;
            this.presentAlertConfirm();
          }
        }
      }, {
        key: "presentAlertConfirm",
        value: function presentAlertConfirm() {
          return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0,
          /*#__PURE__*/
          regeneratorRuntime.mark(function _callee4() {
            var _this2 = this;

            var alert;
            return regeneratorRuntime.wrap(function _callee4$(_context4) {
              while (1) {
                switch (_context4.prev = _context4.next) {
                  case 0:
                    _context4.next = 2;
                    return this.alertController.create({
                      header: 'Confirmar!',
                      message: 'Desea cambiar el estado de venta del automovil?',
                      buttons: [{
                        text: 'Cancelar',
                        role: 'cancel',
                        cssClass: 'secondary',
                        handler: function handler(blah) {
                          console.log('Confirm Cancel: blah');
                        }
                      }, {
                        text: 'Si',
                        handler: function handler() {
                          _this2.service.estadoVehiculoVecom(_this2.data).subscribe(function (data) {
                            return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](_this2, void 0, void 0,
                            /*#__PURE__*/
                            regeneratorRuntime.mark(function _callee3() {
                              return regeneratorRuntime.wrap(function _callee3$(_context3) {
                                while (1) {
                                  switch (_context3.prev = _context3.next) {
                                    case 0:
                                      this.successEstado = data.data;
                                      this.succesSms(this.successEstado);
                                      this.nav.navigateRoot('/perfil-usuario');

                                    case 3:
                                    case "end":
                                      return _context3.stop();
                                  }
                                }
                              }, _callee3, this);
                            }));
                          }, function (err) {
                            console.log(err); // this.message();
                          });
                        }
                      }]
                    });

                  case 2:
                    alert = _context4.sent;
                    _context4.next = 5;
                    return alert.present();

                  case 5:
                  case "end":
                    return _context4.stop();
                }
              }
            }, _callee4, this);
          }));
        }
      }, {
        key: "succesSms",
        value: function succesSms(msm) {
          return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0,
          /*#__PURE__*/
          regeneratorRuntime.mark(function _callee5() {
            var toast;
            return regeneratorRuntime.wrap(function _callee5$(_context5) {
              while (1) {
                switch (_context5.prev = _context5.next) {
                  case 0:
                    _context5.next = 2;
                    return this.toastCtrl.create({
                      message: msm,
                      duration: 4000,
                      cssClass: 'my-custom-class-success'
                    });

                  case 2:
                    toast = _context5.sent;
                    toast.present();

                  case 4:
                  case "end":
                    return _context5.stop();
                }
              }
            }, _callee5, this);
          }));
        }
      }, {
        key: "errorSms",
        value: function errorSms(msm) {
          return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0,
          /*#__PURE__*/
          regeneratorRuntime.mark(function _callee6() {
            var toast;
            return regeneratorRuntime.wrap(function _callee6$(_context6) {
              while (1) {
                switch (_context6.prev = _context6.next) {
                  case 0:
                    _context6.next = 2;
                    return this.toastCtrl.create({
                      message: msm,
                      duration: 4000,
                      cssClass: 'my-custom-class'
                    });

                  case 2:
                    toast = _context6.sent;
                    toast.present();

                  case 4:
                  case "end":
                    return _context6.stop();
                }
              }
            }, _callee6, this);
          }));
        }
      }]);

      return EstadisticasPage;
    }();

    EstadisticasPage.ctorParameters = function () {
      return [{
        type: _servicios_servicios_service__WEBPACK_IMPORTED_MODULE_2__["ServiciosService"]
      }, {
        type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["LoadingController"]
      }, {
        type: _angular_router__WEBPACK_IMPORTED_MODULE_4__["ActivatedRoute"]
      }, {
        type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["AlertController"]
      }, {
        type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["ToastController"]
      }, {
        type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["NavController"]
      }];
    };

    EstadisticasPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
      selector: 'app-estadisticas',
      template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! raw-loader!./estadisticas.page.html */
      "./node_modules/raw-loader/dist/cjs.js!./src/app/estadisticas/estadisticas.page.html")).default,
      styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! ./estadisticas.page.scss */
      "./src/app/estadisticas/estadisticas.page.scss")).default]
    }), tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_servicios_servicios_service__WEBPACK_IMPORTED_MODULE_2__["ServiciosService"], _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["LoadingController"], _angular_router__WEBPACK_IMPORTED_MODULE_4__["ActivatedRoute"], _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["AlertController"], _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["ToastController"], _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["NavController"]])], EstadisticasPage);
    /***/
  }
}]);
//# sourceMappingURL=estadisticas-estadisticas-module-es5.js.map