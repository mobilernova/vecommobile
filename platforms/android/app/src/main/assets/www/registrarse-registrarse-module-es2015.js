(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["registrarse-registrarse-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/registrarse/registrarse.page.html":
/*!*****************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/registrarse/registrarse.page.html ***!
  \*****************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-content>\n  <ion-toolbar>\n    <ion-buttons slot=\"start\">\n      <ion-back-button defaultHref=\"inicio-sesion\"></ion-back-button>\n    </ion-buttons>\n  </ion-toolbar> \n\n\n<div clas=\"logo-vecom\" align=\"center\">\n    <img src=\"https://gravatar.com/avatar/dba6bae8c566f9d4041fb9cd9ada7741?d=identicon&f=y\">\n<h3>Ingrese sus datos personales</h3>\n</div>\n    <form [formGroup]=\"validations_form\"  (ngSubmit)=\"onSubmit(validations_form.value)\">\n        <ion-item class=\"box\">\n          <ion-label class=\"color-label\" position=\"floating\" >Nombre completo</ion-label>\n          <ion-input type=\"text\" formControlName=\"nombre\"></ion-input>\n        </ion-item>\n        <div class=\"validation-errors\">\n          <ng-container *ngFor=\"let validation of validation_messages.nombre\">\n            <div class=\"error-message\" *ngIf=\"validations_form.get('nombre').hasError(validation.type) && (validations_form.get('nombre').dirty || validations_form.get('nombre').touched)\">\n              <ion-icon name=\"information-circle-outline\"></ion-icon> {{ validation.message }}\n            </div>\n          </ng-container>\n        </div>\n        <br>\n      \n        <ion-item class=\"box\">\n            <ion-label class=\"color-label\" position=\"floating\" >Celular</ion-label>\n            <ion-input type=\"tel\" formControlName=\"telefono\"></ion-input>\n          </ion-item>\n    \n          <br>\n    \n          <ion-item class=\"box\">\n              <ion-label class=\"color-label\" position=\"floating\" >Email</ion-label>\n              <ion-input type=\"email\" formControlName=\"email\"></ion-input>\n            </ion-item>\n            <div class=\"validation-errors\">\n              <ng-container *ngFor=\"let validation of validation_messages.email\">\n                <div class=\"error-message\" *ngIf=\"validations_form.get('email').hasError(validation.type) && (validations_form.get('email').dirty || validations_form.get('email').touched)\">\n                  <ion-icon name=\"information-circle-outline\"></ion-icon> {{ validation.message }}\n                </div>\n              </ng-container>\n            </div>\n            <br>\n         <div formGroupName=\"matching_passwords\">\n          <ion-item class=\"box\">\n            <ion-label class=\"color-label\" position=\"floating\" >Password</ion-label>\n            <ion-input type=\"password\" formControlName=\"password\"></ion-input>\n          </ion-item>\n          <div class=\"validation-errors\">\n            <ng-container *ngFor=\"let validation of validation_messages.password\">\n              <div class=\"error-message\" *ngIf=\"validations_form.get('matching_passwords').get('password').hasError(validation.type) && (validations_form.get('matching_passwords').get('password').dirty || validations_form.get('matching_passwords').get('password').touched)\">\n                <ion-icon name=\"information-circle-outline\"></ion-icon> {{ validation.message }}\n              </div>\n            </ng-container>\n          </div>\n          <br>\n          <ion-item class=\"box\"> \n            <ion-label class=\"color-label\" position=\"floating\" >Confirmar contraseña</ion-label>\n            <ion-input type=\"password\" formControlName=\"confirm_password\"></ion-input>\n          </ion-item>\n          <div class=\"validation-errors\">\n            <ng-container *ngFor=\"let validation of validation_messages.confirm_password\">\n              <div class=\"error-message\" *ngIf=\"validations_form.get('matching_passwords').get('confirm_password').hasError(validation.type) && (validations_form.get('matching_passwords').get('confirm_password').dirty || validations_form.get('matching_passwords').get('confirm_password').touched)\">\n                <ion-icon name=\"information-circle-outline\"></ion-icon> {{ validation.message }}\n              </div>\n            </ng-container>\n          </div>\n        </div>\n        <div class=\"validation-errors\">\n          <ng-container *ngFor=\"let validation of validation_messages.matching_passwords\">\n            <div class=\"error-message\" *ngIf=\"validations_form.get('matching_passwords').hasError(validation.type) && (validations_form.get('matching_passwords').get('confirm_password').dirty || validations_form.get('matching_passwords').get('confirm_password').touched)\">\n              <ion-icon name=\"information-circle-outline\"></ion-icon> {{ validation.message }}\n            </div>\n          </ng-container>\n        </div>\n        <br>\n        <div class=\"button-registro\" >\n            <ion-button  class=\"btns\" expand=\"full\" type=\"submit\"><span>Enviar</span> </ion-button>\n        </div>\n        \n      </form>\n</ion-content>\n");

/***/ }),

/***/ "./src/app/registrarse/registrarse-routing.module.ts":
/*!***********************************************************!*\
  !*** ./src/app/registrarse/registrarse-routing.module.ts ***!
  \***********************************************************/
/*! exports provided: RegistrarsePageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RegistrarsePageRoutingModule", function() { return RegistrarsePageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _registrarse_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./registrarse.page */ "./src/app/registrarse/registrarse.page.ts");




const routes = [
    {
        path: '',
        component: _registrarse_page__WEBPACK_IMPORTED_MODULE_3__["RegistrarsePage"]
    }
];
let RegistrarsePageRoutingModule = class RegistrarsePageRoutingModule {
};
RegistrarsePageRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], RegistrarsePageRoutingModule);



/***/ }),

/***/ "./src/app/registrarse/registrarse.module.ts":
/*!***************************************************!*\
  !*** ./src/app/registrarse/registrarse.module.ts ***!
  \***************************************************/
/*! exports provided: RegistrarsePageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RegistrarsePageModule", function() { return RegistrarsePageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _registrarse_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./registrarse-routing.module */ "./src/app/registrarse/registrarse-routing.module.ts");
/* harmony import */ var _registrarse_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./registrarse.page */ "./src/app/registrarse/registrarse.page.ts");







let RegistrarsePageModule = class RegistrarsePageModule {
};
RegistrarsePageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["ReactiveFormsModule"],
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _registrarse_routing_module__WEBPACK_IMPORTED_MODULE_5__["RegistrarsePageRoutingModule"]
        ],
        declarations: [_registrarse_page__WEBPACK_IMPORTED_MODULE_6__["RegistrarsePage"]]
    })
], RegistrarsePageModule);



/***/ }),

/***/ "./src/app/registrarse/registrarse.page.scss":
/*!***************************************************!*\
  !*** ./src/app/registrarse/registrarse.page.scss ***!
  \***************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("img {\n  height: 120px;\n  width: 120px;\n  border-radius: 50%;\n  border: solid 2px #505050;\n  display: inline;\n  box-shadow: 0 0 28px rgba(255, 255, 255, 0.65);\n  margin-top: 20px;\n}\n\n.logo-vecom {\n  padding-top: 10%;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9pYmVyL0RvY3VtZW50cy9pYmVyL3Byb2plY3RzL3ZlY29tbW9iaWxlL3NyYy9hcHAvcmVnaXN0cmFyc2UvcmVnaXN0cmFyc2UucGFnZS5zY3NzIiwic3JjL2FwcC9yZWdpc3RyYXJzZS9yZWdpc3RyYXJzZS5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFFSSxhQUFBO0VBQ0EsWUFBQTtFQUNBLGtCQUFBO0VBQ0EseUJBQUE7RUFDQSxlQUFBO0VBQ0EsOENBQUE7RUFDQSxnQkFBQTtBQ0FKOztBREdBO0VBQ0UsZ0JBQUE7QUNBRiIsImZpbGUiOiJzcmMvYXBwL3JlZ2lzdHJhcnNlL3JlZ2lzdHJhcnNlLnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbImltZyB7XHJcbiAgICBcclxuICAgIGhlaWdodDogMTIwcHg7XHJcbiAgICB3aWR0aDogMTIwcHg7XHJcbiAgICBib3JkZXItcmFkaXVzOiA1MCU7XHJcbiAgICBib3JkZXI6IHNvbGlkIDJweCAjNTA1MDUwO1xyXG4gICAgZGlzcGxheTogaW5saW5lO1xyXG4gICAgYm94LXNoYWRvdzogMCAwIDI4cHggcmdiYSgyNTUsMjU1LDI1NSwgLjY1KTsgICAgXHJcbiAgICBtYXJnaW4tdG9wOiAyMHB4OyAgXHJcbiAgfVxyXG5cclxuLmxvZ28tdmVjb217XHJcbiAgcGFkZGluZy10b3A6IDEwJTtcclxufVxyXG4iLCJpbWcge1xuICBoZWlnaHQ6IDEyMHB4O1xuICB3aWR0aDogMTIwcHg7XG4gIGJvcmRlci1yYWRpdXM6IDUwJTtcbiAgYm9yZGVyOiBzb2xpZCAycHggIzUwNTA1MDtcbiAgZGlzcGxheTogaW5saW5lO1xuICBib3gtc2hhZG93OiAwIDAgMjhweCByZ2JhKDI1NSwgMjU1LCAyNTUsIDAuNjUpO1xuICBtYXJnaW4tdG9wOiAyMHB4O1xufVxuXG4ubG9nby12ZWNvbSB7XG4gIHBhZGRpbmctdG9wOiAxMCU7XG59Il19 */");

/***/ }),

/***/ "./src/app/registrarse/registrarse.page.ts":
/*!*************************************************!*\
  !*** ./src/app/registrarse/registrarse.page.ts ***!
  \*************************************************/
/*! exports provided: RegistrarsePage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RegistrarsePage", function() { return RegistrarsePage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _validators_password_validators__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./../../../validators/password.validators */ "./validators/password.validators.ts");
/* harmony import */ var _validators_username_validators__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./../../../validators/username.validators */ "./validators/username.validators.ts");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _servicios_servicios_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../servicios/servicios.service */ "./src/app/servicios/servicios.service.ts");








let RegistrarsePage = class RegistrarsePage {
    constructor(formBuilder, service, navCtrl, menuCtrl, toastCtrl, alertCtrl, loadingCtrl, route) {
        this.formBuilder = formBuilder;
        this.service = service;
        this.navCtrl = navCtrl;
        this.menuCtrl = menuCtrl;
        this.toastCtrl = toastCtrl;
        this.alertCtrl = alertCtrl;
        this.loadingCtrl = loadingCtrl;
        this.route = route;
        this.validation_messages = {
            'username': [
                { type: 'required', message: 'Username es requerido.' },
                { type: 'minlength', message: 'El nombre de usuario debe tener al menos 5 caracteres.' },
                { type: 'maxlength', message: 'El nombre de usuario no puede tener más de 25 caracteres.' },
                { type: 'pattern', message: 'Tu nombre de usuario debe contener solo números y letras.' },
            ],
            'nombre': [
                { type: 'required', message: 'Nombre es requerido.' },
                { type: 'int', message: "numero campo" }
            ],
            'email': [
                { type: 'required', message: 'Email es requerido.' },
                { type: 'pattern', message: 'Por favor introduzca una dirección de correo electrónico válida.' }
            ],
            'password': [
                { type: 'required', message: 'Password es requerido.' },
                { type: 'minlength', message: 'La contraseña debe tener al menos 5 caracteres.' },
                { type: 'pattern', message: 'Su contraseña debe contener al menos una mayúscula, una minúscula y un número.' }
            ],
            'confirm_password': [
                { type: 'required', message: 'Confirmar password es requerido.' }
            ],
            'matching_passwords': [
                { type: 'areEqual', message: 'Contraseña no coincide.' }
            ],
        };
    }
    ngOnInit() {
        this.matching_passwords_group = new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormGroup"]({
            password: new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormControl"]('', _angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].compose([
                _angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].minLength(5),
                _angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].required,
                _angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].pattern('^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])[a-zA-Z0-9]+$')
            ])),
            confirm_password: new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormControl"]('', _angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].required)
        }, (formGroup) => {
            return _validators_password_validators__WEBPACK_IMPORTED_MODULE_1__["PasswordValidator"].areEqual(formGroup);
        });
        this.validations_form = this.formBuilder.group({
            username: new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormControl"]('', _angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].compose([
                _validators_username_validators__WEBPACK_IMPORTED_MODULE_2__["UsernameValidator"].validUsername,
                _angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].maxLength(25),
                _angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].minLength(5),
                _angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].pattern('^(?=.*[a-zA-Z])(?=.*[0-9])[a-zA-Z0-9]+$'),
                _angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].required
            ])),
            nombre: new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormControl"]('', _angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].required),
            ciudad: new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormControl"]('', _angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].required),
            telefono: new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormControl"]('', _angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].required),
            // lastname: new FormControl('', Validators.required),
            email: new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormControl"]('', _angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].compose([
                _angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].required,
                _angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].pattern('^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+$')
            ])),
            matching_passwords: this.matching_passwords_group,
        });
    }
    onSubmit(d) {
        console.log(d);
        // this.player_id = localStorage.getItem("player-id");
        this.service.registroVecom(d.nombre, d.telefono, d.email, d.matching_passwords['password']).subscribe((data) => tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            this.data = data.data;
            console.log(this.data);
            localStorage.setItem('access_token', this.data.access_token);
            localStorage.setItem('idUsuario', this.data.id);
            localStorage.setItem('celular', this.data.telefono_celular);
            this.confirmation(this.data.nombre_completo);
            this.navCtrl.navigateRoot('/home');
        }), err => {
            console.log(err);
            if (err.status == '0') {
                this.errorConexion();
            }
            else {
                this.presentToast(err.error.data.message);
            }
        });
    }
    confirmation(name) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            const toast = yield this.toastCtrl.create({
                message: 'Felicidades ' + name + ' usted es usuario de VECOM.',
                cssClass: "my-custom-class-success",
            });
            // this.welcome(nombre);
            yield toast.present();
        });
    }
    errorConexion() {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            const toast = yield this.toastCtrl.create({
                message: "Error de conexión",
                duration: 4000,
                cssClass: "my-custom-class-alert"
            });
            toast.present();
        });
    }
    presentToast(msm) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            const toast = yield this.toastCtrl.create({
                message: msm,
                duration: 4000,
                cssClass: "my-custom-class"
            });
            toast.present();
        });
    }
};
RegistrarsePage.ctorParameters = () => [
    { type: _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormBuilder"] },
    { type: _servicios_servicios_service__WEBPACK_IMPORTED_MODULE_7__["ServiciosService"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_6__["NavController"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_6__["MenuController"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_6__["ToastController"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_6__["AlertController"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_6__["LoadingController"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_5__["ActivatedRoute"] }
];
RegistrarsePage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_3__["Component"])({
        selector: 'app-registrarse',
        template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./registrarse.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/registrarse/registrarse.page.html")).default,
        styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./registrarse.page.scss */ "./src/app/registrarse/registrarse.page.scss")).default]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormBuilder"],
        _servicios_servicios_service__WEBPACK_IMPORTED_MODULE_7__["ServiciosService"],
        _ionic_angular__WEBPACK_IMPORTED_MODULE_6__["NavController"],
        _ionic_angular__WEBPACK_IMPORTED_MODULE_6__["MenuController"],
        _ionic_angular__WEBPACK_IMPORTED_MODULE_6__["ToastController"],
        _ionic_angular__WEBPACK_IMPORTED_MODULE_6__["AlertController"],
        _ionic_angular__WEBPACK_IMPORTED_MODULE_6__["LoadingController"],
        _angular_router__WEBPACK_IMPORTED_MODULE_5__["ActivatedRoute"]])
], RegistrarsePage);



/***/ }),

/***/ "./validators/password.validators.ts":
/*!*******************************************!*\
  !*** ./validators/password.validators.ts ***!
  \*******************************************/
/*! exports provided: PasswordValidator */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PasswordValidator", function() { return PasswordValidator; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");

class PasswordValidator {
    // Inspired on: http://plnkr.co/edit/Zcbg2T3tOxYmhxs7vaAm?p=preview
    static areEqual(formGroup) {
        let val;
        let valid = true;
        for (let key in formGroup.controls) {
            if (formGroup.controls.hasOwnProperty(key)) {
                let control = formGroup.controls[key];
                if (val === undefined) {
                    val = control.value;
                }
                else {
                    if (val !== control.value) {
                        valid = false;
                        break;
                    }
                }
            }
        }
        if (valid) {
            return null;
        }
        return {
            areEqual: true
        };
    }
}


/***/ }),

/***/ "./validators/username.validators.ts":
/*!*******************************************!*\
  !*** ./validators/username.validators.ts ***!
  \*******************************************/
/*! exports provided: UsernameValidator */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UsernameValidator", function() { return UsernameValidator; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");

class UsernameValidator {
    static validUsername(fc) {
        if (fc.value.toLowerCase() === "abc123" || fc.value.toLowerCase() === "123abc") {
            return {
                validUsername: true
            };
        }
        else {
            return null;
        }
    }
}


/***/ })

}]);
//# sourceMappingURL=registrarse-registrarse-module-es2015.js.map