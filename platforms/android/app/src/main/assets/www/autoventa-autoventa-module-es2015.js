(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["autoventa-autoventa-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/autoventa/autoventa.page.html":
/*!*************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/autoventa/autoventa.page.html ***!
  \*************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<app-header titulo= \"AutoVenta\"></app-header>\n\n<ion-content class=\"ion-padding\">\n  <ion-list>\n      <ion-item *ngFor=\"let item of arrayPosts\" (click)=\"detallePublicacion(item)\">\n        <ion-thumbnail>\n          <img [src]=\"item.foto ? item.foto: imgNotCar\" />\n        </ion-thumbnail>\n          <!-- <ion-img [src]=\"item.imagenes_autos[0]\" (click)=\"detalles()\" style=\"width: 100px; height: 90px;\"></ion-img> -->\n          <ion-label class=\"datos\">\n            <h3>{{ item.nombre }}</h3>\n            <p>{{ item.ciudad }}</p>\n            <ion-button class=\"head\" >{{ item.precio }} $</ion-button>\n           \n                     <!-- <ion-icon name=\"heart-half\" class=\"ion-float-end\" style=\"zoom:2.0;\"></ion-icon> -->\n          </ion-label>\n          <div align=\"right\">\n            <label >Ver</label> \n           </div>\n        </ion-item>\n  </ion-list>\n  <ion-infinite-scroll threshold=\"100px\" (ionInfinite)=\"loadData($event)\">\n      <ion-infinite-scroll-content\n        loadingSpinner=\"crescent\"\n        loadingText=\"Cargando mas...\">\n      </ion-infinite-scroll-content>\n    </ion-infinite-scroll>\n</ion-content>\n\n<app-footer></app-footer>");

/***/ }),

/***/ "./src/app/autoventa/autoventa-routing.module.ts":
/*!*******************************************************!*\
  !*** ./src/app/autoventa/autoventa-routing.module.ts ***!
  \*******************************************************/
/*! exports provided: AutoventaPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AutoventaPageRoutingModule", function() { return AutoventaPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _autoventa_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./autoventa.page */ "./src/app/autoventa/autoventa.page.ts");




const routes = [
    {
        path: '',
        component: _autoventa_page__WEBPACK_IMPORTED_MODULE_3__["AutoventaPage"]
    }
];
let AutoventaPageRoutingModule = class AutoventaPageRoutingModule {
};
AutoventaPageRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], AutoventaPageRoutingModule);



/***/ }),

/***/ "./src/app/autoventa/autoventa.module.ts":
/*!***********************************************!*\
  !*** ./src/app/autoventa/autoventa.module.ts ***!
  \***********************************************/
/*! exports provided: AutoventaPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AutoventaPageModule", function() { return AutoventaPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _autoventa_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./autoventa-routing.module */ "./src/app/autoventa/autoventa-routing.module.ts");
/* harmony import */ var _autoventa_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./autoventa.page */ "./src/app/autoventa/autoventa.page.ts");
/* harmony import */ var _components_components_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../components/components.module */ "./src/app/components/components.module.ts");








let AutoventaPageModule = class AutoventaPageModule {
};
AutoventaPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _components_components_module__WEBPACK_IMPORTED_MODULE_7__["ComponentsModule"],
            _autoventa_routing_module__WEBPACK_IMPORTED_MODULE_5__["AutoventaPageRoutingModule"]
        ],
        declarations: [_autoventa_page__WEBPACK_IMPORTED_MODULE_6__["AutoventaPage"]]
    })
], AutoventaPageModule);



/***/ }),

/***/ "./src/app/autoventa/autoventa.page.scss":
/*!***********************************************!*\
  !*** ./src/app/autoventa/autoventa.page.scss ***!
  \***********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".datos {\n  margin-left: 15px;\n  margin-right: 15px;\n}\n\nion-thumbnail {\n  width: 150px;\n  height: 100px;\n}\n\n.head {\n  --background: #f28f29;\n}\n\napp-footer {\n  height: 60px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9pYmVyL0RvY3VtZW50cy9pYmVyL3Byb2plY3RzL3ZlY29tbW9iaWxlL3NyYy9hcHAvYXV0b3ZlbnRhL2F1dG92ZW50YS5wYWdlLnNjc3MiLCJzcmMvYXBwL2F1dG92ZW50YS9hdXRvdmVudGEucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0ksaUJBQUE7RUFDQSxrQkFBQTtBQ0NKOztBRENBO0VBQ0ksWUFBQTtFQUNBLGFBQUE7QUNFSjs7QURBQTtFQUNJLHFCQUFBO0FDR0o7O0FEREE7RUFDSSxZQUFBO0FDSUoiLCJmaWxlIjoic3JjL2FwcC9hdXRvdmVudGEvYXV0b3ZlbnRhLnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi5kYXRvc3sgICAgXHJcbiAgICBtYXJnaW4tbGVmdDogMTVweDsgXHJcbiAgICBtYXJnaW4tcmlnaHQ6IDE1cHg7XHJcbn1cclxuaW9uLXRodW1ibmFpbHtcclxuICAgIHdpZHRoOiAxNTBweDtcclxuICAgIGhlaWdodDogMTAwcHg7XHJcbn1cclxuLmhlYWR7XHJcbiAgICAtLWJhY2tncm91bmQ6ICNmMjhmMjk7XHJcbn1cclxuYXBwLWZvb3RlcntcclxuICAgIGhlaWdodDogNjBweDtcclxuICB9XHJcblxyXG4iLCIuZGF0b3Mge1xuICBtYXJnaW4tbGVmdDogMTVweDtcbiAgbWFyZ2luLXJpZ2h0OiAxNXB4O1xufVxuXG5pb24tdGh1bWJuYWlsIHtcbiAgd2lkdGg6IDE1MHB4O1xuICBoZWlnaHQ6IDEwMHB4O1xufVxuXG4uaGVhZCB7XG4gIC0tYmFja2dyb3VuZDogI2YyOGYyOTtcbn1cblxuYXBwLWZvb3RlciB7XG4gIGhlaWdodDogNjBweDtcbn0iXX0= */");

/***/ }),

/***/ "./src/app/autoventa/autoventa.page.ts":
/*!*********************************************!*\
  !*** ./src/app/autoventa/autoventa.page.ts ***!
  \*********************************************/
/*! exports provided: AutoventaPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AutoventaPage", function() { return AutoventaPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _servicios_servicios_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../servicios/servicios.service */ "./src/app/servicios/servicios.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _servicios_publicidad_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../servicios/publicidad.service */ "./src/app/servicios/publicidad.service.ts");






let AutoventaPage = class AutoventaPage {
    constructor(navCtrl, loadingController, toastController, service, service2, router) {
        this.navCtrl = navCtrl;
        this.loadingController = loadingController;
        this.toastController = toastController;
        this.service = service;
        this.service2 = service2;
        this.router = router;
        this.imgNotCar = 'assets/images/notfound.jpg';
    }
    ionViewWillEnter() {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            this.autito();
            this.page = 1;
            const loading = yield this.loadingController.create({
                message: 'Cargando lista VECOM',
            });
            yield loading.present().then(() => {
                this.service.listaVehiculoVecom(this.page).subscribe((data) => tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
                    this.arrayPosts = data.data;
                    // this.categorias = data.data;
                    console.log(this.arrayPosts);
                    yield loading.dismiss();
                    this.msjToast();
                }), err => {
                    console.log(err);
                    loading.dismiss();
                });
            });
        });
    }
    loadData(event) {
        setTimeout(() => {
            this.page++;
            this.service.listaVehiculoVecom(this.page).subscribe(data => {
                data = data.data;
                console.log(data);
                let list = data;
                if (list.length > 0) {
                    for (let item of list) {
                        this.arrayPosts.push(item);
                    }
                    event.target.complete();
                }
                else {
                    event.target.disabled = true;
                    event.target.complete();
                }
            }, err => {
                this.message();
            });
        }, 500);
    }
    detallePublicacion(c) {
        const idVehiculo = c.id;
        this.router.navigate(['detalle-auto', { detalleAutoVecom: idVehiculo }]);
    }
    message() {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            const loading = yield this.loadingController.create({
                message: 'Error de Conexion',
            });
            yield loading.present();
            const { role, data } = yield loading.onDidDismiss();
        });
    }
    msjToast() {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            const toast = yield this.toastController.create({
                message: 'Datos mostrados.',
                duration: 2000
            });
            toast.present();
        });
    }
    autito() {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            const loading = yield this.loadingController.create({
                message: 'Cargando...',
            });
            yield loading.present().then(() => {
                this.service2.listaPublicidadesVecom().subscribe((data) => tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
                    this.arrayPosts = data;
                    // this.categorias = data.data;
                    console.log(this.arrayPosts);
                    yield loading.dismiss();
                    // this.msjToast();
                }), err => {
                    console.log(err);
                    loading.dismiss();
                });
            });
        });
    }
};
AutoventaPage.ctorParameters = () => [
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["NavController"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["LoadingController"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ToastController"] },
    { type: _servicios_servicios_service__WEBPACK_IMPORTED_MODULE_3__["ServiciosService"] },
    { type: _servicios_publicidad_service__WEBPACK_IMPORTED_MODULE_5__["PublicidadService"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"] }
];
AutoventaPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-autoventa',
        template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./autoventa.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/autoventa/autoventa.page.html")).default,
        styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./autoventa.page.scss */ "./src/app/autoventa/autoventa.page.scss")).default]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_angular__WEBPACK_IMPORTED_MODULE_2__["NavController"],
        _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["LoadingController"],
        _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ToastController"],
        _servicios_servicios_service__WEBPACK_IMPORTED_MODULE_3__["ServiciosService"],
        _servicios_publicidad_service__WEBPACK_IMPORTED_MODULE_5__["PublicidadService"],
        _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"]])
], AutoventaPage);



/***/ })

}]);
//# sourceMappingURL=autoventa-autoventa-module-es2015.js.map