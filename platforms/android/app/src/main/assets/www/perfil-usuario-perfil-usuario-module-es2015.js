(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["perfil-usuario-perfil-usuario-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/perfil-usuario/perfil-usuario.page.html":
/*!***********************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/perfil-usuario/perfil-usuario.page.html ***!
  \***********************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<!-- <app-header titulo= \"Mi Perfil\"></app-header> -->\n<ion-header align=\"center\">\n    <ion-toolbar class=\"head\">\n      <ion-buttons slot=\"start\">\n        <ion-menu-button  style=\"color: #ffffff;\"></ion-menu-button>\n      </ion-buttons>\n      <ion-title class=\"title\">Mi Perfil</ion-title>\n      <ion-buttons slot=\"end\" >\n        <ion-button (click)=\"crearPublicacion()\">\n          <ion-icon color=\"light\" name=\"add\"></ion-icon>\n        </ion-button>\n      </ion-buttons>\n    </ion-toolbar>\n  </ion-header>\n\n<ion-content>\n  <div>\n    <ion-row>\n      <ion-col size=\"6\">\n          <img class=\"avatar\"\n          [src]=\"data.foto ? data.foto : img\">\n      </ion-col>\n      <ion-col size=\"6\">\n        <div class=\"button-perfil\">\n            <ion-button class=\"btns\"\n            (click)=\"EditarPerfil()\">\n            Editar Perfil</ion-button>\n            <ion-button class=\"btns\"\n            (click)=\"logout()\">\n            Cerrar sesión</ion-button>\n        </div>\n      </ion-col>\n    </ion-row>\n  </div>\n\n  <div>\n    <ion-item class=\"box\">\n      <ion-label>\n      <h3>Nombre: {{data.nombre_completo}}</h3>\n      <p>E-mail: {{data.email}}</p>\n      <p>Celular: {{data.celular}}</p>\n        <!-- <p>Cuidad</p> -->\n      </ion-label>\n    </ion-item>\n  </div>\n  <br>\n\n\n  <ion-segment [(ngModel)]=\"categoria\" (ionChange)=\"itemSave($event)\">\n    <ion-segment-button value=\"publicaciones\">\n      <ion-label >Publicaciones</ion-label>\n      <ion-icon name=\"cart\"></ion-icon>\n    </ion-segment-button>\n    <ion-segment-button value=\"favoritos\">\n      <ion-label >Favoritos</ion-label>\n      <ion-icon name=\"heart\"></ion-icon>\n    </ion-segment-button>\n  </ion-segment>\n  <div [ngSwitch]=\"categoria\">\n    <ion-list *ngSwitchCase=\"'publicaciones'\" #lista>\n      <ion-item *ngIf=\"!misPublicaciones\" >\n        <ion-label>Bienvenidos a publicaciones</ion-label>\n      </ion-item>\n        <ion-item-sliding *ngFor=\"let item of misPublicaciones\">\n        <ion-item (click)=\"detallePublicacion(item)\">\n          <ion-thumbnail slot=\"start\">\n            <img [src]=\"item.foto ? item.foto : imgNotCar\">\n          </ion-thumbnail>\n          <ion-label>\n            <h3>{{item.nombre}}</h3>\n          <p>{{item.precio}}$</p>\n          <p>{{item.estado}}</p>\n          \n          </ion-label>\n          <div align=\"right\">\n            <label >Deslizar</label> \n           </div>\n        </ion-item>\n        <ion-item-options side=\"end\" expandable >\n            <ion-item-option color=\"success\" expandable (click)=\"editarVehiculo(item.id)\">\n                <ion-icon  slot=\"top\" name=\"create\"></ion-icon>\n                    <p>Editar</p>\n              </ion-item-option>\n              <ion-item-option color=\"danger\" expandable (click)=\"estadisticasVehiculo(item.id, item.estado)\">\n                  <!-- <ion-icon slot=\"top\"  name=\"<ion-icon name=\"stats-chart-outline\"></ion-icon>\"></ion-icon> -->\n                  <ion-icon  slot=\"top\" name=\"trending-up\"></ion-icon>\n\n                      <p >Estadisticas</p>\n              </ion-item-option>\n              <ion-item-option color=\"tertiary\" expandable (click)=\"galleriaAutomovil(item.id)\">\n                <ion-icon  slot=\"top\" name=\"images\"></ion-icon>\n                <p>Fotos</p>\n              </ion-item-option>\n          </ion-item-options>\n          </ion-item-sliding>\n    </ion-list>\n\n    <ion-list *ngSwitchCase=\"'favoritos'\">\n        <ion-item *ngIf=\"!misFavoritos\" >\n            <ion-label>Bienvenidos a Favoritos</ion-label>\n          </ion-item>\n          <ion-item-sliding  *ngFor=\"let item of misFavoritos\">\n\n            <ion-item (click)=\"detallePublicacion(item)\">\n              <ion-thumbnail slot=\"start\">\n                <img [src]=\"item.foto ? item.foto : imgNotCar\">\n              </ion-thumbnail>\n              <ion-label>\n                <h3>{{item.nombre}}</h3>\n              <p>{{item.precio}}$</p>\n              <p>{{item.estado}}</p>\n               \n              </ion-label>\n              <div align=\"right\">\n                <label >Ver</label> \n               </div>\n            </ion-item>\n            <ion-item-options side=\"end\" expandable >\n\n            <ion-item-option color=\"danger\" expandable (click)=\"deleteFavorito(item.idfavorito)\">\n                <ion-icon  slot=\"top\" name=\"trash\"></ion-icon>\n                <p>Quitar</p>\n              </ion-item-option>\n            </ion-item-options>\n          </ion-item-sliding>\n    </ion-list>\n  </div>\n</ion-content>\n<!-- <app-footer></app-footer> -->");

/***/ }),

/***/ "./src/app/perfil-usuario/perfil-usuario-routing.module.ts":
/*!*****************************************************************!*\
  !*** ./src/app/perfil-usuario/perfil-usuario-routing.module.ts ***!
  \*****************************************************************/
/*! exports provided: PerfilUsuarioPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PerfilUsuarioPageRoutingModule", function() { return PerfilUsuarioPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _perfil_usuario_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./perfil-usuario.page */ "./src/app/perfil-usuario/perfil-usuario.page.ts");




const routes = [
    {
        path: '',
        component: _perfil_usuario_page__WEBPACK_IMPORTED_MODULE_3__["PerfilUsuarioPage"]
    }
];
let PerfilUsuarioPageRoutingModule = class PerfilUsuarioPageRoutingModule {
};
PerfilUsuarioPageRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], PerfilUsuarioPageRoutingModule);



/***/ }),

/***/ "./src/app/perfil-usuario/perfil-usuario.module.ts":
/*!*********************************************************!*\
  !*** ./src/app/perfil-usuario/perfil-usuario.module.ts ***!
  \*********************************************************/
/*! exports provided: PerfilUsuarioPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PerfilUsuarioPageModule", function() { return PerfilUsuarioPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _perfil_usuario_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./perfil-usuario-routing.module */ "./src/app/perfil-usuario/perfil-usuario-routing.module.ts");
/* harmony import */ var _perfil_usuario_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./perfil-usuario.page */ "./src/app/perfil-usuario/perfil-usuario.page.ts");
/* harmony import */ var _components_components_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../components/components.module */ "./src/app/components/components.module.ts");








let PerfilUsuarioPageModule = class PerfilUsuarioPageModule {
};
PerfilUsuarioPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _components_components_module__WEBPACK_IMPORTED_MODULE_7__["ComponentsModule"],
            _perfil_usuario_routing_module__WEBPACK_IMPORTED_MODULE_5__["PerfilUsuarioPageRoutingModule"]
        ],
        declarations: [_perfil_usuario_page__WEBPACK_IMPORTED_MODULE_6__["PerfilUsuarioPage"]]
    })
], PerfilUsuarioPageModule);



/***/ }),

/***/ "./src/app/perfil-usuario/perfil-usuario.page.scss":
/*!*********************************************************!*\
  !*** ./src/app/perfil-usuario/perfil-usuario.page.scss ***!
  \*********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".avatar {\n  height: 150px;\n  width: 150px;\n  border-radius: 50%;\n  border: solid 3px #505050;\n  display: inline;\n  box-shadow: 0 0 28px rgba(255, 255, 255, 0.65);\n  margin-top: 5px;\n  margin-left: 15px;\n}\n\n.icoc {\n  zoom: 1.5;\n  color: white;\n}\n\n.button-perfil {\n  position: absolute;\n  bottom: 20px;\n}\n\nion-segment-button {\n  --background-checked:#572263;\n  --color-checked: white;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9pYmVyL0RvY3VtZW50cy9pYmVyL3Byb2plY3RzL3ZlY29tbW9iaWxlL3NyYy9hcHAvcGVyZmlsLXVzdWFyaW8vcGVyZmlsLXVzdWFyaW8ucGFnZS5zY3NzIiwic3JjL2FwcC9wZXJmaWwtdXN1YXJpby9wZXJmaWwtdXN1YXJpby5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDSSxhQUFBO0VBQ0EsWUFBQTtFQUNBLGtCQUFBO0VBQ0EseUJBQUE7RUFDQSxlQUFBO0VBQ0EsOENBQUE7RUFDQSxlQUFBO0VBQ0EsaUJBQUE7QUNDSjs7QURDRTtFQUNFLFNBQUE7RUFDQSxZQUFBO0FDRUo7O0FEQUU7RUFDRSxrQkFBQTtFQUNBLFlBQUE7QUNHSjs7QURBRTtFQUNFLDRCQUFBO0VBR0Esc0JBQUE7QUNDSiIsImZpbGUiOiJzcmMvYXBwL3BlcmZpbC11c3VhcmlvL3BlcmZpbC11c3VhcmlvLnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi5hdmF0YXIgeyAgICBcclxuICAgIGhlaWdodDogMTUwcHg7XHJcbiAgICB3aWR0aDogMTUwcHg7XHJcbiAgICBib3JkZXItcmFkaXVzOiA1MCU7XHJcbiAgICBib3JkZXI6IHNvbGlkIDNweCAjNTA1MDUwO1xyXG4gICAgZGlzcGxheTogaW5saW5lO1xyXG4gICAgYm94LXNoYWRvdzogMCAwIDI4cHggcmdiYSgyNTUsMjU1LDI1NSwgLjY1KTsgICAgXHJcbiAgICBtYXJnaW4tdG9wOiA1cHg7XHJcbiAgICBtYXJnaW4tbGVmdDogMTVweDtcclxuICB9XHJcbiAgLmljb2N7XHJcbiAgICB6b29tOiAxLjU7XHJcbiAgICBjb2xvcjogd2hpdGU7XHJcbiAgfVxyXG4gIC5idXR0b24tcGVyZmlse1xyXG4gICAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gICAgYm90dG9tOiAyMHB4O1xyXG4gIH1cclxuXHJcbiAgaW9uLXNlZ21lbnQtYnV0dG9ue1xyXG4gICAgLS1iYWNrZ3JvdW5kLWNoZWNrZWQ6IzU3MjI2MztcclxuICAgIC8vIC0tYmFja2dyb3VuZDogIzU3MjI2MztcclxuXHJcbiAgICAtLWNvbG9yLWNoZWNrZWQ6IHdoaXRlO1xyXG4gIH1cclxuICAgICAgICAgXHJcblxyXG5cclxuIiwiLmF2YXRhciB7XG4gIGhlaWdodDogMTUwcHg7XG4gIHdpZHRoOiAxNTBweDtcbiAgYm9yZGVyLXJhZGl1czogNTAlO1xuICBib3JkZXI6IHNvbGlkIDNweCAjNTA1MDUwO1xuICBkaXNwbGF5OiBpbmxpbmU7XG4gIGJveC1zaGFkb3c6IDAgMCAyOHB4IHJnYmEoMjU1LCAyNTUsIDI1NSwgMC42NSk7XG4gIG1hcmdpbi10b3A6IDVweDtcbiAgbWFyZ2luLWxlZnQ6IDE1cHg7XG59XG5cbi5pY29jIHtcbiAgem9vbTogMS41O1xuICBjb2xvcjogd2hpdGU7XG59XG5cbi5idXR0b24tcGVyZmlsIHtcbiAgcG9zaXRpb246IGFic29sdXRlO1xuICBib3R0b206IDIwcHg7XG59XG5cbmlvbi1zZWdtZW50LWJ1dHRvbiB7XG4gIC0tYmFja2dyb3VuZC1jaGVja2VkOiM1NzIyNjM7XG4gIC0tY29sb3ItY2hlY2tlZDogd2hpdGU7XG59Il19 */");

/***/ }),

/***/ "./src/app/perfil-usuario/perfil-usuario.page.ts":
/*!*******************************************************!*\
  !*** ./src/app/perfil-usuario/perfil-usuario.page.ts ***!
  \*******************************************************/
/*! exports provided: PerfilUsuarioPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PerfilUsuarioPage", function() { return PerfilUsuarioPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _servicios_servicios_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../servicios/servicios.service */ "./src/app/servicios/servicios.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");





let PerfilUsuarioPage = class PerfilUsuarioPage {
    constructor(navCtrl, service, loadingCtrl, toastController, router) {
        this.navCtrl = navCtrl;
        this.service = service;
        this.loadingCtrl = loadingCtrl;
        this.toastController = toastController;
        this.router = router;
        this.img = 'assets/images/perfil.jpg';
        this.imgNotCar = 'assets/images/notfound.jpg';
        this.data = { nombre_completo: "", email: '', celular: "", foto: "" };
    }
    ionViewWillEnter() {
        this.loadPerfilUsuario();
        this.categoria = "publicaciones";
        this.loadMisPublicaciones();
    }
    itemSave(e) {
        console.log(e);
        if (e.detail.value == "publicaciones") {
            this.loadMisPublicaciones();
        }
        else {
            this.loadMisFavoritos();
        }
    }
    loadPerfilUsuario() {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            // const loading = await this.loadingCtrl.create({
            //   message: 'Cargando...',
            // });
            // await loading.present().then(()=>{
            this.service.perfilUsuario().subscribe((data) => tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
                this.data = data.data;
                console.log(this.data);
                // await loading.dismiss();
            }), err => {
                console.log(err);
                //  loading.dismiss();
            });
            // });
        });
    }
    loadMisPublicaciones() {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            const loading = yield this.loadingCtrl.create({
                message: 'Cargando mis publicaciones',
            });
            yield loading.present().then(() => {
                this.service.getMisPublicaciones().subscribe((data) => tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
                    this.misPublicaciones = data.data;
                    console.log(this.misPublicaciones);
                    yield loading.dismiss();
                }), err => {
                    console.log(err);
                    loading.dismiss();
                });
            });
        });
    }
    loadMisFavoritos() {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            const loading = yield this.loadingCtrl.create({
                message: 'Cargando mis favoritos',
            });
            yield loading.present().then(() => {
                this.service.getMisFavoritos().subscribe((data) => tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
                    this.misFavoritos = data.data;
                    console.log(this.misFavoritos);
                    yield loading.dismiss();
                }), err => {
                    console.log(err);
                    loading.dismiss();
                });
            });
        });
    }
    detallePublicacion(c) {
        const idVehiculo = c.id;
        this.router.navigate(['detalle-auto', { detalleAutoVecom: idVehiculo }]);
    }
    // public Publicaciones() {
    //   this.navCtrl.navigateRoot('/publicaciones-usuario');
    // }
    // public Favoritos() {
    //   this.navCtrl.navigateRoot('/favoritos-usuario');
    // }
    EditarPerfil() {
        this.router.navigate(['/editar-perfil']);
    }
    logout() {
        localStorage.clear();
        this.navCtrl.navigateRoot('/inicio-sesion');
    }
    crearPublicacion() {
        this.router.navigate(['/agregar-auto']);
    }
    editarVehiculo(item) {
        const idVehiculo = item;
        this.lista.closeSlidingItems();
        this.router.navigate(['editar-vehiculo', { detalleAutoVecom: idVehiculo }]);
    }
    galleriaAutomovil(item) {
        localStorage.setItem("preventIdAutomovil", item);
        this.lista.closeSlidingItems();
        this.navCtrl.navigateRoot(['galleria-automovil']);
    }
    estadisticasVehiculo(item, estado) {
        const idVehiculo = item;
        const estadoVehiculo = estado;
        this.lista.closeSlidingItems();
        this.router.navigate(['estadisticas', { detalleAutoVecom: idVehiculo, estado: estadoVehiculo }]);
    }
    deleteFavorito(id) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            this.service.eliminarFavoritoVehiculoVecom(id).subscribe((data) => tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
                this.success = data.data;
                this.succesSms(this.success);
                console.log(this.misFavoritos);
            }), err => {
                console.log(err);
                this.errorSms(err.error.data.message);
            });
        });
    }
    succesSms(msm) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            const toast = yield this.toastController.create({
                message: msm,
                duration: 4000,
                cssClass: 'my-custom-class-success',
            });
            toast.present();
            this.loadMisFavoritos();
        });
    }
    errorSms(msm) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            const toast = yield this.toastController.create({
                message: msm,
                duration: 4000,
                cssClass: 'my-custom-class',
            });
            toast.present();
        });
    }
};
PerfilUsuarioPage.ctorParameters = () => [
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["NavController"] },
    { type: _servicios_servicios_service__WEBPACK_IMPORTED_MODULE_3__["ServiciosService"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["LoadingController"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ToastController"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"] }
];
tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])('lista', { static: false }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
], PerfilUsuarioPage.prototype, "lista", void 0);
PerfilUsuarioPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-perfil-usuario',
        template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./perfil-usuario.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/perfil-usuario/perfil-usuario.page.html")).default,
        styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./perfil-usuario.page.scss */ "./src/app/perfil-usuario/perfil-usuario.page.scss")).default]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_angular__WEBPACK_IMPORTED_MODULE_2__["NavController"],
        _servicios_servicios_service__WEBPACK_IMPORTED_MODULE_3__["ServiciosService"],
        _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["LoadingController"],
        _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ToastController"],
        _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"]])
], PerfilUsuarioPage);



/***/ })

}]);
//# sourceMappingURL=perfil-usuario-perfil-usuario-module-es2015.js.map