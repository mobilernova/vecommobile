(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["agregar-auto-agregar-auto-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/agregar-auto/agregar-auto.page.html":
/*!*******************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/agregar-auto/agregar-auto.page.html ***!
  \*******************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-header>\n  <ion-toolbar class=\"head\">\n    <!-- <ion-buttons slot=\"start\">\n      <ion-menu-button  style=\"color: #ffffff;\"></ion-menu-button>\n    </ion-buttons> -->\n    <ion-buttons slot=\"start\">\n      <ion-back-button color=\"light\"></ion-back-button>\n    </ion-buttons>\n    <ion-title class=\"title\">\n      Agregar Vehiculo\n    </ion-title>\n  </ion-toolbar> \n</ion-header>\n<ion-content>\n \n\n  <div align=\"center\">\n    <h3>Información basica</h3>\n    <ion-item class=\"box\"> \n      <ion-label>Nombre:</ion-label>\n      <ion-input [(ngModel)]=\"nombre\" placeholder=\"Nissan pathfinder\"></ion-input>  \n  </ion-item>\n  <br>\n\t<ion-item class=\"box\">\n\t  <ion-label class=\"fw500\">Ciudad</ion-label>\n\t  <ion-select [(ngModel)]=\"ciudad\" multiple=\"false\">\n\t    <ion-select-option>La Paz</ion-select-option>\n\t    <ion-select-option>Santa Cruz</ion-select-option>\n\t    <ion-select-option>Cochabamba</ion-select-option>\n\t    <ion-select-option>Potosi</ion-select-option>\n\t    <ion-select-option>Oruro</ion-select-option>\n\t    <ion-select-option>Beni</ion-select-option>\n\t    <ion-select-option>Pando</ion-select-option>\n\t    <ion-select-option>Tarija</ion-select-option>\n\t    <ion-select-option>Sucre</ion-select-option>\n\t  </ion-select>\n\t</ion-item>\n<br>\n<ion-item class=\"box\">\n  <ion-label class=\"fw500\">Tipo de Vehiculo</ion-label>\n  <ion-select [(ngModel)]=\"tipo_vehiculo\" multiple=\"false\">\n    <ion-select-option *ngFor=\"let item of tipoVehiculo\" [value]=\"item.id\">{{item.nombre}}</ion-select-option>\n  </ion-select>\n</ion-item>\n<br>\n<ion-item class=\"box\" >\n  <ion-label class=\"fw500\">Marca</ion-label>\n  <ion-select  [(ngModel)]=\"marca\"  (ionChange)=\"callModelos($event)\">\n  <ion-select-option *ngFor=\"let item of marcas\"  value=\"{{item.id}}\">{{item.nombre}}</ion-select-option>\n  </ion-select>\n</ion-item>\n<br>\n<ion-item class=\"box\">\n  <ion-label class=\"fw500\" >Modelo</ion-label>\n  <ion-select [(ngModel)]=\"modelo\" (ionChange)=\"verifyMarca($event)\">\n  <ion-select-option *ngFor=\"let item of modelos\" [value]=\"item.id\" >{{item.nombre}}</ion-select-option>                \n  </ion-select>\n</ion-item> \n<br>  \n<div>\n<ion-item class=\"box\">\n  <ion-label class=\"fw500\">Color</ion-label>\n  <input type=\"text\" [(ngModel)]=\"color\" multiple=\"false\">\n</ion-item>\n</div>\n<br> \n<div>\n<ion-item class=\"box\"> \n  <ion-label class=\"fw500\">Cilindrada:</ion-label>\n  <ion-input type=\"text\" [(ngModel)]=\"cilindrada\"></ion-input>  \n</ion-item>\n</div>\n<br>\n<ion-item class=\"box\">\n  <ion-label class=\"fw500\">Combustible</ion-label>\n  <ion-select [(ngModel)]=\"combustible\" multiple=\"false\">\n    <ion-select-option value=\"gasolina\">Gasolina</ion-select-option>\n    <ion-select-option value=\"gnv\">GNV</ion-select-option>\n    <ion-select-option value=\"diesel\">Diesel</ion-select-option>   \n    <ion-select-option value=\"diesel/gnv\">Diesel/GNV</ion-select-option>                  \n    <ion-select-option value=\"gasolina/gnv\">Gasolina/GNV</ion-select-option>                  \n\n  </ion-select>\n</ion-item>\n<br>\n<ion-item class=\"box\">\n  <ion-label class=\"fw500\">Caja</ion-label>\n  <ion-select [(ngModel)]=\"caja\" multiple=\"false\">\n    <ion-select-option value=\"automatica\">Automatica</ion-select-option>\n    <ion-select-option value=\"mecanica\">Mecanica</ion-select-option>\n    <ion-select-option value=\"secuencial\">Secuencial</ion-select-option>                  \n  </ion-select>\n</ion-item>\n<br>\n\n<ion-item class=\"box\">\n  <ion-label class=\"fw500\">Año</ion-label>\n  <!-- <ion-datetime [(ngModel)]=\"anho\" placeholder=\"YYYY\"\n                display-format=\"YYYY\"\n                min=\"1950\"\n                max=\"2025\"\n                value=\"2021\" \n                displayFormat=\"YYYY\" pickerFormat=\"YYYY\" >\n                \n  </ion-datetime> -->\n  <ion-input type=\"number\" [(ngModel)]=\"anho\"></ion-input>\n</ion-item> \n<br>\n       \n  <div>\n  <ion-item class=\"box\"> \n      <ion-label class=\"fw500\">Precio:</ion-label>\n      <ion-input type=\"number\" [(ngModel)]=\"precio\"></ion-input>  \n  </ion-item>\n  </div>\n  <br>\n  <div>\n    <ion-item class=\"box\"> \n        <ion-label class=\"fw500\">Numero de puertas:</ion-label>\n        <ion-input type=\"text\" [(ngModel)]=\"puertas\"></ion-input>  \n    </ion-item>\n    </div>\n    <br>\n  <div>\n    <ion-item class=\"box\"> \n      <textarea [(ngModel)]=\"caracteristicas\" name=\"\" id=\"\" cols=\"40\" rows=\"10\" placeholder=\"Caracteristicas\"></textarea>\n      </ion-item>\n    </div>\n    <br>  \n\n</div>\n<div align=\"center\">\n  <!-- <div>\n  <img src=\"https://gravatar.com/avatar/dba6bae8c566f9d4041fb9cd9ada7741?d=identicon&f=y\">\n</div>\n  <ion-icon class=\"icod\" name=\"camera\"></ion-icon> -->\n  <ion-button  class=\"button-publicacion\" expand=\"block\" fill=\"outline\" (click)=\"takephoto()\">\n    <ion-icon slot=\"start\" name=\"camera\"></ion-icon>\n    Tomar fotografia</ion-button>\n    <ion-button  class=\"button-publicacion\"  expand=\"block\" fill=\"outline\" (click)=\"getimage()\">\n      <ion-icon slot=\"start\" name=\"add\"></ion-icon>\n      Seleccionar fotografia</ion-button>\n     <ion-row>\n       <ion-col size=\"4\" *ngFor=\"let item of fotografias; index as key\" >\n         <img [src]=\"item.foto\" alt=\"\">\n         <div align=\"center\">\n          <ion-button icon-left color=\"danger\" (click)=\"removeItem(key)\">\n              <ion-icon name=\"close\"></ion-icon> \n          </ion-button>\n          </div>\n       </ion-col>\n     </ion-row>\n</div>\n<div id=\"buttons-link\" align=\"center\">\n  <ion-button expand=\"block\" class=\"btns\" (click)=\"addVehiculoVecom()\">\n    Guardar\n  </ion-button>\n</div>\n</ion-content>\n\n\n");

/***/ }),

/***/ "./src/app/agregar-auto/agregar-auto-routing.module.ts":
/*!*************************************************************!*\
  !*** ./src/app/agregar-auto/agregar-auto-routing.module.ts ***!
  \*************************************************************/
/*! exports provided: AgregarAutoPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AgregarAutoPageRoutingModule", function() { return AgregarAutoPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _agregar_auto_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./agregar-auto.page */ "./src/app/agregar-auto/agregar-auto.page.ts");




const routes = [
    {
        path: '',
        component: _agregar_auto_page__WEBPACK_IMPORTED_MODULE_3__["AgregarAutoPage"]
    }
];
let AgregarAutoPageRoutingModule = class AgregarAutoPageRoutingModule {
};
AgregarAutoPageRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], AgregarAutoPageRoutingModule);



/***/ }),

/***/ "./src/app/agregar-auto/agregar-auto.module.ts":
/*!*****************************************************!*\
  !*** ./src/app/agregar-auto/agregar-auto.module.ts ***!
  \*****************************************************/
/*! exports provided: AgregarAutoPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AgregarAutoPageModule", function() { return AgregarAutoPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _agregar_auto_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./agregar-auto-routing.module */ "./src/app/agregar-auto/agregar-auto-routing.module.ts");
/* harmony import */ var _agregar_auto_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./agregar-auto.page */ "./src/app/agregar-auto/agregar-auto.page.ts");







let AgregarAutoPageModule = class AgregarAutoPageModule {
};
AgregarAutoPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _agregar_auto_routing_module__WEBPACK_IMPORTED_MODULE_5__["AgregarAutoPageRoutingModule"]
        ],
        declarations: [_agregar_auto_page__WEBPACK_IMPORTED_MODULE_6__["AgregarAutoPage"]]
    })
], AgregarAutoPageModule);



/***/ }),

/***/ "./src/app/agregar-auto/agregar-auto.page.scss":
/*!*****************************************************!*\
  !*** ./src/app/agregar-auto/agregar-auto.page.scss ***!
  \*****************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("img {\n  height: 200px;\n  width: 600px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9pYmVyL0RvY3VtZW50cy9pYmVyL3Byb2plY3RzL3ZlY29tbW9iaWxlL3NyYy9hcHAvYWdyZWdhci1hdXRvL2FncmVnYXItYXV0by5wYWdlLnNjc3MiLCJzcmMvYXBwL2FncmVnYXItYXV0by9hZ3JlZ2FyLWF1dG8ucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUNBO0VBQ0ksYUFBQTtFQUNBLFlBQUE7QUNBSiIsImZpbGUiOiJzcmMvYXBwL2FncmVnYXItYXV0by9hZ3JlZ2FyLWF1dG8ucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiICBcclxuaW1ne1xyXG4gICAgaGVpZ2h0OiAyMDBweDtcclxuICAgIHdpZHRoOiA2MDBweDtcclxufSIsImltZyB7XG4gIGhlaWdodDogMjAwcHg7XG4gIHdpZHRoOiA2MDBweDtcbn0iXX0= */");

/***/ }),

/***/ "./src/app/agregar-auto/agregar-auto.page.ts":
/*!***************************************************!*\
  !*** ./src/app/agregar-auto/agregar-auto.page.ts ***!
  \***************************************************/
/*! exports provided: AgregarAutoPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AgregarAutoPage", function() { return AgregarAutoPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _ionic_native_camera_ngx__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ionic-native/camera/ngx */ "./node_modules/@ionic-native/camera/ngx/index.js");
/* harmony import */ var _servicios_servicios_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../servicios/servicios.service */ "./src/app/servicios/servicios.service.ts");





let AgregarAutoPage = class AgregarAutoPage {
    constructor(navCtrl, camera, service, loadingCtrl, toastCtrl) {
        this.navCtrl = navCtrl;
        this.camera = camera;
        this.service = service;
        this.loadingCtrl = loadingCtrl;
        this.toastCtrl = toastCtrl;
    }
    ngOnInit() {
        this.loadMarcas();
        this.loadTiposVehiculo();
    }
    loadTiposVehiculo() {
        this.service.getTipoVehiculo().subscribe((data) => tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            this.tipoVehiculo = data.data;
            console.log(this.tipoVehiculo);
        }), err => {
        });
    }
    verifyMarca(e) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            console.log(e);
            if (!e.detail.value) {
                const toast = yield this.toastCtrl.create({
                    message: "Seleccionar Marca",
                    duration: 2000,
                    cssClass: 'my-custom-class',
                });
                toast.present();
            }
        });
    }
    callModelos(e) {
        if (e.detail.value) {
            console.log("entre aqui a marcas on select");
            this.loadModelos(this.marca);
        }
    }
    loadMarcas() {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            const loading = yield this.loadingCtrl.create({
                message: 'Cargando...',
            });
            yield loading.present().then(() => {
                this.service.getMarcas().subscribe((data) => tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
                    this.marcas = data.data;
                    console.log(this.marcas);
                    yield loading.dismiss();
                }), err => {
                    console.log(err);
                    loading.dismiss();
                });
            });
        });
    }
    loadModelos(id) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            const loading = yield this.loadingCtrl.create({
                message: 'Cargando modelos',
            });
            yield loading.present().then(() => {
                this.service.getModelos(id).subscribe((data) => tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
                    this.modelos = data.data;
                    console.log(this.modelos);
                    yield loading.dismiss();
                }), err => {
                    console.log(err);
                    loading.dismiss();
                });
            });
        });
    }
    guardar() {
        this.navCtrl.navigateRoot('/perfil-usuario');
    }
    takephoto() {
        const options = {
            quality: 100,
            destinationType: this.camera.DestinationType.DATA_URL,
            encodingType: this.camera.EncodingType.JPEG,
            mediaType: this.camera.MediaType.PICTURE,
            correctOrientation: true,
            allowEdit: true,
        };
        this.camera.getPicture(options).then((imageData) => {
            this.imgOficial = "data:image/jpeg;base64," + imageData;
            this.service.addPhotography(this.imgOficial);
            this.loadFotografias();
        }, (err) => {
        });
    }
    getimage() {
        const options = {
            quality: 100,
            destinationType: this.camera.DestinationType.DATA_URL,
            sourceType: this.camera.PictureSourceType.PHOTOLIBRARY,
            saveToPhotoAlbum: false,
            correctOrientation: true,
            allowEdit: true,
        };
        this.camera.getPicture(options).then((imageData) => {
            this.imgOficial = "data:image/jpeg;base64," + imageData;
            this.service.addPhotography(this.imgOficial);
            this.loadFotografias();
        }, (err) => {
        });
    }
    loadFotografias() {
        this.service.loadAll().then(result => {
            this.fotografias = result;
        });
    }
    removeItem(ndx) {
        this.service.removeItem(ndx);
    }
    addVehiculoVecom() {
        this.userId = localStorage.getItem('idUsuario');
        this.service.addVehiculoVecom(this.nombre, this.color, this.puertas, this.cilindrada, this.combustible, this.caracteristicas, this.ciudad, this.precio, this.anho, this.marca, this.modelo, this.tipo_vehiculo, this.userId, this.fotografias).subscribe(data => {
            this.success = data.data.data;
            console.log(this.success);
            this.succesSms(this.success);
            this.navCtrl.navigateRoot('/perfil-usuario');
        }, err => {
            console.log(err);
            if (err.status == 422) {
                this.errorSms(err.error.data.data);
            }
            else {
                this.errorConexion();
            }
        });
    }
    errorConexion() {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            const toast = yield this.toastCtrl.create({
                message: "error de conexion",
                duration: 3000,
                cssClass: 'my-custom-class',
            });
            toast.present();
        });
    }
    succesSms(msm) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            const toast = yield this.toastCtrl.create({
                message: msm,
                duration: 4000,
                cssClass: 'my-custom-class-success',
            });
            toast.present();
        });
    }
    errorSms(msm) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            const toast = yield this.toastCtrl.create({
                message: msm,
                duration: 4000,
                cssClass: 'my-custom-class',
            });
            toast.present();
        });
    }
};
AgregarAutoPage.ctorParameters = () => [
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["NavController"] },
    { type: _ionic_native_camera_ngx__WEBPACK_IMPORTED_MODULE_3__["Camera"] },
    { type: _servicios_servicios_service__WEBPACK_IMPORTED_MODULE_4__["ServiciosService"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["LoadingController"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ToastController"] }
];
AgregarAutoPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-agregar-auto',
        template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./agregar-auto.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/agregar-auto/agregar-auto.page.html")).default,
        styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./agregar-auto.page.scss */ "./src/app/agregar-auto/agregar-auto.page.scss")).default]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_angular__WEBPACK_IMPORTED_MODULE_2__["NavController"],
        _ionic_native_camera_ngx__WEBPACK_IMPORTED_MODULE_3__["Camera"],
        _servicios_servicios_service__WEBPACK_IMPORTED_MODULE_4__["ServiciosService"],
        _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["LoadingController"],
        _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ToastController"]])
], AgregarAutoPage);



/***/ })

}]);
//# sourceMappingURL=agregar-auto-agregar-auto-module-es2015.js.map