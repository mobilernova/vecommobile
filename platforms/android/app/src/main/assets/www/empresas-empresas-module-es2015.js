(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["empresas-empresas-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/empresas/empresas.page.html":
/*!***********************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/empresas/empresas.page.html ***!
  \***********************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<app-header titulo= \"Empresas\"></app-header>\n\n\n  <ion-content  ion-padding>  \n    <ion-grid>\n      <ion-row >\n        <ion-col size=\"6\" *ngFor=\"let item of arrayPosts\">\n        <ion-card class=\"box\" (click)=\"DetalleEmpresa(item)\" >\n          <img [src]=\"item.foto\" alt=\"\"/>       \n          \n        </ion-card> \n      </ion-col>        \n      </ion-row>\n    </ion-grid>\n  </ion-content>\n\n<app-footer></app-footer>");

/***/ }),

/***/ "./src/app/empresas/empresas-routing.module.ts":
/*!*****************************************************!*\
  !*** ./src/app/empresas/empresas-routing.module.ts ***!
  \*****************************************************/
/*! exports provided: EmpresasPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EmpresasPageRoutingModule", function() { return EmpresasPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _empresas_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./empresas.page */ "./src/app/empresas/empresas.page.ts");




const routes = [
    {
        path: '',
        component: _empresas_page__WEBPACK_IMPORTED_MODULE_3__["EmpresasPage"]
    }
];
let EmpresasPageRoutingModule = class EmpresasPageRoutingModule {
};
EmpresasPageRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], EmpresasPageRoutingModule);



/***/ }),

/***/ "./src/app/empresas/empresas.module.ts":
/*!*********************************************!*\
  !*** ./src/app/empresas/empresas.module.ts ***!
  \*********************************************/
/*! exports provided: EmpresasPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EmpresasPageModule", function() { return EmpresasPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _empresas_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./empresas-routing.module */ "./src/app/empresas/empresas-routing.module.ts");
/* harmony import */ var _empresas_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./empresas.page */ "./src/app/empresas/empresas.page.ts");
/* harmony import */ var _components_components_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../components/components.module */ "./src/app/components/components.module.ts");








let EmpresasPageModule = class EmpresasPageModule {
};
EmpresasPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _components_components_module__WEBPACK_IMPORTED_MODULE_7__["ComponentsModule"],
            _empresas_routing_module__WEBPACK_IMPORTED_MODULE_5__["EmpresasPageRoutingModule"]
        ],
        declarations: [_empresas_page__WEBPACK_IMPORTED_MODULE_6__["EmpresasPage"]]
    })
], EmpresasPageModule);



/***/ }),

/***/ "./src/app/empresas/empresas.page.scss":
/*!*********************************************!*\
  !*** ./src/app/empresas/empresas.page.scss ***!
  \*********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("img {\n  height: 180px;\n}\n\napp-footer {\n  height: 60px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9pYmVyL0RvY3VtZW50cy9pYmVyL3Byb2plY3RzL3ZlY29tbW9iaWxlL3NyYy9hcHAvZW1wcmVzYXMvZW1wcmVzYXMucGFnZS5zY3NzIiwic3JjL2FwcC9lbXByZXNhcy9lbXByZXNhcy5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDSSxhQUFBO0FDQ0o7O0FEQ0E7RUFDSSxZQUFBO0FDRUoiLCJmaWxlIjoic3JjL2FwcC9lbXByZXNhcy9lbXByZXNhcy5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyJpbWd7XG4gICAgaGVpZ2h0OiAxODBweDtcbn1cbmFwcC1mb290ZXJ7XG4gICAgaGVpZ2h0OiA2MHB4O1xuICB9IiwiaW1nIHtcbiAgaGVpZ2h0OiAxODBweDtcbn1cblxuYXBwLWZvb3RlciB7XG4gIGhlaWdodDogNjBweDtcbn0iXX0= */");

/***/ }),

/***/ "./src/app/empresas/empresas.page.ts":
/*!*******************************************!*\
  !*** ./src/app/empresas/empresas.page.ts ***!
  \*******************************************/
/*! exports provided: EmpresasPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EmpresasPage", function() { return EmpresasPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _servicios_empresas_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../servicios/empresas.service */ "./src/app/servicios/empresas.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");





let EmpresasPage = class EmpresasPage {
    constructor(navCtrl, service, loadingController, toastController, router) {
        this.navCtrl = navCtrl;
        this.service = service;
        this.loadingController = loadingController;
        this.toastController = toastController;
        this.router = router;
    }
    ngOnInit() {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            this.page = 1;
            const loading = yield this.loadingController.create({
                message: 'Cargando...',
            });
            yield loading.present().then(() => {
                this.service.listaEmpresasVecom(this.page).subscribe((data) => tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
                    this.arrayPosts = data.data;
                    // this.categorias = data.data;
                    console.log(this.arrayPosts);
                    yield loading.dismiss();
                    this.msjToast();
                }), err => {
                    console.log(err);
                    loading.dismiss();
                });
            });
        });
    }
    msjToast() {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            const toast = yield this.toastController.create({
                message: 'Empresas Cargadas',
                duration: 2000
            });
            toast.present();
        });
    }
    DetalleEmpresa(item) {
        const idEmpresa = item.id;
        this.router.navigate(['/detalle-empresa', { item: idEmpresa }]);
    }
};
EmpresasPage.ctorParameters = () => [
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["NavController"] },
    { type: _servicios_empresas_service__WEBPACK_IMPORTED_MODULE_3__["EmpresasService"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["LoadingController"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ToastController"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"] }
];
EmpresasPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-empresas',
        template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./empresas.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/empresas/empresas.page.html")).default,
        styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./empresas.page.scss */ "./src/app/empresas/empresas.page.scss")).default]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_angular__WEBPACK_IMPORTED_MODULE_2__["NavController"],
        _servicios_empresas_service__WEBPACK_IMPORTED_MODULE_3__["EmpresasService"],
        _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["LoadingController"],
        _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ToastController"],
        _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"]])
], EmpresasPage);



/***/ })

}]);
//# sourceMappingURL=empresas-empresas-module-es2015.js.map