function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["agregar-auto-agregar-auto-module"], {
  /***/
  "./node_modules/raw-loader/dist/cjs.js!./src/app/agregar-auto/agregar-auto.page.html":
  /*!*******************************************************************************************!*\
    !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/agregar-auto/agregar-auto.page.html ***!
    \*******************************************************************************************/

  /*! exports provided: default */

  /***/
  function node_modulesRawLoaderDistCjsJsSrcAppAgregarAutoAgregarAutoPageHtml(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "<ion-header>\n  <ion-toolbar class=\"head\">\n    <!-- <ion-buttons slot=\"start\">\n      <ion-menu-button  style=\"color: #ffffff;\"></ion-menu-button>\n    </ion-buttons> -->\n    <ion-buttons slot=\"start\">\n      <ion-back-button color=\"light\"></ion-back-button>\n    </ion-buttons>\n    <ion-title class=\"title\">\n      Agregar Vehiculo\n    </ion-title>\n  </ion-toolbar> \n</ion-header>\n<ion-content>\n \n\n  <div align=\"center\">\n    <h3>Información basica</h3>\n    <ion-item class=\"box\"> \n      <ion-label>Nombre:</ion-label>\n      <ion-input [(ngModel)]=\"nombre\" placeholder=\"Nissan pathfinder\"></ion-input>  \n  </ion-item>\n  <br>\n\t<ion-item class=\"box\">\n\t  <ion-label class=\"fw500\">Ciudad</ion-label>\n\t  <ion-select [(ngModel)]=\"ciudad\" multiple=\"false\">\n\t    <ion-select-option>La Paz</ion-select-option>\n\t    <ion-select-option>Santa Cruz</ion-select-option>\n\t    <ion-select-option>Cochabamba</ion-select-option>\n\t    <ion-select-option>Potosi</ion-select-option>\n\t    <ion-select-option>Oruro</ion-select-option>\n\t    <ion-select-option>Beni</ion-select-option>\n\t    <ion-select-option>Pando</ion-select-option>\n\t    <ion-select-option>Tarija</ion-select-option>\n\t    <ion-select-option>Sucre</ion-select-option>\n\t  </ion-select>\n\t</ion-item>\n<br>\n<ion-item class=\"box\">\n  <ion-label class=\"fw500\">Tipo de Vehiculo</ion-label>\n  <ion-select [(ngModel)]=\"tipo_vehiculo\" multiple=\"false\">\n    <ion-select-option *ngFor=\"let item of tipoVehiculo\" [value]=\"item.id\">{{item.nombre}}</ion-select-option>\n  </ion-select>\n</ion-item>\n<br>\n<ion-item class=\"box\" >\n  <ion-label class=\"fw500\">Marca</ion-label>\n  <ion-select  [(ngModel)]=\"marca\"  (ionChange)=\"callModelos($event)\">\n  <ion-select-option *ngFor=\"let item of marcas\"  value=\"{{item.id}}\">{{item.nombre}}</ion-select-option>\n  </ion-select>\n</ion-item>\n<br>\n<ion-item class=\"box\">\n  <ion-label class=\"fw500\" >Modelo</ion-label>\n  <ion-select [(ngModel)]=\"modelo\" (ionChange)=\"verifyMarca($event)\">\n  <ion-select-option *ngFor=\"let item of modelos\" [value]=\"item.id\" >{{item.nombre}}</ion-select-option>                \n  </ion-select>\n</ion-item> \n<br>  \n<div>\n<ion-item class=\"box\">\n  <ion-label class=\"fw500\">Color</ion-label>\n  <input type=\"text\" [(ngModel)]=\"color\" multiple=\"false\">\n</ion-item>\n</div>\n<br> \n<div>\n<ion-item class=\"box\"> \n  <ion-label class=\"fw500\">Cilindrada:</ion-label>\n  <ion-input type=\"text\" [(ngModel)]=\"cilindrada\"></ion-input>  \n</ion-item>\n</div>\n<br>\n<ion-item class=\"box\">\n  <ion-label class=\"fw500\">Combustible</ion-label>\n  <ion-select [(ngModel)]=\"combustible\" multiple=\"false\">\n    <ion-select-option value=\"gasolina\">Gasolina</ion-select-option>\n    <ion-select-option value=\"gnv\">GNV</ion-select-option>\n    <ion-select-option value=\"diesel\">Diesel</ion-select-option>   \n    <ion-select-option value=\"diesel/gnv\">Diesel/GNV</ion-select-option>                  \n    <ion-select-option value=\"gasolina/gnv\">Gasolina/GNV</ion-select-option>                  \n\n  </ion-select>\n</ion-item>\n<br>\n<ion-item class=\"box\">\n  <ion-label class=\"fw500\">Caja</ion-label>\n  <ion-select [(ngModel)]=\"caja\" multiple=\"false\">\n    <ion-select-option value=\"automatica\">Automatica</ion-select-option>\n    <ion-select-option value=\"mecanica\">Mecanica</ion-select-option>\n    <ion-select-option value=\"secuencial\">Secuencial</ion-select-option>                  \n  </ion-select>\n</ion-item>\n<br>\n\n<ion-item class=\"box\">\n  <ion-label class=\"fw500\">Año</ion-label>\n  <!-- <ion-datetime [(ngModel)]=\"anho\" placeholder=\"YYYY\"\n                display-format=\"YYYY\"\n                min=\"1950\"\n                max=\"2025\"\n                value=\"2021\" \n                displayFormat=\"YYYY\" pickerFormat=\"YYYY\" >\n                \n  </ion-datetime> -->\n  <ion-input type=\"number\" [(ngModel)]=\"anho\"></ion-input>\n</ion-item> \n<br>\n       \n  <div>\n  <ion-item class=\"box\"> \n      <ion-label class=\"fw500\">Precio:</ion-label>\n      <ion-input type=\"number\" [(ngModel)]=\"precio\"></ion-input>  \n  </ion-item>\n  </div>\n  <br>\n  <div>\n    <ion-item class=\"box\"> \n        <ion-label class=\"fw500\">Numero de puertas:</ion-label>\n        <ion-input type=\"text\" [(ngModel)]=\"puertas\"></ion-input>  \n    </ion-item>\n    </div>\n    <br>\n  <div>\n    <ion-item class=\"box\"> \n      <textarea [(ngModel)]=\"caracteristicas\" name=\"\" id=\"\" cols=\"40\" rows=\"10\" placeholder=\"Caracteristicas\"></textarea>\n      </ion-item>\n    </div>\n    <br>  \n\n</div>\n<div align=\"center\">\n  <!-- <div>\n  <img src=\"https://gravatar.com/avatar/dba6bae8c566f9d4041fb9cd9ada7741?d=identicon&f=y\">\n</div>\n  <ion-icon class=\"icod\" name=\"camera\"></ion-icon> -->\n  <ion-button  class=\"button-publicacion\" expand=\"block\" fill=\"outline\" (click)=\"takephoto()\">\n    <ion-icon slot=\"start\" name=\"camera\"></ion-icon>\n    Tomar fotografia</ion-button>\n    <ion-button  class=\"button-publicacion\"  expand=\"block\" fill=\"outline\" (click)=\"getimage()\">\n      <ion-icon slot=\"start\" name=\"add\"></ion-icon>\n      Seleccionar fotografia</ion-button>\n     <ion-row>\n       <ion-col size=\"4\" *ngFor=\"let item of fotografias; index as key\" >\n         <img [src]=\"item.foto\" alt=\"\">\n         <div align=\"center\">\n          <ion-button icon-left color=\"danger\" (click)=\"removeItem(key)\">\n              <ion-icon name=\"close\"></ion-icon> \n          </ion-button>\n          </div>\n       </ion-col>\n     </ion-row>\n</div>\n<div id=\"buttons-link\" align=\"center\">\n  <ion-button expand=\"block\" class=\"btns\" (click)=\"addVehiculoVecom()\">\n    Guardar\n  </ion-button>\n</div>\n</ion-content>\n\n\n";
    /***/
  },

  /***/
  "./src/app/agregar-auto/agregar-auto-routing.module.ts":
  /*!*************************************************************!*\
    !*** ./src/app/agregar-auto/agregar-auto-routing.module.ts ***!
    \*************************************************************/

  /*! exports provided: AgregarAutoPageRoutingModule */

  /***/
  function srcAppAgregarAutoAgregarAutoRoutingModuleTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "AgregarAutoPageRoutingModule", function () {
      return AgregarAutoPageRoutingModule;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/fesm2015/router.js");
    /* harmony import */


    var _agregar_auto_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! ./agregar-auto.page */
    "./src/app/agregar-auto/agregar-auto.page.ts");

    var routes = [{
      path: '',
      component: _agregar_auto_page__WEBPACK_IMPORTED_MODULE_3__["AgregarAutoPage"]
    }];

    var AgregarAutoPageRoutingModule = function AgregarAutoPageRoutingModule() {
      _classCallCheck(this, AgregarAutoPageRoutingModule);
    };

    AgregarAutoPageRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
      imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
      exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
    })], AgregarAutoPageRoutingModule);
    /***/
  },

  /***/
  "./src/app/agregar-auto/agregar-auto.module.ts":
  /*!*****************************************************!*\
    !*** ./src/app/agregar-auto/agregar-auto.module.ts ***!
    \*****************************************************/

  /*! exports provided: AgregarAutoPageModule */

  /***/
  function srcAppAgregarAutoAgregarAutoModuleTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "AgregarAutoPageModule", function () {
      return AgregarAutoPageModule;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/common */
    "./node_modules/@angular/common/fesm2015/common.js");
    /* harmony import */


    var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! @angular/forms */
    "./node_modules/@angular/forms/fesm2015/forms.js");
    /* harmony import */


    var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! @ionic/angular */
    "./node_modules/@ionic/angular/dist/fesm5.js");
    /* harmony import */


    var _agregar_auto_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! ./agregar-auto-routing.module */
    "./src/app/agregar-auto/agregar-auto-routing.module.ts");
    /* harmony import */


    var _agregar_auto_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! ./agregar-auto.page */
    "./src/app/agregar-auto/agregar-auto.page.ts");

    var AgregarAutoPageModule = function AgregarAutoPageModule() {
      _classCallCheck(this, AgregarAutoPageModule);
    };

    AgregarAutoPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
      imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"], _agregar_auto_routing_module__WEBPACK_IMPORTED_MODULE_5__["AgregarAutoPageRoutingModule"]],
      declarations: [_agregar_auto_page__WEBPACK_IMPORTED_MODULE_6__["AgregarAutoPage"]]
    })], AgregarAutoPageModule);
    /***/
  },

  /***/
  "./src/app/agregar-auto/agregar-auto.page.scss":
  /*!*****************************************************!*\
    !*** ./src/app/agregar-auto/agregar-auto.page.scss ***!
    \*****************************************************/

  /*! exports provided: default */

  /***/
  function srcAppAgregarAutoAgregarAutoPageScss(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "img {\n  height: 200px;\n  width: 600px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9pYmVyL0RvY3VtZW50cy9pYmVyL3Byb2plY3RzL3ZlY29tbW9iaWxlL3NyYy9hcHAvYWdyZWdhci1hdXRvL2FncmVnYXItYXV0by5wYWdlLnNjc3MiLCJzcmMvYXBwL2FncmVnYXItYXV0by9hZ3JlZ2FyLWF1dG8ucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUNBO0VBQ0ksYUFBQTtFQUNBLFlBQUE7QUNBSiIsImZpbGUiOiJzcmMvYXBwL2FncmVnYXItYXV0by9hZ3JlZ2FyLWF1dG8ucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiICBcclxuaW1ne1xyXG4gICAgaGVpZ2h0OiAyMDBweDtcclxuICAgIHdpZHRoOiA2MDBweDtcclxufSIsImltZyB7XG4gIGhlaWdodDogMjAwcHg7XG4gIHdpZHRoOiA2MDBweDtcbn0iXX0= */";
    /***/
  },

  /***/
  "./src/app/agregar-auto/agregar-auto.page.ts":
  /*!***************************************************!*\
    !*** ./src/app/agregar-auto/agregar-auto.page.ts ***!
    \***************************************************/

  /*! exports provided: AgregarAutoPage */

  /***/
  function srcAppAgregarAutoAgregarAutoPageTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "AgregarAutoPage", function () {
      return AgregarAutoPage;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @ionic/angular */
    "./node_modules/@ionic/angular/dist/fesm5.js");
    /* harmony import */


    var _ionic_native_camera_ngx__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! @ionic-native/camera/ngx */
    "./node_modules/@ionic-native/camera/ngx/index.js");
    /* harmony import */


    var _servicios_servicios_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! ../servicios/servicios.service */
    "./src/app/servicios/servicios.service.ts");

    var AgregarAutoPage =
    /*#__PURE__*/
    function () {
      function AgregarAutoPage(navCtrl, camera, service, loadingCtrl, toastCtrl) {
        _classCallCheck(this, AgregarAutoPage);

        this.navCtrl = navCtrl;
        this.camera = camera;
        this.service = service;
        this.loadingCtrl = loadingCtrl;
        this.toastCtrl = toastCtrl;
      }

      _createClass(AgregarAutoPage, [{
        key: "ngOnInit",
        value: function ngOnInit() {
          this.loadMarcas();
          this.loadTiposVehiculo();
        }
      }, {
        key: "loadTiposVehiculo",
        value: function loadTiposVehiculo() {
          var _this = this;

          this.service.getTipoVehiculo().subscribe(function (data) {
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](_this, void 0, void 0,
            /*#__PURE__*/
            regeneratorRuntime.mark(function _callee() {
              return regeneratorRuntime.wrap(function _callee$(_context) {
                while (1) {
                  switch (_context.prev = _context.next) {
                    case 0:
                      this.tipoVehiculo = data.data;
                      console.log(this.tipoVehiculo);

                    case 2:
                    case "end":
                      return _context.stop();
                  }
                }
              }, _callee, this);
            }));
          }, function (err) {});
        }
      }, {
        key: "verifyMarca",
        value: function verifyMarca(e) {
          return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0,
          /*#__PURE__*/
          regeneratorRuntime.mark(function _callee2() {
            var toast;
            return regeneratorRuntime.wrap(function _callee2$(_context2) {
              while (1) {
                switch (_context2.prev = _context2.next) {
                  case 0:
                    console.log(e);

                    if (e.detail.value) {
                      _context2.next = 6;
                      break;
                    }

                    _context2.next = 4;
                    return this.toastCtrl.create({
                      message: "Seleccionar Marca",
                      duration: 2000,
                      cssClass: 'my-custom-class'
                    });

                  case 4:
                    toast = _context2.sent;
                    toast.present();

                  case 6:
                  case "end":
                    return _context2.stop();
                }
              }
            }, _callee2, this);
          }));
        }
      }, {
        key: "callModelos",
        value: function callModelos(e) {
          if (e.detail.value) {
            console.log("entre aqui a marcas on select");
            this.loadModelos(this.marca);
          }
        }
      }, {
        key: "loadMarcas",
        value: function loadMarcas() {
          return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0,
          /*#__PURE__*/
          regeneratorRuntime.mark(function _callee4() {
            var _this2 = this;

            var loading;
            return regeneratorRuntime.wrap(function _callee4$(_context4) {
              while (1) {
                switch (_context4.prev = _context4.next) {
                  case 0:
                    _context4.next = 2;
                    return this.loadingCtrl.create({
                      message: 'Cargando...'
                    });

                  case 2:
                    loading = _context4.sent;
                    _context4.next = 5;
                    return loading.present().then(function () {
                      _this2.service.getMarcas().subscribe(function (data) {
                        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](_this2, void 0, void 0,
                        /*#__PURE__*/
                        regeneratorRuntime.mark(function _callee3() {
                          return regeneratorRuntime.wrap(function _callee3$(_context3) {
                            while (1) {
                              switch (_context3.prev = _context3.next) {
                                case 0:
                                  this.marcas = data.data;
                                  console.log(this.marcas);
                                  _context3.next = 4;
                                  return loading.dismiss();

                                case 4:
                                case "end":
                                  return _context3.stop();
                              }
                            }
                          }, _callee3, this);
                        }));
                      }, function (err) {
                        console.log(err);
                        loading.dismiss();
                      });
                    });

                  case 5:
                  case "end":
                    return _context4.stop();
                }
              }
            }, _callee4, this);
          }));
        }
      }, {
        key: "loadModelos",
        value: function loadModelos(id) {
          return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0,
          /*#__PURE__*/
          regeneratorRuntime.mark(function _callee6() {
            var _this3 = this;

            var loading;
            return regeneratorRuntime.wrap(function _callee6$(_context6) {
              while (1) {
                switch (_context6.prev = _context6.next) {
                  case 0:
                    _context6.next = 2;
                    return this.loadingCtrl.create({
                      message: 'Cargando modelos'
                    });

                  case 2:
                    loading = _context6.sent;
                    _context6.next = 5;
                    return loading.present().then(function () {
                      _this3.service.getModelos(id).subscribe(function (data) {
                        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](_this3, void 0, void 0,
                        /*#__PURE__*/
                        regeneratorRuntime.mark(function _callee5() {
                          return regeneratorRuntime.wrap(function _callee5$(_context5) {
                            while (1) {
                              switch (_context5.prev = _context5.next) {
                                case 0:
                                  this.modelos = data.data;
                                  console.log(this.modelos);
                                  _context5.next = 4;
                                  return loading.dismiss();

                                case 4:
                                case "end":
                                  return _context5.stop();
                              }
                            }
                          }, _callee5, this);
                        }));
                      }, function (err) {
                        console.log(err);
                        loading.dismiss();
                      });
                    });

                  case 5:
                  case "end":
                    return _context6.stop();
                }
              }
            }, _callee6, this);
          }));
        }
      }, {
        key: "guardar",
        value: function guardar() {
          this.navCtrl.navigateRoot('/perfil-usuario');
        }
      }, {
        key: "takephoto",
        value: function takephoto() {
          var _this4 = this;

          var options = {
            quality: 100,
            destinationType: this.camera.DestinationType.DATA_URL,
            encodingType: this.camera.EncodingType.JPEG,
            mediaType: this.camera.MediaType.PICTURE,
            correctOrientation: true,
            allowEdit: true
          };
          this.camera.getPicture(options).then(function (imageData) {
            _this4.imgOficial = "data:image/jpeg;base64," + imageData;

            _this4.service.addPhotography(_this4.imgOficial);

            _this4.loadFotografias();
          }, function (err) {});
        }
      }, {
        key: "getimage",
        value: function getimage() {
          var _this5 = this;

          var options = {
            quality: 100,
            destinationType: this.camera.DestinationType.DATA_URL,
            sourceType: this.camera.PictureSourceType.PHOTOLIBRARY,
            saveToPhotoAlbum: false,
            correctOrientation: true,
            allowEdit: true
          };
          this.camera.getPicture(options).then(function (imageData) {
            _this5.imgOficial = "data:image/jpeg;base64," + imageData;

            _this5.service.addPhotography(_this5.imgOficial);

            _this5.loadFotografias();
          }, function (err) {});
        }
      }, {
        key: "loadFotografias",
        value: function loadFotografias() {
          var _this6 = this;

          this.service.loadAll().then(function (result) {
            _this6.fotografias = result;
          });
        }
      }, {
        key: "removeItem",
        value: function removeItem(ndx) {
          this.service.removeItem(ndx);
        }
      }, {
        key: "addVehiculoVecom",
        value: function addVehiculoVecom() {
          var _this7 = this;

          this.userId = localStorage.getItem('idUsuario');
          this.service.addVehiculoVecom(this.nombre, this.color, this.puertas, this.cilindrada, this.combustible, this.caracteristicas, this.ciudad, this.precio, this.anho, this.marca, this.modelo, this.tipo_vehiculo, this.userId, this.fotografias).subscribe(function (data) {
            _this7.success = data.data.data;
            console.log(_this7.success);

            _this7.succesSms(_this7.success);

            _this7.navCtrl.navigateRoot('/perfil-usuario');
          }, function (err) {
            console.log(err);

            if (err.status == 422) {
              _this7.errorSms(err.error.data.data);
            } else {
              _this7.errorConexion();
            }
          });
        }
      }, {
        key: "errorConexion",
        value: function errorConexion() {
          return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0,
          /*#__PURE__*/
          regeneratorRuntime.mark(function _callee7() {
            var toast;
            return regeneratorRuntime.wrap(function _callee7$(_context7) {
              while (1) {
                switch (_context7.prev = _context7.next) {
                  case 0:
                    _context7.next = 2;
                    return this.toastCtrl.create({
                      message: "error de conexion",
                      duration: 3000,
                      cssClass: 'my-custom-class'
                    });

                  case 2:
                    toast = _context7.sent;
                    toast.present();

                  case 4:
                  case "end":
                    return _context7.stop();
                }
              }
            }, _callee7, this);
          }));
        }
      }, {
        key: "succesSms",
        value: function succesSms(msm) {
          return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0,
          /*#__PURE__*/
          regeneratorRuntime.mark(function _callee8() {
            var toast;
            return regeneratorRuntime.wrap(function _callee8$(_context8) {
              while (1) {
                switch (_context8.prev = _context8.next) {
                  case 0:
                    _context8.next = 2;
                    return this.toastCtrl.create({
                      message: msm,
                      duration: 4000,
                      cssClass: 'my-custom-class-success'
                    });

                  case 2:
                    toast = _context8.sent;
                    toast.present();

                  case 4:
                  case "end":
                    return _context8.stop();
                }
              }
            }, _callee8, this);
          }));
        }
      }, {
        key: "errorSms",
        value: function errorSms(msm) {
          return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0,
          /*#__PURE__*/
          regeneratorRuntime.mark(function _callee9() {
            var toast;
            return regeneratorRuntime.wrap(function _callee9$(_context9) {
              while (1) {
                switch (_context9.prev = _context9.next) {
                  case 0:
                    _context9.next = 2;
                    return this.toastCtrl.create({
                      message: msm,
                      duration: 4000,
                      cssClass: 'my-custom-class'
                    });

                  case 2:
                    toast = _context9.sent;
                    toast.present();

                  case 4:
                  case "end":
                    return _context9.stop();
                }
              }
            }, _callee9, this);
          }));
        }
      }]);

      return AgregarAutoPage;
    }();

    AgregarAutoPage.ctorParameters = function () {
      return [{
        type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["NavController"]
      }, {
        type: _ionic_native_camera_ngx__WEBPACK_IMPORTED_MODULE_3__["Camera"]
      }, {
        type: _servicios_servicios_service__WEBPACK_IMPORTED_MODULE_4__["ServiciosService"]
      }, {
        type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["LoadingController"]
      }, {
        type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ToastController"]
      }];
    };

    AgregarAutoPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
      selector: 'app-agregar-auto',
      template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! raw-loader!./agregar-auto.page.html */
      "./node_modules/raw-loader/dist/cjs.js!./src/app/agregar-auto/agregar-auto.page.html")).default,
      styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! ./agregar-auto.page.scss */
      "./src/app/agregar-auto/agregar-auto.page.scss")).default]
    }), tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_angular__WEBPACK_IMPORTED_MODULE_2__["NavController"], _ionic_native_camera_ngx__WEBPACK_IMPORTED_MODULE_3__["Camera"], _servicios_servicios_service__WEBPACK_IMPORTED_MODULE_4__["ServiciosService"], _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["LoadingController"], _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ToastController"]])], AgregarAutoPage);
    /***/
  }
}]);
//# sourceMappingURL=agregar-auto-agregar-auto-module-es5.js.map