(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["detalle-auto-detalle-auto-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/detalle-auto/detalle-auto.page.html":
/*!*******************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/detalle-auto/detalle-auto.page.html ***!
  \*******************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<!-- <app-header titulo=\"Detalles\">\n  <ion-icon name=\"heart-half\" class=\"ion-float-end\"></ion-icon>\n</app-header> -->\n<!--el icono de favoritos no se muestra -->\n<ion-header >\n  <ion-toolbar class=\"head\">\n      <ion-buttons slot=\"start\">\n          <ion-back-button color=\"light\"></ion-back-button>\n        </ion-buttons>\n    <ion-title style=\"color: #ffffff;\"> Detalle</ion-title>\n    <ion-buttons slot=\"end\">\n        <ion-menu-button  style=\"color: #ffffff;\"></ion-menu-button>\n      </ion-buttons>\n  </ion-toolbar>\n</ion-header>\n<ion-content>\n    <ion-slides page>\n        <ion-slide *ngFor=\"let foto of arrayPosts.imagenes_autos\">\n            <img\n              [src]=\"foto\">\n        </ion-slide>\n      </ion-slides>\n  <div align=\"center\">\n    <ion-icon class=\"icod\" name=\"heart\" class=\"icod\" (click)=\"addFavorito()\"></ion-icon>\n  </div>\n  <div align=\"center\">\n    <h2>PRECIO: {{arrayPosts.precio}} $</h2>\n  </div>\n  <div>\n    <ion-item class=\"datos\">\n    <p>MARCA:{{arrayPosts.marca}}</p>\n    </ion-item>\n    <ion-item class=\"datos\">\n    <p>MODELO:{{arrayPosts.modelo}}</p>\n    </ion-item>\n    <ion-grid>\n      <ion-row>\n        <ion-col size=\"6\">\n          <ion-item style=\"font-size: small; margin-left: 5px;\">\n          <p>AÑO:{{arrayPosts.anho}}</p>\n          </ion-item>\n        </ion-col>\n        <ion-col size=\"6\">\n          <ion-item style=\"font-size: small; margin-right: 15px;\">\n          <p>COLOR:{{arrayPosts.color}}</p>\n          </ion-item>\n        </ion-col>\n        <ion-col size=\"6\">\n          <ion-item style=\"font-size: small; margin-left: 5px;\">\n          <p>PUERTAS:{{arrayPosts.puertas}}</p>\n          </ion-item>\n        </ion-col>\n        <ion-col size=\"6\">\n          <ion-item style=\"font-size: small; margin-right: 15px;\">\n          <p>CILINDRADA:{{arrayPosts.cilindrada}}</p>\n          </ion-item>\n        </ion-col>\n      </ion-row>\n    </ion-grid>\n    <ion-item class=\"datos\">\n    <p>TIPO DE COMBUSTIBLE:{{arrayPosts.tipo_combustible}}</p>\n    </ion-item>\n    <ion-item class=\"datos\">\n    <p>CIUDAD:{{arrayPosts.ciudad}}</p>\n    </ion-item>\n    <ion-item class=\"datos\">\n    <p align=\"justify\">CARACTERISTICAS:{{arrayPosts.caracteristicas}}</p>\n    </ion-item>\n  </div>\n  <br>\n  <ion-button class=\"btns\" expand=\"block\" (click)=\"presentActionSheet()\">Contactar</ion-button>\n</ion-content>\n");

/***/ }),

/***/ "./src/app/detalle-auto/detalle-auto-routing.module.ts":
/*!*************************************************************!*\
  !*** ./src/app/detalle-auto/detalle-auto-routing.module.ts ***!
  \*************************************************************/
/*! exports provided: DetalleAutoPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DetalleAutoPageRoutingModule", function() { return DetalleAutoPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _detalle_auto_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./detalle-auto.page */ "./src/app/detalle-auto/detalle-auto.page.ts");




const routes = [
    {
        path: '',
        component: _detalle_auto_page__WEBPACK_IMPORTED_MODULE_3__["DetalleAutoPage"]
    }
];
let DetalleAutoPageRoutingModule = class DetalleAutoPageRoutingModule {
};
DetalleAutoPageRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], DetalleAutoPageRoutingModule);



/***/ }),

/***/ "./src/app/detalle-auto/detalle-auto.module.ts":
/*!*****************************************************!*\
  !*** ./src/app/detalle-auto/detalle-auto.module.ts ***!
  \*****************************************************/
/*! exports provided: DetalleAutoPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DetalleAutoPageModule", function() { return DetalleAutoPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _detalle_auto_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./detalle-auto-routing.module */ "./src/app/detalle-auto/detalle-auto-routing.module.ts");
/* harmony import */ var _detalle_auto_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./detalle-auto.page */ "./src/app/detalle-auto/detalle-auto.page.ts");
/* harmony import */ var _components_components_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../components/components.module */ "./src/app/components/components.module.ts");








let DetalleAutoPageModule = class DetalleAutoPageModule {
};
DetalleAutoPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _components_components_module__WEBPACK_IMPORTED_MODULE_7__["ComponentsModule"],
            _detalle_auto_routing_module__WEBPACK_IMPORTED_MODULE_5__["DetalleAutoPageRoutingModule"]
        ],
        declarations: [_detalle_auto_page__WEBPACK_IMPORTED_MODULE_6__["DetalleAutoPage"]]
    })
], DetalleAutoPageModule);



/***/ }),

/***/ "./src/app/detalle-auto/detalle-auto.page.scss":
/*!*****************************************************!*\
  !*** ./src/app/detalle-auto/detalle-auto.page.scss ***!
  \*****************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("ion-card {\n  border-radius: 40px 40px 0px 0px;\n  overflow: unset !important;\n  height: 160px;\n  width: 280px;\n  margin-top: 40px;\n}\n\nimg {\n  width: 1024px;\n  height: 300px;\n  background: black;\n}\n\n.datos {\n  margin-left: 15px;\n  margin-right: 25px;\n  font-size: small;\n}\n\n.ion-float-end {\n  zoom: 1.2;\n  color: #ffffff;\n  margin-right: 2px;\n}\n\n.icotrash {\n  zoom: 2;\n  color: darkred;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9pYmVyL0RvY3VtZW50cy9pYmVyL3Byb2plY3RzL3ZlY29tbW9iaWxlL3NyYy9hcHAvZGV0YWxsZS1hdXRvL2RldGFsbGUtYXV0by5wYWdlLnNjc3MiLCJzcmMvYXBwL2RldGFsbGUtYXV0by9kZXRhbGxlLWF1dG8ucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUNBO0VBRUksZ0NBQUE7RUFDQSwwQkFBQTtFQUNBLGFBQUE7RUFDQSxZQUFBO0VBQ0EsZ0JBQUE7QUNBSjs7QURHQTtFQUNJLGFBQUE7RUFDQSxhQUFBO0VBQ0EsaUJBQUE7QUNBSjs7QURLQTtFQUNJLGlCQUFBO0VBQ0Esa0JBQUE7RUFDQSxnQkFBQTtBQ0ZKOztBRElBO0VBQ0ksU0FBQTtFQUNBLGNBQUE7RUFDQSxpQkFBQTtBQ0RKOztBREdBO0VBQ0ksT0FBQTtFQUNBLGNBQUE7QUNBSiIsImZpbGUiOiJzcmMvYXBwL2RldGFsbGUtYXV0by9kZXRhbGxlLWF1dG8ucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiXHJcbmlvbi1jYXJkIHtcclxuICAgIC13ZWJraXQtYm9yZGVyLXJhZGl1czogMjVweCAyNXB4IDBweCAwcHg7XHJcbiAgICBib3JkZXItcmFkaXVzOiA0MHB4IDQwcHggMHB4IDBweDtcclxuICAgIG92ZXJmbG93OiB1bnNldCAhaW1wb3J0YW50O1xyXG4gICAgaGVpZ2h0OiAxNjBweDtcclxuICAgIHdpZHRoOiAyODBweDtcclxuICAgIG1hcmdpbi10b3A6IDQwcHg7XHJcbn1cclxuXHJcbmltZ3tcclxuICAgIHdpZHRoOiAxMDI0cHg7XHJcbiAgICBoZWlnaHQ6IDMwMHB4O1xyXG4gICAgYmFja2dyb3VuZDogYmxhY2s7XHJcbiAgICAvLyBwb3NpdGlvbjogZml4ZWQ7XHJcbiAgICAvLyBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbn1cclxuXHJcbi5kYXRvc3tcclxuICAgIG1hcmdpbi1sZWZ0OiAxNXB4O1xyXG4gICAgbWFyZ2luLXJpZ2h0OiAyNXB4O1xyXG4gICAgZm9udC1zaXplOiBzbWFsbDtcclxufVxyXG4uaW9uLWZsb2F0LWVuZHsgICAgXHJcbiAgICB6b29tOiAxLjI7IFxyXG4gICAgY29sb3I6ICNmZmZmZmY7IFxyXG4gICAgbWFyZ2luLXJpZ2h0OiAycHg7XHJcbn1cclxuLmljb3RyYXNoe1xyXG4gICAgem9vbTogMjtcclxuICAgIGNvbG9yOiBkYXJrcmVkO1xyXG4gICAgXHJcbn0iLCJpb24tY2FyZCB7XG4gIC13ZWJraXQtYm9yZGVyLXJhZGl1czogMjVweCAyNXB4IDBweCAwcHg7XG4gIGJvcmRlci1yYWRpdXM6IDQwcHggNDBweCAwcHggMHB4O1xuICBvdmVyZmxvdzogdW5zZXQgIWltcG9ydGFudDtcbiAgaGVpZ2h0OiAxNjBweDtcbiAgd2lkdGg6IDI4MHB4O1xuICBtYXJnaW4tdG9wOiA0MHB4O1xufVxuXG5pbWcge1xuICB3aWR0aDogMTAyNHB4O1xuICBoZWlnaHQ6IDMwMHB4O1xuICBiYWNrZ3JvdW5kOiBibGFjaztcbn1cblxuLmRhdG9zIHtcbiAgbWFyZ2luLWxlZnQ6IDE1cHg7XG4gIG1hcmdpbi1yaWdodDogMjVweDtcbiAgZm9udC1zaXplOiBzbWFsbDtcbn1cblxuLmlvbi1mbG9hdC1lbmQge1xuICB6b29tOiAxLjI7XG4gIGNvbG9yOiAjZmZmZmZmO1xuICBtYXJnaW4tcmlnaHQ6IDJweDtcbn1cblxuLmljb3RyYXNoIHtcbiAgem9vbTogMjtcbiAgY29sb3I6IGRhcmtyZWQ7XG59Il19 */");

/***/ }),

/***/ "./src/app/detalle-auto/detalle-auto.page.ts":
/*!***************************************************!*\
  !*** ./src/app/detalle-auto/detalle-auto.page.ts ***!
  \***************************************************/
/*! exports provided: DetalleAutoPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DetalleAutoPage", function() { return DetalleAutoPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _servicios_servicios_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../servicios/servicios.service */ "./src/app/servicios/servicios.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");





let DetalleAutoPage = class DetalleAutoPage {
    constructor(navCtrl, service, loadingController, toastController, actionSheetCtrl, route, alertController) {
        this.navCtrl = navCtrl;
        this.service = service;
        this.loadingController = loadingController;
        this.toastController = toastController;
        this.actionSheetCtrl = actionSheetCtrl;
        this.route = route;
        this.alertController = alertController;
        this.arrayPosts = { id: "", nombre: "", puertas: "", cilindrada: "", tipo_combustible: "", caracteristicas: "", ciudad: "", precio: "", anho: "", marca: "", modelo: "", tipovehiculo: "", color: "", imagenes_autos: [{}] };
        this.data = this.route.snapshot.paramMap.get('detalleAutoVecom');
    }
    ngOnInit() {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            console.log(this.data);
            const loading = yield this.loadingController.create({
                message: 'Cargando...',
            });
            yield loading.present().then(() => {
                this.service.getDetalleAutomovil(this.data).subscribe((data) => tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
                    this.arrayPosts = data['data'];
                    console.log(this.arrayPosts);
                    yield loading.dismiss();
                    this.msjToast();
                }), err => {
                    console.log(err);
                    loading.dismiss();
                });
            });
        });
    }
    msjToast() {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            const toast = yield this.toastController.create({
                message: 'Datos mostrados.',
                duration: 2000
            });
            toast.present();
        });
    }
    presentActionSheet() {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            const actionSheet = yield this.actionSheetCtrl.create({
                backdropDismiss: false,
                buttons: [{
                        text: 'Llamar',
                        icon: 'call',
                        handler: () => {
                            console.log('Call clicked');
                        }
                    }, {
                        text: 'Mandar Mensaje',
                        icon: 'logo-whatsapp',
                        handler: () => {
                            console.log('SendMsj clicked');
                        }
                    }, {
                        text: 'Compartir',
                        icon: 'share-alt',
                        handler: () => {
                            console.log('Share clicked');
                        }
                    }, {
                        text: 'Cancelar',
                        icon: 'close',
                        role: 'cancelar',
                        cssClass: 'secondary',
                        handler: () => {
                            console.log('Cancel clicked');
                        }
                    }]
            });
            yield actionSheet.present();
        });
    }
    addFavorito() {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            const alert = yield this.alertController.create({
                header: 'Anadir favorito!',
                message: 'Esta seguro que desea agregar a favoritos?',
                buttons: [
                    {
                        text: 'No',
                        role: 'cancel',
                        cssClass: 'secondary',
                        handler: (blah) => {
                            console.log('Confirm Cancel: blah');
                        }
                    }, {
                        text: 'Si',
                        handler: () => {
                            this.service.anhadirFavoritoVehiculoVecom(this.data).subscribe((data) => tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
                                console.log(data.data);
                                this.succesSms(data.data);
                            }), err => {
                                this.errorSms(err.error.data.message);
                                console.log(err.error.data.message);
                            });
                        }
                    }
                ]
            });
            yield alert.present();
        });
    }
    succesSms(msm) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            const toast = yield this.toastController.create({
                message: msm,
                duration: 4000,
                cssClass: 'my-custom-class-success',
            });
            toast.present();
        });
    }
    errorSms(msm) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            const toast = yield this.toastController.create({
                message: msm,
                duration: 4000,
                cssClass: 'my-custom-class',
            });
            toast.present();
        });
    }
};
DetalleAutoPage.ctorParameters = () => [
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["NavController"] },
    { type: _servicios_servicios_service__WEBPACK_IMPORTED_MODULE_3__["ServiciosService"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["LoadingController"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ToastController"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ActionSheetController"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_4__["ActivatedRoute"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["AlertController"] }
];
DetalleAutoPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-detalle-auto',
        template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./detalle-auto.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/detalle-auto/detalle-auto.page.html")).default,
        styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./detalle-auto.page.scss */ "./src/app/detalle-auto/detalle-auto.page.scss")).default]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_angular__WEBPACK_IMPORTED_MODULE_2__["NavController"],
        _servicios_servicios_service__WEBPACK_IMPORTED_MODULE_3__["ServiciosService"],
        _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["LoadingController"],
        _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ToastController"],
        _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ActionSheetController"],
        _angular_router__WEBPACK_IMPORTED_MODULE_4__["ActivatedRoute"],
        _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["AlertController"]])
], DetalleAutoPage);



/***/ })

}]);
//# sourceMappingURL=detalle-auto-detalle-auto-module-es2015.js.map