(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["editar-vehiculo-editar-vehiculo-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/editar-vehiculo/editar-vehiculo.page.html":
/*!*************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/editar-vehiculo/editar-vehiculo.page.html ***!
  \*************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-header>\n    <ion-toolbar class=\"head\">\n      <!-- <ion-buttons slot=\"start\">\n        <ion-menu-button  style=\"color: #ffffff;\"></ion-menu-button>\n      </ion-buttons> -->\n      <ion-buttons slot=\"start\">\n        <ion-back-button color=\"light\"></ion-back-button>\n      </ion-buttons>\n      <ion-title class=\"title\">\n        Editar Vehiculo\n      </ion-title>\n    </ion-toolbar> \n  </ion-header>\n  <ion-content>\n   \n  \n    <div align=\"center\">\n      <h3>Información basica</h3>\n      <ion-item class=\"box\"> \n        <ion-label>Nombre:</ion-label>\n        <ion-input [(ngModel)]=\"nombre\" placeholder=\"Nissan pathfinder\"></ion-input>  \n    </ion-item>\n    <br>\n    <ion-item class=\"box\">\n      <ion-label class=\"fw500\">Ciudad</ion-label>\n      <ion-select [(ngModel)]=\"ciudad\" multiple=\"false\">\n        <ion-select-option>La Paz</ion-select-option>\n        <ion-select-option>Santa Cruz</ion-select-option>\n        <ion-select-option>Cochabamba</ion-select-option>\n        <ion-select-option>Potosi</ion-select-option>\n        <ion-select-option>Oruro</ion-select-option>\n        <ion-select-option>Beni</ion-select-option>\n        <ion-select-option>Pando</ion-select-option>\n        <ion-select-option>Tarija</ion-select-option>\n        <ion-select-option>Sucre</ion-select-option>\n      </ion-select>\n    </ion-item>\n  <br>\n  <ion-item class=\"box\">\n    <ion-label class=\"fw500\">Tipo de Vehiculo</ion-label>\n    <ion-select [(ngModel)]=\"tipo_vehiculo\" multiple=\"false\">\n      <ion-select-option *ngFor=\"let item of tipoVehiculo\" [value]=\"item.id\">{{item.nombre}}</ion-select-option>\n    </ion-select>\n  </ion-item>\n  <br>\n  <ion-item class=\"box\" >\n    <ion-label class=\"fw500\">Marca</ion-label>\n    <ion-select  [(ngModel)]=\"marca\"  (ionChange)=\"callModelos($event)\">\n    <ion-select-option *ngFor=\"let item of marcas\"  value=\"{{item.id}}\">{{item.nombre}}</ion-select-option>\n    </ion-select>\n  </ion-item>\n  <br>\n  <ion-item class=\"box\">\n    <ion-label class=\"fw500\" >Modelo</ion-label>\n    <ion-select [(ngModel)]=\"modelo\" (ionChange)=\"verifyMarca($event)\">\n    <ion-select-option *ngFor=\"let item of modelos\" [value]=\"item.id\" >{{item.nombre}}</ion-select-option>                \n    </ion-select>\n  </ion-item> \n  <br>  \n  <div>\n  <ion-item class=\"box\">\n    <ion-label class=\"fw500\">Color</ion-label>\n    <input type=\"text\" [(ngModel)]=\"color\" multiple=\"false\">\n  </ion-item>\n  </div>\n  <br> \n  <div>\n  <ion-item class=\"box\"> \n    <ion-label class=\"fw500\">Cilindrada:</ion-label>\n    <ion-input type=\"text\" [(ngModel)]=\"cilindrada\"></ion-input>  \n  </ion-item>\n  </div>\n  <br>\n  <ion-item class=\"box\">\n    <ion-label class=\"fw500\">Combustible</ion-label>\n    <ion-select [(ngModel)]=\"combustible\" multiple=\"false\">\n      <ion-select-option value=\"gasolina\">Gasolina</ion-select-option>\n      <ion-select-option value=\"gnv\">GNV</ion-select-option>\n      <ion-select-option value=\"diesel\">Diesel</ion-select-option>   \n      <ion-select-option value=\"diesel/gnv\">Diesel/GNV</ion-select-option>                  \n      <ion-select-option value=\"gasolina/gnv\">Gasolina/GNV</ion-select-option>                  \n  \n    </ion-select>\n  </ion-item>\n  <br>\n  <ion-item class=\"box\">\n    <ion-label class=\"fw500\">Caja</ion-label>\n    <ion-select [(ngModel)]=\"caja\" multiple=\"false\">\n      <ion-select-option value=\"automatica\">Automatica</ion-select-option>\n      <ion-select-option value=\"mecanica\">Mecanica</ion-select-option>\n      <ion-select-option value=\"secuencial\">Secuencial</ion-select-option>                  \n    </ion-select>\n  </ion-item>\n  <br>\n  \n  <ion-item class=\"box\">\n    <ion-label class=\"fw500\">Año</ion-label>\n    <!-- <ion-datetime [(ngModel)]=\"anho\" placeholder=\"YYYY\"\n                  display-format=\"YYYY\"\n                  min=\"1950\"\n                  max=\"2025\"\n                  value=\"2021\" \n                  displayFormat=\"YYYY\" pickerFormat=\"YYYY\" >\n                  \n    </ion-datetime> -->\n    <ion-input type=\"text\" [(ngModel)]=\"anho\"></ion-input>\n  </ion-item> \n  <br>\n         \n    <div>\n    <ion-item class=\"box\"> \n        <ion-label class=\"fw500\">Precio:</ion-label>\n        <ion-input type=\"number\" [(ngModel)]=\"precio\"></ion-input>  \n    </ion-item>\n    </div>\n    <br>\n    <div>\n      <ion-item class=\"box\"> \n          <ion-label class=\"fw500\">Numero de puertas:</ion-label>\n          <ion-input type=\"text\" [(ngModel)]=\"puertas\"></ion-input>  \n      </ion-item>\n      </div>\n      <br>\n    <div>\n      <ion-item class=\"box\"> \n        <textarea [(ngModel)]=\"caracteristicas\" name=\"\" id=\"\" cols=\"40\" rows=\"10\" placeholder=\"Caracteristicas\"></textarea>\n        </ion-item>\n      </div>\n      <br>  \n  \n  </div>\n  <div id=\"buttons-link\" align=\"center\">\n      <ion-button expand=\"block\" class=\"btns\" (click)=\"editarVehiculoVecom()\">\n        Editar\n      </ion-button>\n    </div>\n</ion-content>\n");

/***/ }),

/***/ "./src/app/editar-vehiculo/editar-vehiculo-routing.module.ts":
/*!*******************************************************************!*\
  !*** ./src/app/editar-vehiculo/editar-vehiculo-routing.module.ts ***!
  \*******************************************************************/
/*! exports provided: EditarVehiculoPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EditarVehiculoPageRoutingModule", function() { return EditarVehiculoPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _editar_vehiculo_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./editar-vehiculo.page */ "./src/app/editar-vehiculo/editar-vehiculo.page.ts");




const routes = [
    {
        path: '',
        component: _editar_vehiculo_page__WEBPACK_IMPORTED_MODULE_3__["EditarVehiculoPage"]
    }
];
let EditarVehiculoPageRoutingModule = class EditarVehiculoPageRoutingModule {
};
EditarVehiculoPageRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], EditarVehiculoPageRoutingModule);



/***/ }),

/***/ "./src/app/editar-vehiculo/editar-vehiculo.module.ts":
/*!***********************************************************!*\
  !*** ./src/app/editar-vehiculo/editar-vehiculo.module.ts ***!
  \***********************************************************/
/*! exports provided: EditarVehiculoPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EditarVehiculoPageModule", function() { return EditarVehiculoPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _editar_vehiculo_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./editar-vehiculo-routing.module */ "./src/app/editar-vehiculo/editar-vehiculo-routing.module.ts");
/* harmony import */ var _editar_vehiculo_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./editar-vehiculo.page */ "./src/app/editar-vehiculo/editar-vehiculo.page.ts");







let EditarVehiculoPageModule = class EditarVehiculoPageModule {
};
EditarVehiculoPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _editar_vehiculo_routing_module__WEBPACK_IMPORTED_MODULE_5__["EditarVehiculoPageRoutingModule"]
        ],
        declarations: [_editar_vehiculo_page__WEBPACK_IMPORTED_MODULE_6__["EditarVehiculoPage"]]
    })
], EditarVehiculoPageModule);



/***/ }),

/***/ "./src/app/editar-vehiculo/editar-vehiculo.page.scss":
/*!***********************************************************!*\
  !*** ./src/app/editar-vehiculo/editar-vehiculo.page.scss ***!
  \***********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2VkaXRhci12ZWhpY3Vsby9lZGl0YXItdmVoaWN1bG8ucGFnZS5zY3NzIn0= */");

/***/ }),

/***/ "./src/app/editar-vehiculo/editar-vehiculo.page.ts":
/*!*********************************************************!*\
  !*** ./src/app/editar-vehiculo/editar-vehiculo.page.ts ***!
  \*********************************************************/
/*! exports provided: EditarVehiculoPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EditarVehiculoPage", function() { return EditarVehiculoPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _servicios_servicios_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../servicios/servicios.service */ "./src/app/servicios/servicios.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");





let EditarVehiculoPage = class EditarVehiculoPage {
    constructor(service, route, loadingController, toastCtrl) {
        this.service = service;
        this.route = route;
        this.loadingController = loadingController;
        this.toastCtrl = toastCtrl;
        this.arrayPosts = { id: "", nombre: "", puertas: "", cilindrada: "", tipo_combustible: "", caracteristicas: "", ciudad: "", precio: "", anho: "", marca: "", modelo: "", tipovehiculo: "", color: "", caja: "", imagenes_autos: [{}] };
        this.data = this.route.snapshot.paramMap.get('detalleAutoVecom');
    }
    ngOnInit() {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            this.loadMarcas();
            this.loadTiposVehiculo();
            this.loadInformacion();
        });
    }
    loadTiposVehiculo() {
        this.service.getTipoVehiculo().subscribe((data) => tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            this.tipoVehiculo = data.data;
            console.log(this.tipoVehiculo);
        }), err => {
        });
    }
    verifyMarca(e) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            console.log(e);
            if (!e.detail.value) {
                const toast = yield this.toastCtrl.create({
                    message: "Seleccionar Marca",
                    duration: 2000,
                    cssClass: 'my-custom-class',
                });
                toast.present();
            }
        });
    }
    loadMarcas() {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            this.service.getMarcas().subscribe((data) => tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
                this.marcas = data.data;
            }), err => {
            });
        });
    }
    callModelos(e) {
        if (e.detail.value) {
            console.log("entre aqui a marcas on select");
            this.loadModelos(this.marca);
        }
    }
    loadInformacion() {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            const loading = yield this.loadingController.create({
                message: 'Cargando...',
            });
            yield loading.present().then(() => {
                this.service.getEditarAutomovil(this.data).subscribe((data) => tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
                    this.arrayPosts = data['data'];
                    this.nombre = this.arrayPosts.nombre;
                    this.ciudad = this.arrayPosts.ciudad;
                    this.tipo_vehiculo = this.arrayPosts.tipovehiculo;
                    this.marca = this.arrayPosts.marca;
                    this.modelo = this.arrayPosts.modelo;
                    this.color = this.arrayPosts.color;
                    this.cilindrada = this.arrayPosts.cilindrada;
                    this.combustible = this.arrayPosts.tipo_combustible;
                    this.caja = this.arrayPosts.caja;
                    this.anho = this.arrayPosts.anho;
                    this.caracteristicas = this.arrayPosts.caracteristicas;
                    this.precio = this.arrayPosts.precio;
                    this.puertas = this.arrayPosts.puertas;
                    console.log(this.arrayPosts);
                    yield loading.dismiss();
                }), err => {
                    console.log(err);
                    loading.dismiss();
                });
            });
        });
    }
    loadModelos(id) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            const loading = yield this.loadingController.create({
                message: 'Cargando modelos',
            });
            yield loading.present().then(() => {
                this.service.getModelos(id).subscribe((data) => tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
                    this.modelos = data.data;
                    yield loading.dismiss();
                }), err => {
                    this.errorSms(err.data.data);
                    loading.dismiss();
                });
            });
        });
    }
    editarVehiculoVecom() {
        this.userId = localStorage.getItem('idUsuario');
        this.service.editVehiculoVecom(this.data, this.nombre, this.color, this.puertas, this.cilindrada, this.combustible, this.caracteristicas, this.ciudad, this.precio, this.anho, this.marca, this.modelo, this.tipo_vehiculo, this.caja, this.userId).subscribe(data => {
            this.success = data.data;
            console.log(this.success);
            this.succesSms(this.success.data);
            //  this.navCtrl.navigateRoot('/perfil-usuario');
        }, err => {
            console.log(err);
            this.errorSms(err.error.data.data);
            // if (err.status == 401) {
            //   this.errorSms(err.error.data.message);
            // } else {
            //   this.errorConexion();
            // }
        });
    }
    succesSms(msm) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            const toast = yield this.toastCtrl.create({
                message: msm,
                duration: 4000,
                cssClass: 'my-custom-class-success',
            });
            toast.present();
        });
    }
    errorSms(msm) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            const toast = yield this.toastCtrl.create({
                message: msm,
                duration: 4000,
                cssClass: 'my-custom-class',
            });
            toast.present();
        });
    }
};
EditarVehiculoPage.ctorParameters = () => [
    { type: _servicios_servicios_service__WEBPACK_IMPORTED_MODULE_2__["ServiciosService"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_3__["ActivatedRoute"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["LoadingController"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["ToastController"] }
];
EditarVehiculoPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-editar-vehiculo',
        template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./editar-vehiculo.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/editar-vehiculo/editar-vehiculo.page.html")).default,
        styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./editar-vehiculo.page.scss */ "./src/app/editar-vehiculo/editar-vehiculo.page.scss")).default]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_servicios_servicios_service__WEBPACK_IMPORTED_MODULE_2__["ServiciosService"],
        _angular_router__WEBPACK_IMPORTED_MODULE_3__["ActivatedRoute"],
        _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["LoadingController"],
        _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["ToastController"]])
], EditarVehiculoPage);



/***/ })

}]);
//# sourceMappingURL=editar-vehiculo-editar-vehiculo-module-es2015.js.map